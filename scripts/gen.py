import json
import random
import sys
if(len(sys.argv) < 4):
    print("error usage: - wordLength - filename -MaxWordCount")
    exit()
MAX_LEN = int(sys.argv[1])
input_file = open('wordArray.json')
json_array = json.load(input_file)
wordList = json_array['array']
def firstUpper(s):
    return s[0].upper() + s[1:]
def filterShort(s):
    return len(s) <= MAX_LEN

print(len(wordList))
wordList = list(filter(filterShort, wordList))
wordList = list(map( firstUpper, wordList ))
wordList = wordList[:int(sys.argv[3])]
secondWord = wordList[:]
random.shuffle(wordList)
random.shuffle(secondWord)
out = []
print(len(wordList));
print("Generating bite codes... ")


for word in wordList:
    for inner in secondWord:
        out.append( word + inner)
random.shuffle(out)
print("Writing to file")
with open(sys.argv[2], 'w') as file_handler:
    for item in out:
        file_handler.write("{}\n".format(item))