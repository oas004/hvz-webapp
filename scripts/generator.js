let json = require("./wordArray.json");
var fs = require('fs');
//console.log(json.array);
let words = json.array;
const maxLength = 5;

let filteredWords = json.array.filter(e => e.length < maxLength);

filteredWords = filteredWords.map(e => {
    
    return e[0].toUpperCase() + e.slice(1);
} );
console.log(filteredWords.length);

/* Randomize array in-place using Durstenfeld shuffle algorithm 
Posted: Laurens Holst
Edited: shleedawg
https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array

*/
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

let firstWords = shuffleArray([...filteredWords]);
let secondWords = shuffleArray([...filteredWords]);
let generatedWords = [];
firstWords.forEach((word, index) => {
    
    secondWords.forEach(innerWord => {
        generatedWords.push(word + innerWord);
    })
});
var wordsJson = JSON.stringify(generatedWords);
fs.writeFile('biteCodes.json', wordsJson, 'utf-8', (err, data) => {
    err => {
        console.log(err);
    }
    data => {
        console.log(data);
    }
})