import json
import random

MAX_LEN = 4
input_file = open('wordArray.json')
json_array = json.load(input_file)
wordList = json_array['array']
def firstUpper(s):
    return s[0].upper() + s[1:]
def filterShort(s):
    return len(s) <= MAX_LEN

print(len(wordList))
wordList = list(filter(filterShort, wordList))
wordList = list(map( firstUpper, wordList ))


random.shuffle(wordList)

out = []

print("Generating bite codes... ")


for word in wordList:
    out.append(word)
print("Writing to file")
with open('one.txt', 'w') as file_handler:
    for item in out:
        file_handler.write("{}\n".format(item))