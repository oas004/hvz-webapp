#   Humans vs. Zombies
    ![Logo](HVZ-WEBAPP/ClientApp/src/assets/images/hvz-logo.png)

    Human vs. is a live action game often played at college campus in the US.
    The storyline of the game is that all players but one starts of as humans. 
    Futher, the games main objective is to survive a zombie apocalypse, that 
    started with the first player. This player is refered to as the patient zero.
    Humans can defend them self by using any item that is approved by a moderator,
    where the most common equipment being balls of socks and marshmalows.
    
    This projects aim was to keep track of this game in an elegant maner, and to
    make it easier for the players to keep track of who is a zombie and who is
    a human.
    
    
##  The HvZ Team

    * Odin Asbjørnsen
    * Håkon Engly Johannsen
    * Johan Emil Løvald
    * Tage Lessum
    * Tony H. Abaz

##   Getting started

    Clone this repo to your computer, and open in visual studio and run npm i in
    the client app. Then, press f5 to build and launch IIS Express.
    
    
##   Docker
    
    In order to make a docker repo, we have dockerized the application. If you
    want to host the application from docker, please delete the package-lock.json.
    Then run:
    
                docker-compose up 
                
    and the docker file will build itself. If you are asked about environment 
    variables, add the variables to your database string into docker.
    
##  Ngrok

    If you want to host it from your server, you can download ngrok 
    (https://ngrok.com/).
    
    Open your port on 8080, and get the url from ngrok. Furthermore, go to 
    properties -> debug -> web and change the app url to the ngrok one. Then,
    build with the docker-compose file.
    
##  API Documentation

    In this project we have used Swagger to generate the API documentation.
    
    * https://localhost:<port>/swagger/index.html
    
