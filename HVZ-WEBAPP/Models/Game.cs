﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HVZ_WEBAPP.Models
{
    public partial class Game
    {
        public Game()
        {
            Chat = new HashSet<Chat>();
            Kill = new HashSet<Kill>();
            Mission = new HashSet<Mission>();
            Player = new HashSet<Player>();
            Squad = new HashSet<Squad>();
            SquadCheckin = new HashSet<SquadCheckin>();
            SquadMember = new HashSet<SquadMember>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [Column("name")]
        public string Name { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("game_state")]
        public byte GameState { get; set; }
        [Column("nw_lat", TypeName = "decimal(8, 6)")]
        public decimal? NwLat { get; set; }
        [Column("nw_lng", TypeName = "decimal(9, 6)")]
        public decimal? NwLng { get; set; }
        [Column("se_lat", TypeName = "decimal(8, 6)")]
        public decimal? SeLat { get; set; }
        [Column("se_lng", TypeName = "decimal(9, 6)")]
        public decimal? SeLng { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("start_time", TypeName = "datetime2")]
        public DateTime StartTime { get; set; }

        [Column("end_time", TypeName = "datetime2")]
        public DateTime EndTime { get; set; }

        [InverseProperty("Game")]
        public virtual ICollection<Chat> Chat { get; set; }
        [InverseProperty("Game")]
        public virtual ICollection<Kill> Kill { get; set; }
        [InverseProperty("Game")]
        public virtual ICollection<Mission> Mission { get; set; }
        [InverseProperty("Game")]
        public virtual ICollection<Player> Player { get; set; }
        [InverseProperty("Game")]
        public virtual ICollection<Squad> Squad { get; set; }
        [InverseProperty("Game")]
        public virtual ICollection<SquadCheckin> SquadCheckin { get; set; }
        [InverseProperty("Game")]
        public virtual ICollection<SquadMember> SquadMember { get; set; }

        public GameListPayload GetBaseView()
        {
            return  new GameListPayload(this, true);

        }
        public GameListPayload GetUserView()
        {
            GameListPayload payload = new GameListPayload(this);
            return payload;
        }

        public GameListPayload GetAdminView()
        {
            return new GameListPayload(this);
        }
    }
    //  Sent by game controller, sends only what a non-logged in user should see.
    
    public class GameListPayload
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public byte GameState { get; set; }
        public int Players { get; set; }
        [Column("nw_lat", TypeName = "decimal(8, 6)")]
        public decimal? NwLat { get; set; }
        [Column("nw_lng", TypeName = "decimal(9, 6)")]
        public decimal? NwLng { get; set; }
        [Column("se_lat", TypeName = "decimal(8, 6)")]
        public decimal? SeLat { get; set; }
        [Column("se_lng", TypeName = "decimal(9, 6)")]
        public decimal? SeLng { get; set; }
        public DateTime StartTime { get; set; }
        
        public DateTime EndTime { get; set; }

        public Boolean IsDeleted { get; set; }

        public GameListPayload(Game game)
        {
            Id = game.Id;
            Name = game.Name;
            GameState = game.GameState;
            Players = game.Player.Count;
            StartTime = game.StartTime;
            EndTime = game.EndTime;
            NwLat = game.NwLat;
            NwLng = game.NwLng;
            SeLat = game.SeLat;
            SeLng = game.SeLng;
        }

        public GameListPayload(Game game, bool access)
        {
            Id = game.Id;
            Name = game.Name;
            GameState = game.GameState;
            Players = game.Player.Count;
            StartTime = game.StartTime;
            EndTime = game.EndTime;
        }
        
    }
}
