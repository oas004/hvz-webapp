﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HVZ_WEBAPP.Models
{
    public partial class Mission
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Column("name")]
        public string Name { get; set; }
        [Column("is_human_visible")]
        public bool IsHumanVisible { get; set; }
        [Column("is_zombie_visible")]
        public bool IsZombieVisible { get; set; }
        [Column("description")]
        [StringLength(128)]
        public string Description { get; set; }
        [Column("start_time", TypeName = "date")]
        public DateTime? StartTime { get; set; }
        [Column("end_time", TypeName = "date")]
        public DateTime? EndTime { get; set; }
        [Column("game_id")]
        public int GameId { get; set; }

        [Column("lat", TypeName = "decimal(8, 6)")]
        public decimal? lat { get; set; }
        [Column("lng", TypeName = "decimal(9, 6)")]
        public decimal? lng { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("Mission")]
        public virtual Game Game { get; set; }
    }
}
