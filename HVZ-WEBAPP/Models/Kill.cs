﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace HVZ_WEBAPP.Models
{
    public partial class Kill
    {
        [Key]
        public int Id { get; set; }
        [Column("time_of_death", TypeName = "datetime")]
        public DateTime TimeOfDeath { get; set; }
        [Column("story")]
        public string Story { get; set; }
        [Column("lat", TypeName = "decimal(8, 6)")]
        public decimal? Lat { get; set; }
        [Column("lng", TypeName = "decimal(9, 6)")]
        public decimal? Lng { get; set; }
        [Column("game_id")]
        public int GameId { get; set; }
        [Column("killer_id")]
        public int KillerId { get; set; }
        [Column("victim_id")]
        public int VictimId { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("Kill")]
        [JsonIgnore]
        public virtual Game Game { get; set; }
        [ForeignKey(nameof(KillerId))]
        [InverseProperty(nameof(Player.KillKiller))]
        [JsonIgnore]
        public virtual Player Killer { get; set; }
        [ForeignKey(nameof(VictimId))]
        [InverseProperty(nameof(Player.KillVictim))]
        [JsonIgnore]
        public virtual Player Victim { get; set; }

        public object GetMarkerView()
        {
            object Killer = new {this.Killer.User.Username};
            object Victim = new {this.Victim.User.Username};
            return new
            {
                Id, TimeOfDeath, Story, Lat, Lng, GameId, KillerId, VictimId, IsDeleted,
                Killer, Victim
            };
        }
    }
}
