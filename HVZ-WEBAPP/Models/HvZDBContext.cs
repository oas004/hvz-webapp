﻿using System;
using HvZ_WEBAPP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HVZ_WEBAPP.Models
{
    public partial class HvZDBContext : DbContext
    {
        public HvZDBContext()
        {
        }

        public HvZDBContext(DbContextOptions<HvZDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Chat> Chat { get; set; }
        public virtual DbSet<Game> Game { get; set; }
        public virtual DbSet<Kill> Kill { get; set; }
        public virtual DbSet<Mission> Mission { get; set; }
        public virtual DbSet<Player> Player { get; set; }
        public virtual DbSet<Squad> Squad { get; set; }
        public virtual DbSet<SquadCheckin> SquadCheckin { get; set; }
        public virtual DbSet<SquadMember> SquadMember { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=tcp:hvz.database.windows.net;Database=HvZDB;Trusted_Connection=True;Persist Security Info=False;User ID=hvzadmin;Password=Memeteam123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chat>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.HasIndex(e => e.PlayerId);

                entity.HasIndex(e => e.SquadId);

                //entity.Property(e => e.ChatTime)
                //    .IsRowVersion()
                //    .IsConcurrencyToken();

                entity.Property(e => e.Message).IsFixedLength();

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.Chat)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Chat_Game");

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.Chat)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Chat_Player");

                entity.HasOne(d => d.Squad)
                    .WithMany(p => p.Chat)
                    .HasForeignKey(d => d.SquadId)
                    .HasConstraintName("FK_Chat_Squad");
            });

            modelBuilder.Entity<Game>(entity =>
            {
                entity.Property(e => e.Name).IsFixedLength();
            });

            modelBuilder.Entity<Kill>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.HasIndex(e => e.KillerId);

                entity.HasIndex(e => e.VictimId);

                entity.Property(e => e.Story).IsFixedLength();

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.Kill)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Kill_Game");

                entity.HasOne(d => d.Killer)
                    .WithMany(p => p.KillKiller)
                    .HasForeignKey(d => d.KillerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Kill_Player_Killer");

                entity.HasOne(d => d.Victim)
                    .WithMany(p => p.KillVictim)
                    .HasForeignKey(d => d.VictimId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Kill_Player_Victim");
            });

            modelBuilder.Entity<Mission>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.Property(e => e.Description).IsFixedLength();

                entity.Property(e => e.Name).IsFixedLength();

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.Mission)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Mission_Game");
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.BiteCode).IsFixedLength();

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.Player)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Player_Game");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Player)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Player_User");
            });

            modelBuilder.Entity<Squad>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.Property(e => e.Name).IsFixedLength();

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.Squad)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Squad_Game");
            });

            modelBuilder.Entity<SquadCheckin>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.HasIndex(e => e.SquadId);

                entity.HasIndex(e => e.SquadMemberId);

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.SquadCheckin)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SquadCheckin_Game");

                entity.HasOne(d => d.Squad)
                    .WithMany(p => p.SquadCheckin)
                    .HasForeignKey(d => d.SquadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SquadCheckin_Squad");

                entity.HasOne(d => d.SquadMember)
                    .WithMany(p => p.SquadCheckin)
                    .HasForeignKey(d => d.SquadMemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SquadCheckin_SquadMember");
            });

            modelBuilder.Entity<SquadMember>(entity =>
            {
                entity.HasIndex(e => e.GameId);

                entity.HasIndex(e => e.PlayerId);

                entity.HasIndex(e => e.SquadId);

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.SquadMember)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SquadMember_Game");

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.SquadMember)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SquadMember_Player");

                entity.HasOne(d => d.Squad)
                    .WithMany(p => p.SquadMember)
                    .HasForeignKey(d => d.SquadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SquadMember_Squad");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.FirstName).IsFixedLength();

                entity.Property(e => e.LastName).IsFixedLength();
                entity.HasAlternateKey(u => u.AspNetUserId);
                entity.HasAlternateKey(u => u.Username);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
