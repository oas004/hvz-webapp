﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HVZ_WEBAPP.Models
{
    public partial class SquadCheckin
    {
        [Key]
        public int ? Id { get; set; }
        [Column("start_time", TypeName = "datetime")]
        public DateTime StartTime { get; set; }
        [Column("end_time", TypeName = "datetime")]
        public DateTime EndTime { get; set; }
        [Column("lat", TypeName = "decimal(8, 6)")]
        public decimal ? Lat { get; set; }
        [Column("lng", TypeName = "decimal(9, 6)")]
        public decimal ? Lng { get; set; }
        [Column("game_id")]
        public int GameId { get; set; }
        [Column("squad_id")]
        public int SquadId { get; set; }
        [Column("squad_member_id")]
        public int SquadMemberId { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("SquadCheckin")]
        public virtual Game Game { get; set; }
        [ForeignKey(nameof(SquadId))]
        [InverseProperty("SquadCheckin")]
        public virtual Squad Squad { get; set; }
        [ForeignKey(nameof(SquadMemberId))]
        [InverseProperty("SquadCheckin")]

        
        public virtual SquadMember SquadMember { get; set; }

        public object GetMapAdminView()
        {
            var a = new { Username = SquadMember.Player.User.Username, CheckIn = this };
            
            // encapsulate this.
            return a;
        }
        public object GetMapView()
        {
            var a = new {Username = SquadMember.Player.User.Username, CheckIn = new
            {
                Id, StartTime, EndTime, Lat, Lng, GameId, SquadId, SquadMemberId
            }};
            /*
            id: number = 0;
            startTime: number = Date.now();
            endTime: number = Date.now();
            lat: number = 0;
            lng: number = 0;
            gameId: number = 0;
            squadId: number = 0;
            squadMemberId: number = 0; // can be retrieved by server.*/ 
            
            // encapsulate this.
            return a;
        }
    }

    
}
