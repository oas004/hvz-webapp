﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace HVZ_WEBAPP.Models
{
    public partial class Squad
    {
        public Squad()
        {
            Chat = new HashSet<Chat>();
            SquadCheckin = new HashSet<SquadCheckin>();
            SquadMember = new HashSet<SquadMember>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [Column("name")]
        public string Name { get; set; }
        [Column("is_human")]
        public bool IsHuman { get; set; }
        [Column("game_id")]
        public int GameId { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("Squad")]
        [JsonIgnore]
        public virtual Game Game { get; set; }
        [InverseProperty("Squad")]
        [JsonIgnore]
        public virtual ICollection<Chat> Chat { get; set; }
        [InverseProperty("Squad")]
        [JsonIgnore]
        public virtual ICollection<SquadCheckin> SquadCheckin { get; set; }
        [InverseProperty("Squad")]
        [JsonIgnore]
        public virtual ICollection<SquadMember> SquadMember { get; set; }

        public object GetChatView()
        {
            return new {Id, Name, IsHuman };
        }
    }
}