﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using HvZ_WEBAPP.Models;


namespace HVZ_WEBAPP.Models
{
    public partial class Player
    {
        public Player()
        {
            Chat = new HashSet<Chat>();
            KillKiller = new HashSet<Kill>();
            KillVictim = new HashSet<Kill>();
            SquadMember = new HashSet<SquadMember>();
        }
        [Key]
        public int? Id { get; set; }
        [Column("is_human")]
        public bool IsHuman { get; set; }
        [Column("is_patient_zero")]
        public bool IsPatientZero { get; set; }


        [Required]
        [Column("rank")]
        public int Rank { get; set; }
        [Required]
        [Column("bite_code")]
        public string ? BiteCode { get; set; }
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("game_id")]
        public int GameId  { get; set; }

        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("Player")]
        [JsonIgnore]
        public virtual Game Game  { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty("Player")]
        public virtual User User  { get; set; }
        [InverseProperty("Player")]
        public virtual ICollection<Chat>  Chat { get; set; }
        [InverseProperty(nameof(Kill.Killer))]
        public virtual ICollection<Kill>  KillKiller { get; set; }
        [InverseProperty(nameof(Kill.Victim))]
        public virtual ICollection<Kill>  KillVictim { get; set; }
        [InverseProperty("Player")]

        [JsonIgnore]
        public virtual ICollection<SquadMember>  SquadMember { get; set; }
        

        public object GetUserView()
        {
            return new  { Id, IsHuman, User, Rank, UserId};
            //  Fuck still need to fix this.
        }
        public object GetAdminView()
        {
            return this;
        }

        public object GetChatView()
        {
            List<SquadMember> SquadMemeber = new List<SquadMember>();
            foreach (var sm in this.SquadMember)
            {
                if(!sm.IsDeleted) SquadMemeber.Add(sm);
            }
            return new {User.Username, Id, IsHuman, SquadMember, Rank  };
        }
    }

    
}
