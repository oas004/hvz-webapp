﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
namespace HVZ_WEBAPP.Models
{
    public partial class SquadMember
    {
        public SquadMember()
        {
            SquadCheckin = new HashSet<SquadCheckin>();
        }

        [Key]
        public int Id { get; set; }
        [Column("rank")]
        public int Rank { get; set; }
        [Column("game_id")]
        public int GameId { get; set; }
        [Column("squad_id")]
       
        public int SquadId { get; set; }
        [Column("player_id")]
        public int PlayerId { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("SquadMember")]
        public virtual Game Game { get; set; }
        [ForeignKey(nameof(PlayerId))]
        [InverseProperty("SquadMember")]
        public virtual Player Player { get; set; }
        [ForeignKey(nameof(SquadId))]
        [InverseProperty("SquadMember")]
        public virtual Squad Squad { get; set; }
        [InverseProperty("SquadMember")]
        public virtual ICollection<SquadCheckin> SquadCheckin { get; set; }
    }
}
