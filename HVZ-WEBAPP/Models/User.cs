﻿using HVZ_WEBAPP.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;
using System.Text.Json.Serialization;

namespace HvZ_WEBAPP.Models
{
    public partial class User
    {
        public User()
        {
            Player = new HashSet<Player>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [Column("asp_net_user_id")]
        
        [StringLength(450)]
        public string AspNetUserId { get; set; }
        [Required]
        [Column("username")]
        public string Username { get; set; }
        [Required]
        [Column("first_name")]
        public string FirstName { get; set; }
        [Required]
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [JsonIgnore]
        [InverseProperty("User")]
        public virtual ICollection<Player> Player { get; set; }

    }
}
