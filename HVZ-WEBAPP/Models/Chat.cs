﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace HVZ_WEBAPP.Models
{
    public partial class Chat
    {
        [Key]
        public int ?  Id { get; set; }
        [Required]
        [Column("message")]
        public string Message { get; set; }
        [Column("is_human_global")]
        public bool IsHumanGlobal { get; set; }
        [Column("is_zombie_global")]
        public bool IsZombieGlobal { get; set; }
        [Required]
        [Column("chat_time", TypeName = "datetime2")]

        public DateTime ? ChatTime { get; set; }
        [Column("game_id")]
        public int GameId { get; set; }
        [Column("player_id")]
        public int PlayerId { get; set; }
        [Column("squad_id")]
        public int? SquadId { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(GameId))]
        [InverseProperty("Chat")]
        [JsonIgnore]
        public virtual Game Game { get; set; }
        [ForeignKey(nameof(PlayerId))]
        [JsonIgnore]
        [InverseProperty("Chat")]
        
        public virtual Player Player { get; set; }
        [ForeignKey(nameof(SquadId))]
        [InverseProperty("Chat")]
        [JsonIgnore]
        public virtual Squad Squad { get; set; }
    }
    //  Encapsulate data
    public class ChatUpdate
    {

        public List<ChatPayload> GlobalChat { get; set; } = new List<ChatPayload>();
        public List<ChatPayload> FactionChat { get; set; } = new List<ChatPayload>();
        public List<ChatPayload> SquadChat { get; set; } = new List<ChatPayload>();

        public ChatUpdate()
        {

        }
        public ChatUpdate(List<ChatPayload> GlobalChat, List<ChatPayload> FactionChat, List<ChatPayload> SquadChat)
        {
            this.GlobalChat = GlobalChat;
            this.FactionChat = FactionChat;
            this.SquadChat = SquadChat;
        }
    }
    //  Could probably do a squad membership here?
    public class ChatPayload
    {
        public int ?Id { get; set; }
        public string Message { get; set; }
        public bool IsHumanGlobal { get; set; }
       
        public bool IsZombieGlobal { get; set; }
        public bool IsSquad { get; set; }
        public int? SquadId { get; set; }

        public DateTime ? ChatTime { get; set; }
        
        public object PlayerRef { get; set; }   //  ugly

        public Chat toChat()
        {
            Chat c = new Chat();
            c.Message = Message;
            c.ChatTime = DateTime.Now;
            c.IsHumanGlobal = IsHumanGlobal;
            c.IsZombieGlobal = IsZombieGlobal;
            return c;
        }
        public ChatPayload()
        {

        }
        public ChatPayload(Chat chat)
        {
            Id = chat.Id;
            Message = chat.Message;
            IsHumanGlobal = chat.IsHumanGlobal;
            IsZombieGlobal = chat.IsZombieGlobal;
            ChatTime = chat.ChatTime;

            PlayerRef = new {chat.Player.User.Username, chat.PlayerId, chat.Player.Rank};
            IsSquad = chat.SquadId != null;
            SquadId = chat.SquadId;
        }
    }
}
