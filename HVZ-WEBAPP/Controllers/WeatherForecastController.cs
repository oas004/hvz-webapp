﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HVZ_WEBAPP.Controllers
{
    [Authorize]
    //[Authorize(Roles = "Administrator")]
    //[Authorize(Policy = "RequireAdministrationRole")]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly UserManager<Models.ApplicationUser> _userManager;
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
    
        public WeatherForecastController(ILogger<WeatherForecastController> logger, UserManager<Models.ApplicationUser>
            userManager)
        {
            _logger = logger;
            _userManager = userManager;

           
        }
        [Authorize]
        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> GetAsync()
        {
           
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _userManager.FindByIdAsync(uid.ToString());
            
            //Console.WriteLine("The user " + user.ToString());

            var rng = new Random();
           
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
        
    }
}
