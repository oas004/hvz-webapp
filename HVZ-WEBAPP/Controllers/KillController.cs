﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HVZ_WEBAPP.Models;
using HVZ_WEBAPP;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace HvZ.Controllers
{
    [Authorize]
    [Route("api/game/")]
    [ApiController]
    public class KillController : ControllerBase
    {

        private readonly HvZDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public KillController(HvZDBContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        /// <summary>
        /// Get all the kills
        /// </summary>
        /// <returns>List of kill object</returns>
        /// <response code="200">Request success</response>
        /// <response code="404">Game was not found by the provided game id</response>
        [Produces("application/json")]
        [AllowAnonymous]
        [HttpGet("{gameid}/kill")]

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        
        //  refactored to return anonymous type
        public ActionResult<IEnumerable<object>> GetKill(int gameid, int offset = 0, int limit = 20000)   // magic variables nononononno
        {
            Game game = _context.Game.Find(gameid);

            if (game == null)
                return NotFound();
            //  how should I get usernames with this?
            
            return _context.Kill.Include(k => k.Killer).ThenInclude(p => p.User)
                .Include(k => k.Victim).ThenInclude(p => p.User)
                .ToList()
                .FindAll(g => g.GameId == gameid && !g.IsDeleted)
                .Skip(offset)
                .Take(limit).Select(k => k.GetMarkerView()).ToList();

        }
        /// <summary>
        /// Gets a kill by id
        /// </summary>
        /// <returns>Kill object</returns>
        /// <response code="200">Request success</response>
        /// <response code="404">Game was not found by the provided game id</response>
        [Produces("application/json")]
        [AllowAnonymous]
        [HttpGet("{gameid}/kill/{killid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Kill> GetKillID(int gameid, int killid)
        {
            Game game = _context.Game.Find(gameid);

            if (game == null)
            {
                return NotFound();
            }
            return _context.Kill.ToList().Find(s => s.Id == killid && gameid == s.GameId && !s.IsDeleted);
        }
        /// <summary>
        /// Add a kill by bite code
        /// </summary>
        /// <param name="gameid"></param>
        /// <param name="kill"></param>
        /// <returns>Kill object</returns>
        /// <response code="201"></response>
        [HttpPost("{gameid}/kill/bitecode")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Kill>> AddViaBitecode(int gameid, KillObject kill)
        {

            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is zombie
            
            if (uid == null) 
                return Forbid();
            var aspnetUser = await _userManager.FindByIdAsync(uid);

            System.Diagnostics.Debug.WriteLine(uid);

            var user = _context.User.Include(u => u.Player).FirstOrDefault(user => user.AspNetUserId == uid);
            // user == killer

            var player = user.Player.FirstOrDefault(p => p.GameId == gameid && !p.IsDeleted);

            if (player == null)
                return NotFound();
            System.Diagnostics.Debug.WriteLine(player.UserId);
            System.Diagnostics.Debug.WriteLine(player.IsHuman);
        //USE BREAKPOINTS TO DEBUG!
            if (!player.IsHuman)
            { 

                Game game = _context.Game.Find(gameid);

                if (game == null || kill == null)
                    return BadRequest();

                if (kill.GameId != gameid)
                    return BadRequest("Game does not exist");

                var validKiller = _context.Player.ToList().Find(x => kill.KillerId == x.Id && !x.IsHuman && x.GameId == gameid && !x.IsDeleted);
                
                if (validKiller == null)
                    return BadRequest("Not valid killer");

                var biteLookup = _context.Player.ToList().Find(x => x.BiteCode == kill.Bitecode && x.IsHuman && x.GameId == gameid && !x.IsDeleted);

                if (biteLookup == null)
                    return BadRequest("Bitecode not valid");

                kill.Victim = biteLookup;

                biteLookup.IsHuman = false;


                _context.Kill.Add(kill);
                _context.Entry(biteLookup).State = EntityState.Modified;
                _context.SaveChanges();

                
                return kill;
            }
            return Forbid();

        }
        /// <summary>
        /// Post a kill by id
        /// </summary>
        /// <param name="gameid"></param>
        /// <param name="kill"></param>
        /// <returns>Kill object</returns>
        /// <response code="201">Request accepted</response>
        /// <response code="400">Game or kill was not found by the provided information</response>
        /// <response code="400">The provided kill id was not the same as the kill id in the body</response>
        /// <response code="400">The kill was not valid</response>
        /// <response code="400">Kill check was not valid</response>
        /// <response code="401">The request was made by a user that was not logged in</response>
        /// <response code="403">The request was not made by an admin</response>
        [HttpPost("{gameid}/kill")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<Kill>> AddKillAsync(int gameid, Kill kill)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            // If admin (or zombie?)
            if (isAdmin)
            {

                Game game = _context.Game.Find(gameid);

                if (game == null || kill == null)
                    return BadRequest();

                if (kill.GameId != gameid)
                    return BadRequest();

                bool validKill = kill.Victim.IsHuman && kill.GameId == gameid && !kill.Killer.IsHuman;

                if (!validKill)
                    return BadRequest();

                var killCheck = _context.Player.ToList().Find(x => x.IsHuman && x.GameId == gameid && !x.IsDeleted);
               
                if (killCheck == null)
                    return BadRequest();


                _context.Kill.Add(kill);
                _context.SaveChanges();

                return kill;
            }
            else
            {
                return Forbid();
            }

           
        }
        /// <summary>
        /// Update a kill object
        /// </summary>
        /// <param name="gameid"></param>
        /// <param name="killId"></param>
        /// <param name="kill"></param>
        /// <response code="204">Request success - no content</response>
        /// <response code="400">Provided kill id did not match the body kill id</response>
        /// <response code="401">User was not authenticated (logged in)</response>
        /// <response code="403">Request was made by a non admin</response>
        /// <response code="404">The kill object was not found</response>
        /// <returns>NoContent - updated</returns>
        [HttpPut("{gameid}/kill/{killid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Kill>> UpdateKillAsync(int gameid, int killId, Kill kill)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");


            // If Admin or Zombie
            if (isAdmin )
            {
                if (killId == kill.Id)
                {

                    Game game = _context.Game.Find(gameid);

                    if (game == null)
                        return NotFound();

                    var existingKill = _context.Kill.AsNoTracking().ToList().Find(x => x.Id == killId && x.GameId == gameid && !x.IsDeleted);
                    if (existingKill == null)
                    {
                        return NotFound();
                    }


                    _context.Entry(kill).State = EntityState.Modified;
                    _context.SaveChanges();

                    return NoContent();
                }

                return BadRequest();
            }
            return Forbid();

        }
        /// <summary>
        /// Delete a kill
        /// </summary>
        /// <param name="gameid"></param>
        /// <param name="killId"></param>
        /// <returns>Ok</returns>
        /// <response code="200">Request was valid</response>
        /// <response code="401">Request was made by a non-authenticated user</response>
        /// <response code="403">Request was made by a non-admin user</response>
        /// <response code="404">Game was not found by the game id</response>
        /// <response code="404">Kill was not found by the kill id</response>
        [HttpDelete("{gameid}/kill/{killid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Kill>> DeleteKillAsync(int gameid, int killId)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            // If Admin
            if (isAdmin)
            {
                Game game = _context.Game.Find(gameid);

                if (game == null)
                    return NotFound();


                var existingKill = _context.Kill.ToList().Find(x => x.Id == killId && x.GameId == gameid && !x.IsDeleted!);
                if (existingKill == null)
                {
                    return NotFound();
                }


                existingKill.IsDeleted = true;

                _context.Entry(existingKill).State = EntityState.Modified;
                _context.SaveChanges();

                return Ok($"Kill with code : {existingKill.Id} was successfully deleted");
            }

            return Forbid();

        }

    }
    public class KillObject : Kill
    {
        public string Bitecode { get; set; }

        
    }
}