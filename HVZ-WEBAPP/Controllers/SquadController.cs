﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HVZ_WEBAPP.Models;
using HVZ_WEBAPP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using HVZ_WEBAPP.HubConfig;
using HvZ_WEBAPP.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Data.SqlClient.Server;


namespace HVZ_WEBAPP.Controllers
{
    [Authorize]
    [Route("api/game/")]
    [ApiController]
    public class SquadController : ControllerBase
    {
        private readonly HvZDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHubContext<ChatHub> _hub;

        public SquadController(HvZDBContext context, UserManager<ApplicationUser> userManager, IHubContext<ChatHub> hub)
        {
            _context = context;
            _userManager = userManager;
            _hub = hub;
        }
        /// <summary>
        /// Get all the squads in a game
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns>List of squad objects</returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by a non authenticated user</response>
        /// <response code="404">Game was not found by the provided game id</response>
        [HttpGet("{game_id}/squad/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Squad>> GetSquads(int game_id, int offset = 0, int limit = 0)

        {
            Game game = _context.Game.Include(g => g.Squad).FirstOrDefault(g => g.Id == game_id && g.IsDeleted == false);

            if (game == null)
            {
                return NotFound();
            }

            if (limit == 0)
            {
                limit = game.Squad.Count() - offset;
            }
            return game.Squad.Skip(offset).Take(limit).ToList();
        }

        /// <summary>
        /// Get a squad by id
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="squad_id"></param>
        /// <returns>Squad object</returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by a non authenticated user</response>
        [HttpGet("{game_id}/squad/{squad_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<Squad> GetSquad(int game_id, int squad_id)
        {
            return _context.Squad.SingleOrDefault(s => s.Id == squad_id && s.IsDeleted == false);
        }

        /// <summary>
        /// Create a squad
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="squad"></param>
        /// <returns>Ok created</returns>
        /// <response code="201">Valid request - Squad created</response>
        /// <response code="401">Request was attempted by a non authenticated user</response>
        /// <response code="404">The game was not found by the provided id</response>
        [HttpPost("{game_id}/squad")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Squad> CreateSquad(int game_id, Squad squad)
        {
           if (game_id == squad.GameId)
            {
                _context.Squad.Add(squad);
                _context.SaveChanges();
            }
            else
            {
                return NotFound();
            }

            return squad;

        }


        /// <summary>
        /// Get squad members
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="squad_id"></param>
        /// <returns>Squadmember object</returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by a non authenticated user</response>

        [HttpGet("{game_id}/squad/{squad_id}/squadmembers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<IEnumerable<SquadMember>> GetSquadMembers(int game_id, int squad_id)
        {
            

            //var squadMember = _context.SquadMember.Include(x =>x.Player).ToList().FindAll(x => x.GameId == game_id && x.SquadId == squad_id && x.IsDeleted == false);
            var squadMember = _context.SquadMember.Include(sm => sm.Player).Where((x =>
                x.GameId == game_id && x.SquadId == squad_id && x.IsDeleted == false)).ToList();

            //var squadMember = _context.SquadMember.Include(x =>x.Player).ToList().FindAll(x => x.GameId == game_id && x.SquadId == squad_id && x.IsDeleted == false);

            return squadMember;
        }

        /// <summary>
        /// Delete a squadmember by id
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="squad_id"></param>
        /// <param name="squadmember_id"></param>
        /// <returns>Accepted - deleted</returns>
        /// <response code="202">Valid request - deleted</response>
        /// <response code="401">Request was made by a non authenticated user</response>
        /// <response code="403">Request was made by a non admin user</response>
        [HttpDelete("{game_id}/squad/{squad_id}/squadmembers/{squadmember_id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> DeleteSquadMemberAsync(int game_id, int squad_id, int squadmember_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            //  well, if you're able to leave a squad?
            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin)
            {
                var squadMember = _context.SquadMember.SingleOrDefault(s => s.Id == squadmember_id);
                if (squadMember == null) return NotFound("Could not find squadmember");
                squadMember.IsDeleted = true;
                _context.SaveChanges();
                return Accepted("Delete was made successfully");

            }
            else
            {
                return Forbid("Only admin can delete squadmembers");
            }

        }

        /// <summary>
        /// Create squadmember - joins a squad
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="squad_id"></param>
        /// <param name="squadMember"></param>
        /// <returns>Created squadmember object</returns>
        /// <response code="201">Valid request</response>
        /// <response code="400">The provided game id is different from the body provided game id</response>
        /// <response code="401">The request was made by a non authenticated user</response>
        /// <response code="404">The game or squad was not found by the provided id</response>
        [HttpPost("{game_id}/squad/{squad_id}/join")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<SquadMember> CreateSquadMember(int game_id, int squad_id, SquadMember squadMember)
        {
            Game game = _context.Game.Include(p => p.Squad).FirstOrDefault(g => g.Id == game_id);

            if (game == null)
            {
                return NotFound();
            }


            Squad squad = game.Squad.FirstOrDefault(s => s.Id == squad_id && s.IsDeleted == false);
            if (squad == null)
            {
                return NotFound();
            }

            if (game_id == squadMember.GameId)
            {
                SquadMember sm = new SquadMember();
                sm.GameId = squadMember.GameId;
                sm.PlayerId = squadMember.PlayerId;
                sm.Rank = squadMember.Rank;
                sm.SquadId = squadMember.SquadId;
                sm.IsDeleted = squadMember.IsDeleted;
                //sm.Player = squadMember.Player;

                _context.SquadMember.Add(sm);
                _context.SaveChanges();
            }
            else
            {
                return BadRequest();
            }

            return CreatedAtAction($"SquadMember with id: {squadMember.Id} succesfully created", squadMember);
        }

        /// <summary>
        /// Update the squad
        /// </summary>
        /// <param name="squad"></param>
        /// <returns>Ok - updated</returns>
        /// <response code="200">Valid response</response>
        /// <response code="401">Request was made by an un authenticated user</response>
        /// <response code="403">Request was made by a non admin</response>
        [HttpPut("{game_id}/squad/{squad_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<Squad>> UpdateSquadAsync(Squad squad)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin) { 
                // Only admin can update a squad

                _context.Entry(squad).State = EntityState.Modified;
                _context.SaveChanges();
                
                int squadid = squad.Id;

                return Ok($"Squad with Id {squadid} updated");
            }
            else
            {
                return Forbid("Only Admin may update a squad");
            }
        }

        /// <summary>
        /// Delete squad
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="squad_id"></param>
        /// <returns>Accepted delete</returns>
        /// <response code="202">Valid request - delete accepted</response>
        /// <response code="404">Squad that was attempted delete was not found</response>
        /// <response code="401">Request was made by an unauthenticated user</response>
        /// <response code="403">Request was made by an non admin</response>
        [HttpDelete("{game_id}/squad/{squad_id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<int>> DeleteSquadAsync(int game_id, int squad_id)
        {
            var squad = _context.Squad.SingleOrDefault(s => s.Id == squad_id);
          
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            if (isAdmin)
            {
                if (squad == null) return NotFound();
                 squad.IsDeleted = true;
                _context.SaveChanges();
                return Accepted();
            }
            else
            {
                return Forbid("Only admin can delete a squad!");
            }
            
        }

        /// <summary>
        /// Get the squad chat messages
        /// </summary>
        /// <param name="squad_id"></param>
        /// <param name="game_id"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns>Chat update object</returns>
        /// <response code="200">Valid request</response>
        /// <response code="400">Bad - not access to other faction chat</response>
        /// <response code="404">Game was not found by the provided id</response>
        /// <response code="404">Squad member was not found by the provided info</response>
        /// <response code="404">Squad was not found by the provided info</response>
        [HttpGet("{game_id}/squad/{squad_id}/chat")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ChatUpdate>> GetChatMessages(int squad_id, int game_id, int offset = 0, int limit = 0)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            
            // Get the player from the user table where fk is the aspnetuser key
            User user = await _context.User.FirstOrDefaultAsync(s => s.AspNetUserId.Equals(uid));
            
            //Game game = _context.Game.Include(g => g.Squad).ThenInclude(s => s.SquadMember).Include().Include(g => g.Player).FirstOrDefault(g => g.Id == game_id);
            Game game = _context.Game.Include(g => g.Squad)
                .ThenInclude(s => s.SquadMember).ThenInclude(sm => sm.Player)
                .Include(g => g.Squad).ThenInclude(s => s.Chat)
                
                .FirstOrDefault(g => g.Id == game_id);
                

            if (game == null)
            {
                return NotFound("Game not found.");
            }
            Squad squad = game.Squad.FirstOrDefault(s => s.Id == squad_id);
            if (squad == null) return NotFound("The selected squad was not found.");
             

            SquadMember member = squad.SquadMember.FirstOrDefault(s => s.Player.UserId == user.Id && !s.IsDeleted);
            if (member == null)
            {
                return NotFound("The player was not found.");
            }

            Player player = member.Player;
            bool is_human = player.IsHuman;

            //  Unused?
            if (limit == 0)
            {
                limit = game.Chat.Count() - offset;
            }

            if (is_human != squad.IsHuman)
            {
                return BadRequest("No access to other faction chat");
            }
            //  convert to chat payload and returnd
            List<ChatPayload> squadMessages = new List<ChatPayload>();
            foreach (Chat chat in squad.Chat)
            {
                squadMessages.Add(new ChatPayload(chat));
            }
            return new ChatUpdate(null, null, squadMessages);
            
           
        }

        /// <summary>
        /// Send squad message 
        /// </summary>
        /// /// <param name="game_id"></param>
        /// <param name="squad_id"></param>
        /// <param name="chat"></param>
        /// <returns>Chat</returns>
        /// <response code="200">Valid request</response>
        /// <response code="400">Squad id is different from the provided chat squad id</response>
        /// <response code="401">Request was made by an unauthenticated user</response>
        /// <response code="403">Request was made by a non admin</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [HttpPost("{game_id}/squad/{squad_id}/chat")]
        public async Task<ActionResult<Chat>> SendSquadMessageAsync(int game_id, int squad_id, Chat chat)

        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            /*
            Squad squad = await _context.Squad.Include(s => s.SquadMember).ThenInclude(sm => sm.Player)
                .ThenInclude(p => p.User).FirstOrDefaultAsync(s =>
                    s.SquadMember.FirstOrDefault(sm => sm.Player.User.AspNetUserId == uid) != null);
            */

            Player player = await _context.Player.Include(p => p.User).Include(p => p.SquadMember)
                .ThenInclude(sm => sm.Squad)
                .FirstOrDefaultAsync(p => p.User.AspNetUserId == uid && p.GameId == game_id);

            if (player == null)
            {
                return NotFound();
            }
            //  check that the is a member of the squad and it's the same squad AND the squad membership is not deleted.
            var membership = player.SquadMember.FirstOrDefault(sm =>
                sm.SquadId == squad_id && sm.Squad.IsHuman == sm.Player.IsHuman && !sm.IsDeleted);
            if (membership == null)
            {
                return Forbid();
            }

            chat.Id = null;
            int id = player.Id ?? default(int);
            chat.PlayerId = id;
            chat.SquadId = squad_id;
            chat.GameId = game_id;
            chat.IsZombieGlobal = false;
            chat.IsHumanGlobal = false;
            await _context.Chat.AddAsync(chat);
            await _context.SaveChangesAsync();
            string groupName = game_id + "S" + squad_id;
            string type = "squadChat";
            
            ChatPayload payload = new ChatPayload(chat);

            var output = new { Type = type, Message = payload };
            var hubCtx = _hub.Clients.Group(groupName)
                .SendAsync("chatUpdate", output);
            //  Submit the message here
            //  Send it here
            return Created("", chat);
        }


        /// <summary>
        /// Get the squad checkins
        /// </summary>
        /// <param name="squad_id"></param>
        /// <param name="game_id"></param>
        /// <returns>List of squad checkins as an anonymous object.</returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by an un authenticated user</response>
        /// <response code="403">Request was made by a non admin user</response>
        /// <response code="404">Game was not found by the provided game id</response>
        [HttpGet("{game_id}/squad/{squad_id}/check-in")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<object>> GetSquadCheckinAsync(int squad_id, int game_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            //  that is okay.
            if (isAdmin) // need to add check for appropriate 
            {
                Game game = _context.Game.Find(game_id);

                if (game == null)
                {
                    return NotFound();
                }

                var checkins =  _context.SquadCheckin.Include(s => s.SquadMember)
                    .ThenInclude(sm => sm.Player).ThenInclude(p => p.User)
                    .Where(s => s.SquadId == squad_id && game_id == s.GameId).Select(checkin => checkin.GetMapView());
                //  not quite necessary though, but ok.
                return checkins.ToList();
            }
            //  check if player exists.
            Player p = await _context.Player.Include(player => player.User).Include(p => p.SquadMember)
                .ThenInclude(member => member.Squad)
                .ThenInclude(squad => squad.SquadCheckin)
                .FirstOrDefaultAsync(p => p.User.AspNetUserId == uid && p.GameId == game_id);

            if (p == null)
            {
                return Forbid();
            }
            //  iterate over memberships
            SquadMember squadMember = p.SquadMember.FirstOrDefault(sm => sm.SquadId == squad_id);
            if (squadMember == null) return NotFound();
            //  Check if player can currently access this.
            if (squadMember.Squad.IsHuman != p.IsHuman) return Forbid();
            return squadMember.Squad.SquadCheckin.Select(checkIn => checkIn.GetMapView()).ToList();

        }
        //  fix documentation
        /// <summary>
        /// Create a squad check-in
        /// </summary>
        /// <param name="squadCheckin"></param>
        /// <response code="201">Valid request</response>
        /// <response code="401">Request was made by an unauthenticated user</response>
        /// <response code="400">Something went very wrong...</response>
        /// /// <response code="403">A request was made by a user who should not be able to add check-ins for the selected squad</response>
        /// <returns>Squad checkin object</returns>
        [HttpPost("{game_id}/squad/{squad_id}/check-in")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<SquadCheckin>> CreateSquadCheckin(int game_id, int squad_id, SquadCheckin squadCheckin)
        {
            //  This is not even remotely sufficient.
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (squadCheckin == null) return BadRequest("Include checkIn?");
            Player p = await _context.Player.Include(p => p.User).Include(p => p.SquadMember).ThenInclude(sm => sm.Squad).FirstOrDefaultAsync(p => p.User.AspNetUserId == uid && p.GameId == game_id);
            if (p == null) return NotFound();
            SquadMember membership =
                p.SquadMember.FirstOrDefault(sm => sm.SquadId == squad_id && sm.Squad.IsHuman == p.IsHuman);
            if (membership == null) return Forbid();

            SquadCheckin input = squadCheckin;
            input.Id = null;
            input.IsDeleted = false;
            input.GameId = game_id;
            input.SquadId = squad_id;
            input.SquadMemberId = membership.Id;
            try
            {
                _context.SquadCheckin.Add(input);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            

            return Created("", "");
        }
    }
}