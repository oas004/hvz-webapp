﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HVZ_WEBAPP.Models;
using HVZ_WEBAPP;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace HvZ.Controllers
{
    [Authorize]
    [Route("api/game/")]
    [ApiController]

    public class MissionController : ControllerBase
    {
        private readonly HvZDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager; //  Get the signed in user
        public MissionController(HvZDBContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        /// <summary>
        /// Get the missions in a game
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>List of missions</returns>
        /// <response code="200">Valid request</response>
        /// <response code="400">Something went wrong</response>
        /// <response code="401">The request was made by a non authenticated user</response>
        /// <response code="403">The request was made by a non admin</response>
        [HttpGet("{game_id}/mission/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<IEnumerable<Mission>>> GetMissionAsync(int game_id, int limit = 200000, int offset = 0)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (!isAdmin)
            {

                // Search through the fk in user table to get the user and se in player if human or not
                //  if the player does not exist, 
                var is_human = (await _context.Player.FirstOrDefaultAsync(s => s.User.AspNetUserId == uid && s.GameId == game_id && !s.IsDeleted))?.IsHuman;
                if (is_human == null)
                {
                    //  if we agree that only players in the game should be able to see game details that is.
                    //  not found, big no no.
                    return Forbid();
                }
                //  If you find a match that is. 
           

                if (is_human == true)
                    return _context.Mission.ToList()
                                           .FindAll(s =>
                                                s.GameId == game_id &&
                                                s.IsHumanVisible == true && s.IsDeleted.Equals(false))
                                           .Skip(offset)
                                           .Take(limit)
                                           .ToList();
                else
                    return _context.Mission.ToList()
                                           .FindAll(s =>
                                                s.GameId == game_id &&
                                                s.IsZombieVisible == true && s.IsDeleted.Equals(false))
                                           .Skip(offset)
                                           .Take(limit)
                                           .ToList();
            }
            else
            {
                // If admin, then return all the missions
                return _context.Mission.ToList().FindAll(s => s.GameId == game_id && s.IsDeleted.Equals(false)).Skip(offset).Take(limit).ToList();
            }

        }

        /// <summary>
        /// Get a mission by id
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="mission_id"></param>
        /// <returns>Mission object</returns>
        /// <response code="200">Valid response</response>
        /// <response code="400">The mission does not exist</response>
        /// <response code="401">The request was made by a non authenticated user</response>
        /// <response code="403">The request of a zombie mission was done by a human or vice versa</response>
        [HttpGet("{game_id}/mission/{mission_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public ActionResult<Mission> GetMissionID(int game_id, int mission_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            
            // Search through the fk in user table to get the user and se in player if human or not
            int userId = _context.User.ToList().Find(s => s.AspNetUserId.Equals(uid)).Id;
            var is_human = _context.Player.ToList().Find(s => s.UserId.Equals(userId)).IsHuman;

            
            // if the user is zombie, is_human = false

            if (is_human == false)
            {
                var mission = _context.Mission.ToList().Find(s => s.GameId.Equals(game_id) && s.Id.Equals(mission_id) && s.IsDeleted.Equals(false));
                if (mission == null) return BadRequest("Mission does not exist");
                if (mission.IsHumanVisible)
                {
                    // Zoombie tries to look at human mission
                    return Forbid();
                }
                else
                {
                    // Zoombie tries to look at Zoombie mission
                    return mission;
                }
            }
            else
            {
                var mission = _context.Mission.ToList().Find(s => s.GameId.Equals(game_id) && s.Id.Equals(mission_id) && s.IsDeleted.Equals(false));
                if (mission == null) return BadRequest("Mission does not exist");
                if (mission.IsHumanVisible)
                {
                    // Human trying to look at human mission
                    return mission;
                }
                else
                {
                    // Human trying to look at zoombie mission
                    return Forbid();
                }
            }
        }

        /// <summary>
        /// Create a mission
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="mission"></param>
        /// <returns>Mission object</returns>
        /// <response code="201">Valid request</response>
        /// <response code="400">The provided game id is different from the body game id</response>
        /// <response code="401">The request was made by a non-authenticated user</response>
        /// <responee code="403">The request was not made by an admin</responee>
        [HttpPost("{game_id}/mission")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<Mission>> CreateMissionAsync(int game_id, Mission mission)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            if (isAdmin)
            {
                if (game_id == mission.GameId)
                {
                    _context.Mission.Add(mission);
                    _context.SaveChanges();
                    return Created("Mission Created", mission);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return Forbid();
            }


            //return mission;
        }
        /// <summary>
        /// Update a mission
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="mission_id"></param>
        /// <param name="mission"></param>
        /// <returns>Updated - no content</returns>
        /// <response code="204">Response success</response>
        /// <response code="400">Mission is does not match the provided mission id</response>
        /// <response code="401">Request was made by a non authenticated user</response>
        /// <response code="403">Request was made by a non admin</response>
        /// <response code="404">The mission you are trying to update does not exist</response>
        [HttpPut("{game_id}/mission/{mission_id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateMissionAsync(int game_id,int mission_id, Mission mission) // MId is mission_id
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin)
            {
                if (mission_id != mission.Id)
                {
                    return BadRequest();
                }

                var exist = _context.Mission.AsNoTracking().ToList().Find(x => x.Id == mission_id);

                if (exist == null)
                {
                    return NotFound();
                }

                _context.Entry(mission).State = EntityState.Modified;
                _context.SaveChanges();

                return NoContent();
            }
            else
            {
                return Forbid("Updates can only be done by admin");
            }
        }
        /// <summary>
        /// Delete a mission by id
        /// </summary>
        /// <param name="game_id"></param>
        /// <param name="mission_id"></param>
        /// <returns>Ok "Delete success"</returns>
        /// <response code="200">Mission was deleted</response>
        /// <response code="401">Request was made by a non authenticated user</response>
        /// <response code="403">Request was made by a non admin</response>
        /// <response code="404">Mission was not found by the provided id</response>
        [HttpDelete("{game_id}/mission/{mission_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Mission>> DeleteMissionAsync(int game_id, int mission_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin) { 
                var mission = _context.Mission.ToList().Find(x => x.Id == mission_id && x.GameId == game_id);

                if (mission == null)
                {
                    return NotFound();
                }


                mission.IsDeleted = true;
                _context.Entry(mission).State = EntityState.Modified;
                _context.SaveChanges();

                return Ok($"Mission with id: {mission.Id} was successfully deleted");
                }
            else
            {
                return Forbid("Deletes can only be done by admin");
            }
        }
    }
}