﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json.Serialization;
using HVZ_WEBAPP.Helpers;
using HVZ_WEBAPP.Migrations;
using HvZ_WEBAPP.Models;
using HVZ_WEBAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore.SqlServer.Query.Internal;


namespace HVZ_WEBAPP.Controllers
{
    [Authorize] // I utgangspunktet må brukeren vare logget inn. Bruker allow anonymous hvis ikke.
    //[Route("api/[action]/[controller]")]
    [Authorize] // I utgangspunktet må brukeren vare logget inn. Bruker allow anonymous hvis ikke.
    //[Route("api/[action]/[controller]")]
    [Route("api/game/{game_id}/player")]

    [ApiController]

    public class PlayerController : ControllerBase
    {
        private readonly HvZDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly BiteCodeGenerator biteCodeGenerator;
        public PlayerController(HvZDBContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            biteCodeGenerator = new BiteCodeGenerator("1.txt", "2.txt","first.txt", "second.txt");    //  will crash if these are wrong.
        }

        /// <summary>
        /// Returns a player from their user id
        /// </summary>
        /// <returns>Player</returns>
        /// <response code="200">Request success</response>
        /// <response code="401">User is not logged in</response>
        /// <response code="404">The user is not in a game</response>
        [Produces("application/json")]
        [HttpGet("user/{user_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Player> GetPlayer(int game_id, int user_id)
        {
            return _context.Player.ToList().Find(x => x.GameId == game_id && x.UserId == user_id);
        }

        /// <summary>
        /// Returns a list of players in a game
        /// </summary>
        /// <returns>List of players</returns>
        /// <response code="200">Request success</response>
        /// <response code="404">The player is not in the game</response>
        [Produces("application/json")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<object>>> GetPlayersAsync(int game_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            Game game = _context.Find<Game>(game_id);
            //  TODO error check
            if (game == null)
            {
                var msg = NotFound();
                return msg;
            }

            var result = _context.Player.Include(x => x.User).Where(p => p.GameId == game_id);
            return isAdmin ? result.Select(e => e.GetAdminView()).ToList() : result.Select(e => e.GetUserView()).ToList();
        }

        /// <summary>
        /// Returns a player in a game based on their player id
        /// </summary>
        /// <returns>Player object</returns>
        /// <response code="200">Request success</response>
        /// <response code="404">Player was not found in the game</response>
        [Produces("application/json")]
        [HttpGet("{player_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<object>> GetPlayerAsync(int game_id, int player_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            var player = _context.Player.Include(x => x.User)
                .FirstOrDefault(p => p.GameId == game_id && p.Id == player_id);
            if (player == null)
            {
                return new JsonResult(NotFound());
            }

            return isAdmin ? player.GetAdminView() : player.GetUserView();
        }

        /// <summary>
        /// Make a new player
        /// </summary>
        /// <returns>Player object</returns>
        /// <response code="201">Request success</response>
        /// <response code="400">The user was not found</response>
        /// <response code="400">The player something wrong with player body</response>
        /// <response code="400">The player already exists in the game</response>
        /// <response code="403">Is not admin and the gamestate is not in register</response>
        /// <response code="404">The game with the provided id does not exist</response>
        [Produces("application/json")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<object>> PostPlayer(int game_id, Player player)
        {
            //TODO:  Authenticate etc.
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            bool isAdmin = false;
            //  Get user since the User needs and UID to access this endpoint
            User user = await _context.User.Include(u => u.Player).FirstOrDefaultAsync(u => u.AspNetUserId == uid);
            if (user == null)
            {
                return BadRequest("The user was not found.");
            }
            Game game = await _context.Game.Include(g => g.Player).ThenInclude(p => p.User).FirstOrDefaultAsync(g => g.Id == game_id);
            if (game == null) return NotFound("The selected game does not exist.");
            if (player == null)
            {
                return BadRequest("Expected player object.");
                //  Check if the user actually exists in the database.
            }
            
            if (game.GameState > 0)
            {
                //  Game is in progress check if user is admin.
                var aspnetUser = await _userManager.FindByIdAsync(uid);
                isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
                if (isAdmin)
                {
                    
                    //  handle insert
                }
                else
                {
                    return Forbid();
                }
            }
            //  At this point we can assume that either a user or admin is registering a user.

            Player duplicateCheck = game.Player.FirstOrDefault(p => p.UserId == user.Id);
            if (duplicateCheck != null)
            {
                return BadRequest("Player already exists in game");
            }

            player.IsHuman = player.IsHuman ? player.IsHuman : false;
            player.GameId = game_id;
            player.UserId = user.Id;
            player.Id = null;   //  auto increments if the user set the id.
            await _context.Player.AddAsync(player);
            await _context.SaveChangesAsync();
            //  Handle nullable int to int
            int refId = player.Id ?? default(int);
            //  Retrieve Id and generate biteCode.
            //  refactor this shit.
            player.BiteCode = biteCodeGenerator.GetBiteCode(refId, game_id);
            _context.Entry(player).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            //TODO: REFACTOR THIS BOIS.
            
            return isAdmin ? player.GetAdminView() :  player.GetUserView();
        }

        /// <summary>
        /// Updates a player object
        /// </summary>
        /// <returns>New player object</returns>
        /// <response code="200">Update success</response>
        /// <response code="400">The provided player body could not be accepted</response>
        /// <response code="403">The request was attempted by a non admin</response>
        /// <response code="404">The player was not found from the provided player_id</response>
        [Produces("application/json")]
        [HttpPut("{player_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<object>> PutPlayerAsync(int game_id, int player_id, Player player)
        {

            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin)
            {
                Game game = _context.Game.Include(g => g.Player).FirstOrDefault(g => g.Id == game_id);
                if (game == null) return NotFound();
                if (player == null)
                {
                    return BadRequest();
                }
                Player entityPlayer = _context.Player.Include(u => u.User).FirstOrDefault(p => p.Id == player_id);
                if (entityPlayer == null)
                {
                    return NotFound();
                }

                entityPlayer.BiteCode = player.BiteCode;
                entityPlayer.GameId = player.GameId;
                entityPlayer.IsDeleted = player.IsDeleted;
                entityPlayer.Rank = player.Rank;
                entityPlayer.IsHuman = player.IsHuman;
                entityPlayer.IsPatientZero = player.IsPatientZero;
                _context.Entry(entityPlayer).State = EntityState.Modified;
                _context.SaveChanges();
                return entityPlayer;
            }
            return Forbid("Only Admin can update player");
        }

        /// <summary>
        /// Delete a player by id
        /// </summary>
        /// <returns>Accepted</returns>
        /// <response code="202">Delete was accepted</response>
        /// <response code="403">Request was attempted by a non-admin</response>
        /// <response code="404">Game could not be found by the provided game id</response>
        [Produces("application/json")]
        [HttpDelete("{player_id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<object>> DeletePlayerAsync(int game_id, int player_id)
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            
            if (isAdmin) { 

                //  export this to a heloer class for god's sake.
                Game game = _context.Game.Include(g => g.Player).FirstOrDefault(g => g.Id == game_id);
                if (game == null) return NotFound();
                Player player = game.Player.FirstOrDefault(p => p.Id == player_id);
                //  Set cascading flags, jfc that will be a chonker.
                //  TODO:
                return Accepted();

            } 
                return Forbid("Only admin can delete a player");
            
        }
    }



}