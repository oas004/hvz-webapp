﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HVZ_WEBAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HVZ_WEBAPP.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;


        public AdminController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Check if the logged in user is an admin
        /// </summary>
        /// <returns>Boolean true/false</returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by an unauthenticated user</response>
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        
        
        public async Task<ActionResult<Boolean>> IsAdmin()
        {
            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (uid == null) return false;

            // See if the user is admin
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            if (isAdmin)
            {
                return true;
                //return true;
            }
            else
            {
                return false;
                //return false;
            }
        }
    }
}