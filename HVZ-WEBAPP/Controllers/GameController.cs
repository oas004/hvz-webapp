﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HVZ_WEBAPP.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using HVZ_WEBAPP.HubConfig;
using Microsoft.AspNetCore.SignalR;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;


namespace HvZ.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class GameController : ControllerBase
    {
        //  chat hub
        private  IHubContext<ChatHub> _hub { get; }
        
        private readonly HvZDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager; // Getting the aspnet user db context
        public GameController(HvZDBContext context, UserManager<ApplicationUser> userManager, IHubContext<ChatHub> hub)
        {
            _context = context;
            _hub = hub;
            _userManager = userManager;
        }

        /// <summary>
        /// Get the list of current games
        /// </summary>
        /// <returns>A list of games</returns>
        [Produces("application/json")]

        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<object>> GetGame()
        {
            var uid = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var list = await _context.Game.Include("Player").Where(g => !g.IsDeleted).ToListAsync();
            if (uid == null)
            {
                //  Not registered.
                return  list.Select(g => g.GetBaseView());
            }
            else
            {
                var aspnetUser = await _userManager.FindByIdAsync(uid);
                bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
                if (isAdmin) return list.Select(g => g.GetAdminView());
                await _context.DisposeAsync();
                return list.Select(g =>g.GetUserView());
            }
            
        }
        /// <summary>
        /// Get a game
        /// </summary>
        /// <returns>A game</returns>
        [Produces("application/json")]
        [AllowAnonymous]
        [HttpGet("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async  Task<ActionResult<GameListPayload>> GetGameID(int Id)
        {

            var uid = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (uid == null)
            {
                return Forbid();
            }
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            Game g = await _context.Game.FirstOrDefaultAsync(s => s.Id == Id);
            if (isAdmin) return g?.GetAdminView();
            return g?.GetUserView();

        }
        /// <summary>
        /// Admin Only, create a game
        /// </summary>
        /// <returns>A newly created game</returns>
        /// <response code="201">Returns the newly created game</response>
        /// <response code="403">Not Admin</response>
        [Produces("application/json")]
        //Admin only
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<Game>> CreateGameAsync(Game game)
        {
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin)
            {
                _context.Game.Add(game);
                _context.SaveChanges();
                return Created("Game created", game);
            }
            else
            {
                return Forbid();
            }
        }
        /// <summary>
        /// Only admin can update a game
        /// </summary>
        /// <returns>No Content</returns>
        /// <response code="400">Url game id is different from body game id</response>
        /// <response code="404">Game with game id was not found</response>
        /// <response code="204">Update success</response>
        /// <response code="403">The user is not an admin</response>
        [HttpPut("{Id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> UpdateGameAsync(int Id, Game game)
        {
            
            var uid = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");
            if (isAdmin)
            {
                Console.WriteLine("User is admin");
                if (Id != game.Id)
                {
                    return BadRequest();
                }

                var ca = _context.Game.AsNoTracking().ToList().Find(x => x.Id == Id);


                if (ca == null)
                {
                    return NotFound();
                }

                _context.Entry(game).State = EntityState.Modified;
                _context.SaveChanges();
                return NoContent();
            }
           
            return Forbid();
            
        }

        /// <summary>
        /// Get player for current game if there is one.
        /// </summary>
        /// <param name="game_id">Id of game</param>
        /// <returns> Returns player object if there is one, or null.</returns>
        
        [HttpGet("{game_id}/me")]
        
        public async Task<ActionResult<object>> GetMe(int game_id) 
        {
            //  get me.
            var uid = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (uid == null) return NotFound("User not logged in or not found.");

            var user = await _context.User.Include(u => u.Player)
                .ThenInclude(p => p.SquadMember).ThenInclude(sm => sm.Squad).FirstOrDefaultAsync(u => uid == u.AspNetUserId);
            Player p = user.Player.FirstOrDefault(p => game_id == p.GameId);
            return p?.GetChatView();

        }

        /// <summary>
        /// Admin only - Delete a game
        /// </summary>
        /// <returns>No Content</returns>
        /// <response code="403">User is not an admin</response>
        /// <response code="404">The game with the game id was not found</response>
        /// <response code="204">The game was deleted</response>

        [HttpDelete("{Id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> DeleteAGame(int Id)
        {
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var aspnetUser = await _userManager.FindByIdAsync(uid);
            bool isAdmin = await _userManager.IsInRoleAsync(aspnetUser, "Administrator");

            if (isAdmin)
            {
                // make a soft delete on the game in question
                var gameToBe = _context.Game.ToList().Find(x => x.Id == Id);

                if(gameToBe == null)
                {
                    return NotFound("Game Not Found");
                }
                gameToBe.IsDeleted = true;
                _context.SaveChanges();
                return NoContent();
            }
            else
            {
                return Forbid("Only an admin can delete a game!");
            }
        }

        /// <summary>
        /// Get the chat of a game
        /// </summary>
        /// <returns>A list chat objects</returns>
        /// <response code="200">Valid response</response>
        [Produces("application/json")]

        [HttpGet("{game_id}/chat")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async  Task<ActionResult<ChatUpdate>> GetChat(int game_id)
        {
            //  check if game exists
            //  do authentication etc
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            Player tmp = await _context.Player.Include(p => p.User).FirstOrDefaultAsync(p => p.User.AspNetUserId == uid);
            if (tmp == null) return Forbid();
            int player_id = tmp.Id?? default(int);  //  placeholder Need to check users fro players and then find the player that is member of this game.


            Player p = await _context.Player.Include(p => p.SquadMember).FirstOrDefaultAsync(p => p.Id == player_id);
            var messages = await _context.Chat.Include(p => p.Player).ThenInclude(p => p.User)
                .Where(chat => chat.GameId == game_id && chat.SquadId == null && !chat.IsDeleted).ToListAsync();

            List<ChatPayload> GlobalChat = new List<ChatPayload>();
            List<ChatPayload> FactionChat = new List<ChatPayload>();
            List<ChatPayload> SquadChat = new List<ChatPayload>();//    Wrong casing.
            messages.ForEach(message =>
            {
                
                if (message.IsZombieGlobal == message.IsHumanGlobal)
                {
                    GlobalChat.Add(new ChatPayload(message));
                }
                else
                {
                    FactionChat.Add(new ChatPayload(message));
                }

            });
            
            //  Now do some magic here, maybe add a new type.
            
            ChatUpdate ret = new ChatUpdate(GlobalChat, FactionChat, SquadChat);
            return new ActionResult<ChatUpdate>(ret);
           
        }
        /// <summary>
        /// Post a new chat message
        /// </summary>
        /// <returns>A created chat message</returns>
        /// <response code="201">Chat message posted</response>
        /// <response code="403">The is for squad</response>
        /// <response code="400">Chatmessage from the json body is null</response>
        [Produces("application/json")]
        [HttpPost("{game_id}/chat")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Chat>> PostChat(int game_id, ChatPayload chatMessage)
        {
            if (chatMessage == null)
            {
                return BadRequest();
            } else if (chatMessage.IsSquad)
            {
                return Forbid();
            }

            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            //  two checks?
            Player player = await _context.Player.Include(p => p.User).FirstOrDefaultAsync(p => p.User.AspNetUserId == uid && p.GameId == game_id);
           
            if (player == null || player.GameId != game_id) return Forbid();

            Chat chatInput = chatMessage.toChat();
            chatInput.PlayerId = player.Id ?? default(int);
            chatInput.GameId = game_id;
            chatInput.ChatTime = DateTime.Now;
            //  Faction chat!
            if (!chatInput.IsZombieGlobal  && !chatInput.IsHumanGlobal)
            {
                //  faction chat, the server needs to check the faction of the player.
                if (player.IsHuman)
                {
                    chatInput.IsHumanGlobal = true;
                }
                else
                {
                    chatInput.IsZombieGlobal = true;
                }
            } 
           
            _context.Chat.Add((chatInput));
            _context.SaveChanges();
            // Connection is not reset, since we refresh we can drop the connection
            //  Now that this is added we can pass the chatInput to the users.
            string groupName = "";
            string type = "";
            if (chatInput.IsZombieGlobal == chatInput.IsHumanGlobal)
            {
                //  global;
                groupName = "" + game_id + "G";
                type = "globalChat";
            }
            else
            {
                type = "factionChat";
                groupName = "" + game_id + 'F' + chatInput.IsHumanGlobal;   //  set to faction group.
            }
            ChatPayload payload = new ChatPayload(chatInput);
            
            var output = new {Type = type, Message = payload };
            
            _hub.Clients.Group(groupName).SendAsync("chatUpdate", output);
            return Created("asd", chatInput);
        }
        //  signalR test
        public string GetUid()
        {
            return this.User.Identity.Name;
        }
    }

}
   