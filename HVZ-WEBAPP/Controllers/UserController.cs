﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HvZ_WEBAPP.Models;
using HVZ_WEBAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HVZ_WEBAPP.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly HvZDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager;


        public UserController(HvZDBContext context, UserManager<ApplicationUser> usermanager)
        {
            _context = context;
            _userManager = usermanager;


        }

        /// <summary>
        /// Get the logged in user
        /// </summary>
        /// <returns>User object</returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by a non authenticated user</response>
        /// <response code="400">Something unexpected happend</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<User>> GetUser()
        {

            // Get the uid from the aspnetuser
            var uid = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var user = _context.User.ToList().Find(x => x.AspNetUserId == uid);

            return user;
        }

        /// <summary>
        /// Get the user object by id
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// <response code="200">Valid request</response>
        /// <response code="401">Request was made by an unauthenticated user</response>
        /// <response code="400">Something unexpected happend</response>
        [HttpGet("{userid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<User> GetUserById(int userid)
        {
            return _context.User.ToList().Find(x => x.Id == userid);
        }
    }
}