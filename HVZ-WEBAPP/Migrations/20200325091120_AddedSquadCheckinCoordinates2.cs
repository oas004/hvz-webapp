﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class AddedSquadCheckinCoordinates2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "is_deleted",
                table: "Mission",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",   //  what the fuck?
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "lat",
                table: "Mission",
                type: "decimal(8, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 6)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "lat",
                table: "Mission",
                type: "decimal(8, 6)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "is_deleted",
                table: "Mission",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
