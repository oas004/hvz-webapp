﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class SettingUniqueConstraints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "username",
                table: "User",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_User_asp_net_user_id",
                table: "User",
                column: "asp_net_user_id");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_User_username",
                table: "User",
                column: "username");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_User_asp_net_user_id",
                table: "User");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_User_username",
                table: "User");

            migrationBuilder.AlterColumn<string>(
                name: "username",
                table: "User",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
