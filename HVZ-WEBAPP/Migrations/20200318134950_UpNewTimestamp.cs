﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class UpNewTimestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "chat_time",
                table: "Chat",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "chat_time",
                table: "Chat");
        }
    }
}
