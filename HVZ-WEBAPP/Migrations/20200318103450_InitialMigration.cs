﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

        //    migrationBuilder.CreateTable(
        //        name: "Game",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            name = table.Column<string>(fixedLength: true, nullable: false),
        //            game_state = table.Column<byte>(nullable: false),
        //            nw_lat = table.Column<decimal>(type: "decimal(8, 6)", nullable: true),
        //            nw_lng = table.Column<decimal>(type: "decimal(9, 6)", nullable: true),
        //            se_lat = table.Column<decimal>(type: "decimal(8, 6)", nullable: true),
        //            se_lng = table.Column<decimal>(type: "decimal(9, 6)", nullable: true),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Game", x => x.Id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "User",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            first_name = table.Column<string>(fixedLength: true, nullable: false),
        //            last_name = table.Column<string>(fixedLength: true, nullable: false),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_User", x => x.Id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Mission",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            name = table.Column<string>(fixedLength: true, nullable: false),
        //            is_human_visible = table.Column<bool>(nullable: false),
        //            is_zombie_visible = table.Column<bool>(nullable: false),
        //            description = table.Column<string>(fixedLength: true, maxLength: 128, nullable: true),
        //            start_time = table.Column<DateTime>(type: "date", nullable: true),
        //            end_time = table.Column<DateTime>(type: "date", nullable: true),
        //            game_id = table.Column<int>(nullable: false),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Mission", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_Mission_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Squad",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            name = table.Column<string>(fixedLength: true, nullable: false),
        //            is_human = table.Column<bool>(nullable: false),
        //            game_id = table.Column<int>(nullable: false),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Squad", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_Squad_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Player",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            is_human = table.Column<bool>(nullable: false),
        //            is_patient_zero = table.Column<bool>(nullable: false),
        //            bite_code = table.Column<string>(fixedLength: true, maxLength: 10, nullable: false),
        //            user_id = table.Column<int>(nullable: false),
        //            game_id = table.Column<int>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Player", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_Player_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_Player_User",
        //                column: x => x.user_id,
        //                principalTable: "User",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Chat",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            message = table.Column<string>(fixedLength: true, nullable: false),
        //            is_human_global = table.Column<bool>(nullable: false),
        //            is_zombie_global = table.Column<bool>(nullable: false),
        //            chat_time = table.Column<DateTime>(type: "datetime2", nullable: false),
        //            game_id = table.Column<int>(nullable: false),
        //            player_id = table.Column<int>(nullable: false),
        //            squad_id = table.Column<int>(nullable: true),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Chat", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_Chat_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_Chat_Player",
        //                column: x => x.player_id,
        //                principalTable: "Player",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_Chat_Squad",
        //                column: x => x.squad_id,
        //                principalTable: "Squad",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Kill",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            time_of_death = table.Column<DateTime>(type: "datetime", nullable: false),
        //            story = table.Column<string>(fixedLength: true, nullable: true),
        //            lat = table.Column<decimal>(type: "decimal(8, 6)", nullable: true),
        //            lng = table.Column<decimal>(type: "decimal(9, 6)", nullable: true),
        //            game_id = table.Column<int>(nullable: false),
        //            killer_id = table.Column<int>(nullable: false),
        //            victim_id = table.Column<int>(nullable: false),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Kill", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_Kill_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_Kill_Player_Killer",
        //                column: x => x.killer_id,
        //                principalTable: "Player",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_Kill_Player_Victim",
        //                column: x => x.victim_id,
        //                principalTable: "Player",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "SquadMember",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            rank = table.Column<int>(nullable: false),
        //            game_id = table.Column<int>(nullable: false),
        //            squad_id = table.Column<int>(nullable: false),
        //            player_id = table.Column<int>(nullable: false),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_SquadMember", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_SquadMember_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_SquadMember_Player",
        //                column: x => x.player_id,
        //                principalTable: "Player",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_SquadMember_Squad",
        //                column: x => x.squad_id,
        //                principalTable: "Squad",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "SquadCheckin",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            start_time = table.Column<DateTime>(type: "datetime", nullable: false),
        //            end_time = table.Column<DateTime>(type: "datetime", nullable: false),
        //            lat = table.Column<decimal>(type: "decimal(8, 6)", nullable: false),
        //            lng = table.Column<decimal>(type: "decimal(9, 6)", nullable: false),
        //            game_id = table.Column<int>(nullable: false),
        //            squad_id = table.Column<int>(nullable: false),
        //            squad_member_id = table.Column<int>(nullable: false),
        //            is_deleted = table.Column<bool>(nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_SquadCheckin", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_SquadCheckin_Game",
        //                column: x => x.game_id,
        //                principalTable: "Game",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_SquadCheckin_Squad",
        //                column: x => x.squad_id,
        //                principalTable: "Squad",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //            table.ForeignKey(
        //                name: "FK_SquadCheckin_SquadMember",
        //                column: x => x.squad_member_id,
        //                principalTable: "SquadMember",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Restrict);
        //        });

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Chat_game_id",
        //        table: "Chat",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Chat_player_id",
        //        table: "Chat",
        //        column: "player_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Chat_squad_id",
        //        table: "Chat",
        //        column: "squad_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Kill_game_id",
        //        table: "Kill",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Kill_killer_id",
        //        table: "Kill",
        //        column: "killer_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Kill_victim_id",
        //        table: "Kill",
        //        column: "victim_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Mission_game_id",
        //        table: "Mission",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Player_game_id",
        //        table: "Player",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Player_user_id",
        //        table: "Player",
        //        column: "user_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Squad_game_id",
        //        table: "Squad",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SquadCheckin_game_id",
        //        table: "SquadCheckin",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SquadCheckin_squad_id",
        //        table: "SquadCheckin",
        //        column: "squad_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SquadCheckin_squad_member_id",
        //        table: "SquadCheckin",
        //        column: "squad_member_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SquadMember_game_id",
        //        table: "SquadMember",
        //        column: "game_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SquadMember_player_id",
        //        table: "SquadMember",
        //        column: "player_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SquadMember_squad_id",
        //        table: "SquadMember",
        //        column: "squad_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "Chat");

            //migrationBuilder.DropTable(
            //    name: "Kill");

            //migrationBuilder.DropTable(
            //    name: "Mission");

            //migrationBuilder.DropTable(
            //    name: "SquadCheckin");

            //migrationBuilder.DropTable(
            //    name: "SquadMember");

            //migrationBuilder.DropTable(
            //    name: "Player");

            //migrationBuilder.DropTable(
            //    name: "Squad");

            //migrationBuilder.DropTable(
            //    name: "User");

            //migrationBuilder.DropTable(
            //    name: "Game");
        }
    }
}
