﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class MissionCoordinates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.AddColumn<decimal>(
                name: "se_lat",
                table: "Mission",
                type: "decimal(8, 6)",
                nullable: true,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "nw_lng",
                table: "Mission",
                type: "decimal(9, 6)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.DropColumn(
                name: "se_lat",
                table: "Mission");

            migrationBuilder.DropColumn(
                name: "nw_lng",
                table: "Mission");

            
        }
    }
}
