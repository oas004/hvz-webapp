﻿// <auto-generated />
using System;
using HVZ_WEBAPP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HVZ_WEBAPP.Migrations
{
    [DbContext(typeof(HvZDBContext))]
    [Migration("20200318133818_FixDateTime2")]
    partial class FixDateTime2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("HVZ_WEBAPP.Models.Chat", b =>
                {
                    b.Property<int?>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<byte[]>("ChatTime")
                        .IsConcurrencyToken()
                        .IsRequired()
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnName("chat_time")
                        .HasColumnType("rowversion");

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<bool>("IsHumanGlobal")
                        .HasColumnName("is_human_global")
                        .HasColumnType("bit");

                    b.Property<bool>("IsZombieGlobal")
                        .HasColumnName("is_zombie_global")
                        .HasColumnType("bit");

                    b.Property<string>("Message")
                        .IsRequired()
                        .HasColumnName("message")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.Property<int>("PlayerId")
                        .HasColumnName("player_id")
                        .HasColumnType("int");

                    b.Property<int?>("SquadId")
                        .HasColumnName("squad_id")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("PlayerId");

                    b.HasIndex("SquadId");

                    b.ToTable("Chat");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Game", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<byte>("GameState")
                        .HasColumnName("game_state")
                        .HasColumnType("tinyint");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.Property<decimal?>("NwLat")
                        .HasColumnName("nw_lat")
                        .HasColumnType("decimal(8, 6)");

                    b.Property<decimal?>("NwLng")
                        .HasColumnName("nw_lng")
                        .HasColumnType("decimal(9, 6)");

                    b.Property<decimal?>("SeLat")
                        .HasColumnName("se_lat")
                        .HasColumnType("decimal(8, 6)");

                    b.Property<decimal?>("SeLng")
                        .HasColumnName("se_lng")
                        .HasColumnType("decimal(9, 6)");

                    b.HasKey("Id");

                    b.ToTable("Game");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Kill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<int>("KillerId")
                        .HasColumnName("killer_id")
                        .HasColumnType("int");

                    b.Property<decimal?>("Lat")
                        .HasColumnName("lat")
                        .HasColumnType("decimal(8, 6)");

                    b.Property<decimal?>("Lng")
                        .HasColumnName("lng")
                        .HasColumnType("decimal(9, 6)");

                    b.Property<string>("Story")
                        .HasColumnName("story")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.Property<DateTime>("TimeOfDeath")
                        .HasColumnName("time_of_death")
                        .HasColumnType("datetime");

                    b.Property<int>("VictimId")
                        .HasColumnName("victim_id")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("KillerId");

                    b.HasIndex("VictimId");

                    b.ToTable("Kill");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Mission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("nchar(128)")
                        .IsFixedLength(true)
                        .HasMaxLength(128);

                    b.Property<DateTime?>("EndTime")
                        .HasColumnName("end_time")
                        .HasColumnType("date");

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<bool>("IsHumanVisible")
                        .HasColumnName("is_human_visible")
                        .HasColumnType("bit");

                    b.Property<bool>("IsZombieVisible")
                        .HasColumnName("is_zombie_visible")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.Property<DateTime?>("StartTime")
                        .HasColumnName("start_time")
                        .HasColumnType("date");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.ToTable("Mission");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Player", b =>
                {
                    b.Property<int?>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("BiteCode")
                        .IsRequired()
                        .HasColumnName("bite_code")
                        .HasColumnType("nchar(10)")
                        .IsFixedLength(true)
                        .HasMaxLength(10);

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsHuman")
                        .HasColumnName("is_human")
                        .HasColumnType("bit");

                    b.Property<bool>("IsPatientZero")
                        .HasColumnName("is_patient_zero")
                        .HasColumnType("bit");

                    b.Property<int>("UserId")
                        .HasColumnName("user_id")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("UserId");

                    b.ToTable("Player");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Squad", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<bool>("IsHuman")
                        .HasColumnName("is_human")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.ToTable("Squad");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.SquadCheckin", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EndTime")
                        .HasColumnName("end_time")
                        .HasColumnType("datetime");

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<decimal>("Lat")
                        .HasColumnName("lat")
                        .HasColumnType("decimal(8, 6)");

                    b.Property<decimal>("Lng")
                        .HasColumnName("lng")
                        .HasColumnType("decimal(9, 6)");

                    b.Property<int>("SquadId")
                        .HasColumnName("squad_id")
                        .HasColumnType("int");

                    b.Property<int>("SquadMemberId")
                        .HasColumnName("squad_member_id")
                        .HasColumnType("int");

                    b.Property<DateTime>("StartTime")
                        .HasColumnName("start_time")
                        .HasColumnType("datetime");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("SquadId");

                    b.HasIndex("SquadMemberId");

                    b.ToTable("SquadCheckin");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.SquadMember", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("GameId")
                        .HasColumnName("game_id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<int>("PlayerId")
                        .HasColumnName("player_id")
                        .HasColumnType("int");

                    b.Property<int>("Rank")
                        .HasColumnName("rank")
                        .HasColumnType("int");

                    b.Property<int>("SquadId")
                        .HasColumnName("squad_id")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("PlayerId");

                    b.HasIndex("SquadId");

                    b.ToTable("SquadMember");
                });

            modelBuilder.Entity("HvZ_WEBAPP.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AspNetUserId")
                        .IsRequired()
                        .HasColumnName("asp_net_user_id")
                        .HasColumnType("nvarchar(450)")
                        .HasMaxLength(450);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnName("first_name")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.Property<bool>("IsDeleted")
                        .HasColumnName("is_deleted")
                        .HasColumnType("bit");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnName("last_name")
                        .HasColumnType("nvarchar(max)")
                        .IsFixedLength(true);

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnName("username")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasAlternateKey("AspNetUserId");

                    b.HasAlternateKey("Username");

                    b.ToTable("User");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Chat", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("Chat")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_Chat_Game")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Player", "Player")
                        .WithMany("Chat")
                        .HasForeignKey("PlayerId")
                        .HasConstraintName("FK_Chat_Player")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Squad", "Squad")
                        .WithMany("Chat")
                        .HasForeignKey("SquadId")
                        .HasConstraintName("FK_Chat_Squad");
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Kill", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("Kill")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_Kill_Game")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Player", "Killer")
                        .WithMany("KillKiller")
                        .HasForeignKey("KillerId")
                        .HasConstraintName("FK_Kill_Player_Killer")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Player", "Victim")
                        .WithMany("KillVictim")
                        .HasForeignKey("VictimId")
                        .HasConstraintName("FK_Kill_Player_Victim")
                        .IsRequired();
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Mission", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("Mission")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_Mission_Game")
                        .IsRequired();
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Player", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("Player")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_Player_Game")
                        .IsRequired();

                    b.HasOne("HvZ_WEBAPP.Models.User", "User")
                        .WithMany("Player")
                        .HasForeignKey("UserId")
                        .HasConstraintName("FK_Player_User")
                        .IsRequired();
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.Squad", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("Squad")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_Squad_Game")
                        .IsRequired();
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.SquadCheckin", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("SquadCheckin")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_SquadCheckin_Game")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Squad", "Squad")
                        .WithMany("SquadCheckin")
                        .HasForeignKey("SquadId")
                        .HasConstraintName("FK_SquadCheckin_Squad")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.SquadMember", "SquadMember")
                        .WithMany("SquadCheckin")
                        .HasForeignKey("SquadMemberId")
                        .HasConstraintName("FK_SquadCheckin_SquadMember")
                        .IsRequired();
                });

            modelBuilder.Entity("HVZ_WEBAPP.Models.SquadMember", b =>
                {
                    b.HasOne("HVZ_WEBAPP.Models.Game", "Game")
                        .WithMany("SquadMember")
                        .HasForeignKey("GameId")
                        .HasConstraintName("FK_SquadMember_Game")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Player", "Player")
                        .WithMany("SquadMember")
                        .HasForeignKey("PlayerId")
                        .HasConstraintName("FK_SquadMember_Player")
                        .IsRequired();

                    b.HasOne("HVZ_WEBAPP.Models.Squad", "Squad")
                        .WithMany("SquadMember")
                        .HasForeignKey("SquadId")
                        .HasConstraintName("FK_SquadMember_Squad")
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
