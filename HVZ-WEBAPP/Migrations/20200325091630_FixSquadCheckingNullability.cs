﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class FixSquadCheckingNullability : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "lng",
                table: "SquadCheckin",
                type: "decimal(9, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(9, 6)");

            migrationBuilder.AlterColumn<decimal>(
                name: "lat",
                table: "SquadCheckin",
                type: "decimal(8, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 6)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "lng",
                table: "SquadCheckin",
                type: "decimal(9, 6)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(9, 6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "lat",
                table: "SquadCheckin",
                type: "decimal(8, 6)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 6)",
                oldNullable: true);
        }
    }
}
