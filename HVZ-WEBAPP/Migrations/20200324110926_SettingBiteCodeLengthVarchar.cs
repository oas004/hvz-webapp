﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class SettingBiteCodeLengthVarchar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*
            migrationBuilder.RenameColumn(
                name: "StartTime",
                table: "Game",
                newName: "start_time");

            migrationBuilder.RenameColumn(
                name: "EndTime",
                table: "Game",
                newName: "end_time");
                */
            migrationBuilder.AlterColumn<string>(
                name: "bite_code",
                table: "Player",
                fixedLength: true,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nchar(20)",
                oldFixedLength: true,
                oldMaxLength: 20,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            /*
            migrationBuilder.RenameColumn(
                name: "start_time",
                table: "Game",
                newName: "StartTime");

            migrationBuilder.RenameColumn(
                name: "end_time",
                table: "Game",
                newName: "EndTime");
                */
            migrationBuilder.AlterColumn<string>(
                name: "bite_code",
                table: "Player",
                type: "nchar(20)",
                fixedLength: true,
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldFixedLength: true,
                oldNullable: true);
        }
}
}
