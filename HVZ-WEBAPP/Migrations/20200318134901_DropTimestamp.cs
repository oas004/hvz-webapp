﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HVZ_WEBAPP.Migrations
{
    public partial class DropTimestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "chat_time",
                table: "Chat");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "chat_time",
                table: "Chat",
                type: "rowversion",
                rowVersion: true,
                nullable: false,
                defaultValue: new byte[] {  });
        }
    }
}
