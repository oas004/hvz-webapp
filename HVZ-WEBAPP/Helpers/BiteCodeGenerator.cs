﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Language.Intermediate;

namespace HVZ_WEBAPP.Helpers
{
    public class BiteCodeGenerator
    {
        private List<List<string>> partContainer = new List<List<string>>();
        public BiteCodeGenerator(params string[] filenames)
        {
            var assembly = Assembly.GetExecutingAssembly();
            foreach (var filename in filenames)
            {
               
                string resourceName = assembly.GetManifestResourceNames()
                    .Single(str => str.EndsWith(filename));
                using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {

                    List<string> li;
                    partContainer.Add(li = new List<string>());
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        //  Write to string to avoid buffering issues, IO is slow.
                        string content = reader.ReadToEnd();
                        using (StringReader strReader = new StringReader(content))
                        {
                            string line = "";
                            do
                            {
                                line = strReader.ReadLine();
                                if (line != null)
                                {
                                    li.Add(line);
                                }
                            } while (line != null);
                        }
                    }
                }
            }
        }

        public string GetBiteCode(params int[] indexes)
        {
            string outStr = "";
            for (int i = 0; i < indexes.Count(); i++)
            {
                outStr += partContainer[i][indexes[i]];
            }
            //  quick fix
            return Guid.NewGuid().ToString();
            //return outStr;
        }
    }
}
