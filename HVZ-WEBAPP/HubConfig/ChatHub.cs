﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using HvZ.Controllers;
using HvZ_WEBAPP.Models;
using HVZ_WEBAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Any;
using Newtonsoft.Json;

namespace HVZ_WEBAPP.HubConfig
{
    
    public class StateHandler
    {
        public  HashSet<string> ConnectedIds = new HashSet<string>();
        //  GameId subscribers.
       
        public Dictionary<string, string> uidConnectionId = new Dictionary<string, string>();
        public Dictionary<string, string> currentMemberships = new Dictionary<string, string>();
        //  Whatever, send out everyting and handle it client-side.
        //  Bind uid to connectionId
        //  Pass uid and update
        //  Depending on message push to selected uids.
        //  

        /**
         * If we filter out the recipients we need to walk through the db at least once, overhead.
         * Having subscriptions to a game is a bit easier.
         */
    }


    public class SubscriptionType
    {
        public string Type { get; set; }
        public int ? GameId { get; set; }

        public bool IsHuman { get; set; }
        public int ? SquadId { get; set; }
    }

    [Authorize(Policy = "SignalR")]
    public class ChatHub : Hub
    {
        private HvZDBContext _context;

        public ChatHub(HvZDBContext context)
        {
            _context = context;
        }
        public async Task Subscribe(SubscriptionType subscription)
        {
            //  Here we need to be smart and should probably obfuscate the chat subscriptions.
            var uid = Context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (uid == null) return;
            User u = await _context.User.Include(u => u.Player).ThenInclude(p => p.SquadMember).ThenInclude(sm => sm.Squad)
                .FirstOrDefaultAsync(u => u.AspNetUserId == uid);
            Player p = u.Player.FirstOrDefault(p => p.GameId == subscription.GameId);
            if (p == null) return;  //  Invalid, cannot subscribe to this.
            string groupName = "";
            switch (subscription.Type)
            {
                case "Global":
                    groupName = "" + subscription.GameId + "G";   //  allowed
                    break;
                case "Faction":
                    if (p.IsHuman == subscription.IsHuman)
                    {
                        groupName = "" + subscription.GameId + 'F' + subscription.IsHuman;  //  IDF0-1  //  groups are created here bois.
                    }
                    break;
                case "Squad":
                    if(subscription.SquadId == null) break;
                    SquadMember sm = p.SquadMember.FirstOrDefault(sm =>
                        sm.SquadId == subscription.SquadId && sm.Squad.IsHuman == p.IsHuman);
                    if (sm == null) break;
                    groupName = "" + subscription.GameId + 'S' + sm.SquadId;
                    //  Needs to refresh or the squad member will receive both on death.
                    break;
            }

            if (groupName == "") return;
            //  turned zombois needs to be refreshed or dropped from the groups yo.
            //  now we need to evaluate the context
            Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            
        }

        public override Task OnConnectedAsync()
        {
            var uid = Context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (uid == null)
            {
                return base.OnConnectedAsync();
            }
            
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
           
            return base.OnDisconnectedAsync(exception);
        }

    }

   
    /*
    public class ChatHub : Hub
    {

        private readonly UserManager<ApplicationUser> _userManager; // Getting the aspnet user db context
        private StateHandler stateHandler = new StateHandler();

        public ChatHub(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;

        }
        public async Task<IActionResult> AddToGroup(string connectionId,string groupName)
        {

            await Groups.AddToGroupAsync(connectionId, groupName);
            return ""
        }

        public async Task RemoveFromGroup(string connectionId, string groupName)
        {
            await Groups.RemoveFromGroupAsync(connectionId, groupName);
        }

        public async Task BroadcastMessage(Chat msg, string game_id)
        {
            //  Broadcast to group?
            Clients.Group(game_id).SendAsync("chatUpdate", JsonConvert.SerializeObject(msg));

        }

        public void AddPlayer(string uid, string game_id)
        {
            
            string match = null;
            
            string connectionId;
            stateHandler.uidConnectionId.TryGetValue(uid, out connectionId);
            if (connectionId == null) return;
            
            AddToGroup(connectionId, game_id);
            var tmp = stateHandler.currentMemberships[uid];
            if (tmp != null)
            {
                RemoveFromGroup(connectionId, tmp);
            }

            stateHandler.currentMemberships[uid] = game_id;
        }

        public void RemovePlayer(string uid)
        {
            var tmp = stateHandler.currentMemberships[uid];
            if (tmp == null) return;
            //  remove from dictionary
            
            
            stateHandler.uidConnectionId.TryGetValue(uid, out string connectionId);
            stateHandler.currentMemberships[uid] = null;
            RemoveFromGroup(connectionId, tmp);
        }
        
        
        public override Task OnConnectedAsync()
        {

            //  overall connections, need to get a way of binding games
            // UserHandler.ConnectedIds.Add(Context.ConnectionId);
            stateHandler.ConnectedIds.Add(Context.ConnectionId);
            var uid = Context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (uid == null)
            {
                return base.OnConnectedAsync();
            }
            stateHandler.uidConnectionId.Add(uid, Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        
        public override Task OnDisconnectedAsync(Exception exception)
        {

            stateHandler.ConnectedIds.Remove(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
    }
    */
}
