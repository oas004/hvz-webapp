import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditIndividualMissionsComponent } from './admin-edit-individual-missions.component';

describe('AdminEditIndividualMissionsComponent', () => {
  let component: AdminEditIndividualMissionsComponent;
  let fixture: ComponentFixture<AdminEditIndividualMissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditIndividualMissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditIndividualMissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
