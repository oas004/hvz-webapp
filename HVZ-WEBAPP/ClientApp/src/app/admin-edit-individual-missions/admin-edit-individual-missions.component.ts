import { Component, OnInit, Input } from '@angular/core';
import { AdminMissionPayload, ApiService, AdminUpdateMissionPayload } from '../api.service';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {take} from 'rxjs/operators';
import { InputField, GameField } from '../map-modal/map-modal.component';
import { Router } from '@angular/router';
import { ErrorService } from '../error.service';
@Component({
  selector: 'app-admin-edit-individual-missions',
  templateUrl: './admin-edit-individual-missions.component.html',
  styleUrls: ['./admin-edit-individual-missions.component.css']
})
export class AdminEditIndividualMissionsComponent implements OnInit {
  @Input() mission:  AdminMissionPayload;
  humanMission:Boolean;
  editMission:Boolean;
  //subscribe:Subscription= new Subscription();
  
  // The edited mission
  title:string;
  description:string;
  human_vissible:Boolean;
  start_time: string;
  end_time:string;
  @Input() game: GameField = new GameField();
  updatedMission:AdminUpdateMissionPayload;
  location: InputField[] = [new InputField("Mission Marker")];
  constructor(private apiService:ApiService, private route:Router, private errorService:ErrorService) { }

  ngOnInit(): void {

    this.checkIfHumanVissible();
    this.editMission = false;
    //  Hack the map center
    this.game.nwLat = this.mission.lat;
    this.game.nwLng = this.mission.lng;
    this.game.seLat = this.mission.lat;
    this.game.seLng = this.mission.lng;
    //Setting default values for the mission so the edit is not null
    this.updatedMission = new AdminUpdateMissionPayload();
   
    this.title = this.mission.name;
    this.description = this.mission.description;
    this.human_vissible = this.mission.isHumanVisible;
    this.start_time = this.mission.startTime;
    this.end_time = this.mission.endTime;
    this.updatedMission.id = this.mission.id;
    this.updatedMission.gameId = this.mission.gameId;
    
  }
  ngOnDestroy(){
    // this.subscribe.unsubscribe();
  }

  
  // Checks if the mission is a zombie mission or a human mission at the start
  checkIfHumanVissible(){
    if(this.mission.isHumanVisible == true){
      this.humanMission = true;
    }else{
      this.humanMission = false;
    }

  }

  // When the admin has decided to edit this mission
  edit(){
    this.editMission = true;
    this.location[0].latLng.lat = this.mission.lat;
    this.location[0].latLng.lng = this.mission.lng;
    this.mission["location"] = this.location;
  }

  // When the admin does not want to change afterall
  undoChanges(){
    this.editMission = false;
    
  }

  // when the admin changes the mission
  submitChanges() {
    if(this.title == null || this.human_vissible == null || this.start_time == null || this.end_time == null){
      alert("Something was not entered right...");
      return;
    }else{
      // Set the values of the updated object
      this.updatedMission.description = this.description;
      this.updatedMission.endTime = this.end_time;
      this.updatedMission.isHumanVissible = this.human_vissible;
      this.updatedMission.name = this.title;
      this.updatedMission.description = this.description;
      this.updatedMission.startTime = this.start_time;
      this.updatedMission.id = this.mission.id;
      this.updatedMission.gameId = this.mission.gameId;
      this.updatedMission.lat = this.location[0].latLng.lat;
      this.updatedMission.lng = this.location[0].latLng.lng;

      // Make put request with changes
      this.apiService.updateMission(this.updatedMission.gameId, this.updatedMission,this.updatedMission.id).pipe(take(1)).subscribe(resp =>{
        if(resp == null) {
          // put was success
          alert("Update was successfull!");
          this.editMission = false;
          // Should update the component
          this.mission.name = this.updatedMission.name;
          this.mission.startTime = this.updatedMission.startTime;
          this.mission.endTime = this.updatedMission.endTime;
          this.mission.description = this.updatedMission.description;
        }else {
          alert("Something went wrong.." + resp.toString());

        }
      });

    }
  }

  deleteMission(){
    let confirmation = confirm("Are you sure you want to delete this misssion?");
    if(confirmation == true){
  
      this.apiService.deleteMission(this.mission.gameId, this.mission.id).subscribe(resp=>{
    
        this.route.navigate([`/game/admin/${this.mission.gameId}`]);
     
       // error => this.errorService.updateError("Could not delete mission " + resp.toString());
      
      });
    }
  }


}
