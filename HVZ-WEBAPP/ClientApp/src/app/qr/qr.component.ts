import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlayerRef } from '../api.service';
import { CurrentgameService } from '../currentgame.service';
import { CurrentPlayerService } from '../current-player.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlayerModel } from '../game-details/game_models/player.model';
import { ErrorService } from '../error.service';
import { InputField } from '../map-modal/map-modal.component';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.css']
})
export class QRComponent implements OnInit {
  
  
  @Input() public scannerEnabled: boolean = true;
  @Input() player: PlayerModel = {name: "QRTest subject", id: 0, isHuman: true, userId: 0, biteCode : "", user: null, rank: 0};  //TODO: Change to currentPlayerService.
  bitecode: string = "";
  @Output() submittedBitecode = new EventEmitter();
  @Input() buttonText: string = "Open bitecode"
  story: string = "";
  waiting = false;
  modal = null;
  mapCoordinates: InputField[] =  [new InputField("Kill Marker Location")];
  constructor(private currentGameService: CurrentgameService, private currentPlayerService: CurrentPlayerService, private modalService: NgbModal, private errorService: ErrorService) { }

  ngOnInit(): void {
    if(this.player.isHuman) {
      this.bitecode = this.player.biteCode;
    }
    this.mapCoordinates[0].latLng.lat = null;
    this.mapCoordinates[0].latLng.lng = null;
  }
  //  opens modal
  openVerticallyCentered(content) {
    this.modal = this.modalService.open(content, { centered: true });
  }
  
  
  submit(hasStory) {
    this.waiting = true;
    let lat = null;
    let lng = null;
    if(hasStory)  {
      lat = this.mapCoordinates[0].latLng.lat;
      lng = this.mapCoordinates[0].latLng.lng;
    }
    this.submittedBitecode.emit({bitecode: this.bitecode, story: this.story, hasStory: hasStory, lat: lat, lng: lng
    ,callback: (succes) => {
      this.waiting = false;
      
      if(succes) {
        this.story = "";
        this.bitecode = "";
        alert("Player was killed.");
      } else {
        this.errorService.updateError("Bite code was not accepted by the server.");
      }
    }})
  }
  //  Handles QR-success TODO: error handling.
  scanSuccessHandler(bitecode) {
    //this.scannerEnabled = false;
    //  qrCodeComponent!
    
    
    this.bitecode = bitecode;
   
  }

}
