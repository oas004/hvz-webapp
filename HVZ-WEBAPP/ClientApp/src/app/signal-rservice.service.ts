import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import {BehaviorSubject, of} from 'rxjs';
import { start } from 'repl';

import {PlayerRef, Message} from './api.service';
@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  private hubConnection: signalR.HubConnection;

  private message: BehaviorSubject<ChatUniCast> = new BehaviorSubject<ChatUniCast>(null);
  public currentMessage = this.message.asObservable();
  previousMessage: ChatUniCast = null;

  public startConnection = (game_id: number, player: PlayerRef, selectedSquad = null) => {
    console.log("Started connection");
    let user = JSON.parse(sessionStorage['oidc.user:https://localhost:44395:HVZ_WEBAPP']);
    

    this.hubConnection = new signalR.HubConnectionBuilder()
    .withUrl('https://localhost:44395/chat', {accessTokenFactory:() => user.access_token })
    .build();
    
    this.hubConnection
      .start()
      .then(
        _ => {console.log("Connection started");
        //  subscribe to the following
        let GlobalSubscription = new Subscription();
        GlobalSubscription.type = "Global";
        GlobalSubscription.gameId = game_id;
        GlobalSubscription.isHuman = player.isHuman;
        // if(selectedSquad != null) GlobalSubscription.squadId = selectedSquad;
        let FactionSubscription = new Subscription();
        FactionSubscription.type = "Faction";
        FactionSubscription.gameId = game_id;
        FactionSubscription.isHuman = player.isHuman;

        if(selectedSquad != null) {
          let SquadSubscription = new Subscription();
          SquadSubscription.type = "Squad";
          SquadSubscription.gameId = game_id;
          SquadSubscription.isHuman = player.isHuman;
          SquadSubscription.squadId = selectedSquad;
          this.hubConnection.send("Subscribe", SquadSubscription)
          .then(respo => console.log(respo));
        }
        
        this.hubConnection.send("Subscribe", GlobalSubscription)
        .then(respo => console.log(respo));
        this.hubConnection.send("Subscribe", FactionSubscription)
        .then(respo => console.log(respo));
        this.addChatListener();
       })
      .catch(err => console.log("Error while connecting to up: " + err));
    
       return of(this.hubConnection);
  }
  
  prevMsg: ChatUniCast = null;
  public addChatListener = () => {
    this.hubConnection.on('chatUpdate', (data: ChatUniCast) => {
        if(this.previousMessage && this.previousMessage.message.id == data.message.id ) return;
        this.previousMessage = data;
        this.message.next(data);
      
    });
  }
    
  constructor() { 
  }
}

export class Subscription{

  type: string = "";
  gameId: number = 0;
  isHuman: boolean = false;
  squadId: number = 0;
}

export class ChatUniCast{
  type: string = "";
  message: Message = null;
}