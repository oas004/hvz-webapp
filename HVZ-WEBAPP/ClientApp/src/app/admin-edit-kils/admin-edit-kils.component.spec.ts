import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditKilsComponent } from './admin-edit-kils.component';

describe('AdminEditKilsComponent', () => {
  let component: AdminEditKilsComponent;
  let fixture: ComponentFixture<AdminEditKilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditKilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditKilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
