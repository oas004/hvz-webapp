import { Component, OnInit, Input } from '@angular/core';
import { AdminKillPayload, ApiService, User, AdminGetUserPayload, AdminEditKillPayload } from '../api.service';
import { Router } from '@angular/router';
import {take} from 'rxjs/operators';
@Component({
  selector: 'app-admin-edit-kils',
  templateUrl: './admin-edit-kils.component.html',
  styleUrls: ['./admin-edit-kils.component.css']
})
export class AdminEditKilsComponent implements OnInit {

  @Input() kill:AdminKillPayload;
  murderer:AdminGetUserPayload;

  // subscription1:Subscription= new Subscription();
  // subscription2:Subscription= new Subscription();
  // subscription3:Subscription= new Subscription();
  
  editing:boolean = false;

  //Edit------------
  editedKill:AdminEditKillPayload;
  editStory:string;
  editLat:number;
  editLng:number;
  //----------------

  constructor(private apiService:ApiService, private route:Router) { }

  ngOnInit(): void {
  
    this.getKilledByUsername();
  }


  getKilledByUsername(){
    this.apiService.getPlayerById(this.kill.gameId, this.kill.killerId).pipe(take(1)).subscribe(resp=>{
      this.apiService.getUserById(resp.userId).pipe(take(1)).subscribe(resp=>{
        this.murderer = resp;
        if(resp == null){alert("Could not get the user of the killer..")}
     
      });

    });
  }

  // Edit the kills
  editKill(){

    this.editing = true;
  }

  // 
  submit(){
 

    if(this.editLat == null || this.editLng == null || this.editStory == null){alert("Some fields are empty.."); return;}
    this.editedKill = new AdminEditKillPayload();
    this.editedKill.killerId = this.kill.killerId;
    this.editedKill.isDeleted = this.kill.isDeleted;
    this.editedKill.id = this.kill.id;
    
    this.editedKill.lat = this.editLat;
    this.editedKill.lng = this.editLng;
    this.editedKill.timeOfDeath = this.kill.timeOfDeath;
    this.editedKill.victimId = this.kill.victimId;
    this.editedKill.gameId = this.kill.gameId;
    this.editedKill.story = this.editStory;
    // Make the put request
    this.apiService.adminUpdateKill(this.editedKill.gameId,this.editedKill.id, this.editedKill).pipe(take(1)).subscribe(resp=>{

      error=>alert(error);
      if(resp == null) {
        alert("Update success!");
        this.route.navigate([`/game/admin/${this.editedKill.gameId}`]);
      }
    });
  }

  // Delete the kill
  deleteKill(){
    
    var confirmation = confirm(`Are you sure you want to delete the kill?`);
    if(confirmation == true){
      this.apiService.adminDeleteKill(this.kill.gameId, this.kill.id).pipe(take(1)).subscribe(resp => {
      
        this.route.navigate([`/game/admin/${this.editedKill.gameId}`]);
     
      });
    }
  }
}
