import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SquadList } from './models/SquadList';
import { SquadMembers } from './models/SquadMembers';
import { Squad, NewSquad } from './models/Squad';
import { BiteCodeModel } from './game-details/game_models/bitecode.model';
import { Observable } from 'rxjs'
import { SquadObjectModel } from "./game-details/game_models/squad.object.model";
import { BiteSubmissionModel } from './game-details/game_models/bitesubmission.model';
import { UserModel } from './game-details/game_models/user.model';
import { PlayerModel } from './game-details/game_models/player.model';  // DU KAN JO IKKE GJØRE DETTE HER! DET GJØR JO API-SERVICEN AVHENGIG AV EN KOMPONENT?!!!!!
import { GameModel } from './game-details/game_models/game.model';
import { missionPayload } from './admin-create-mission/admin-create-mission.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiPath: string = "https://localhost:44395/api/"
  constructor(private http: HttpClient) { }

  //  gets a chat from server.
  getChat(game_id: number): Observable<Message[]>{
     return this.http.get<Message[]>(this.apiPath + `game/${game_id}/chat`);
  }

  getSquadChat(game_id: number, squad_id): Observable<ChatUpdate> {
     return this.http.get<ChatUpdate>(this.apiPath + `game/${game_id}/squad/${squad_id}/chat`);
  }

  getChats(game_id: number): Observable<ChatUpdate> {
     return this.http.get<ChatUpdate>(this.apiPath + `game/${game_id}/chat`);
  }
  getGames(): Observable<GameListPayload[]> {
     return this.http.get<GameListPayload[]>(this.apiPath + `game/`);
    //  anonymous access
  }


  getGame(game_id: number): Observable<GameModel> {
     return this.http.get<GameModel>(this.apiPath + `game/${game_id}`);
  }

  //  Squad checkin not done.
   getSquadCheckin(game_id, squad_id): Observable<SquadCheckinMarker[]> {
      return this.http.get<SquadCheckinMarker[]>(this.apiPath + `game/${game_id}/squad/${squad_id}/check-in`);
   }

   postSquadCheckIn(game_id: number, squad_id: number, squadCheckIn: SquadCheckin): Observable<SquadCheckin> {
      return this.http.post<SquadCheckin>(this.apiPath + `game/${game_id}/squad/${squad_id}/check-in`, squadCheckIn);
   }

  getMultipleSquads(game_id: number): Observable<Squad[]> {
     return this.http.get<Squad[]>(this.apiPath + `game/${game_id}/squad`);
  }
  createSquad(game_id: number, squad: NewSquad): Observable<Squad> {
     return this.http.post<Squad>(this.apiPath + `game/${game_id}/squad`, squad);
  }
  joinSquad(game_id: number, squad_id: number, squadMembers: SquadMembers): Observable<SquadMembers> {
     return this.http.post<SquadMembers>(this.apiPath + `game/${game_id}/squad/${squad_id}/join`, squadMembers)
  }
  getSquadMembersInSquadView(game_id: number, squad_id: number): Observable<SquadMembers[]> {
     return this.http.get<SquadMembers[]>(this.apiPath + `game/${game_id}/squad/${squad_id}/squadmembers`);
  }
  getSquadsGameDetails(game_id: number): Observable<SquadObjectModel[]> {
     return this.http.get<SquadObjectModel[]>(this.apiPath + `game/${game_id}/squad`);
  }
  getBitecode(game_id: number, player_id: number): Observable<BiteCodeModel> {
     return this.http.get<BiteCodeModel>(this.apiPath + `game/${game_id}/player/${player_id}`)
  }
  submitBite(game_id: number, killObject: BiteSubmissionModel): Observable<BiteSubmissionModel> {
     return this.http.post<BiteSubmissionModel>(this.apiPath + `game/${game_id}/kill/bitecode`, killObject);
  }

  getMe(game_id: number): Observable<PlayerRef> {
     return this.http.get<PlayerRef>(this.apiPath + `game/${game_id}/me`)
    //  anonymous?
  }
  // getPlayer(game_id): Observable<PlayerRef>
  postSquadChat(game_id: number, squad_id: number, msg: Message): Observable<Object>{
     return this.http.post<Message>(this.apiPath + `game/${game_id}/squad/${squad_id}/chat`, msg)

  }
  postChat(game_id: number, msg: Message): Observable<Object> {

    if(!msg.isSquad) {
      
      return this.http.post<Message>(this.apiPath + `game/${game_id}/chat`, msg);
    } 
    return this.http.post<Message>(this.apiPath + `game/${game_id}/chat`, msg); 
     //  not implemented, tricky.
    //  The squad chat is a bit finnicky, as a player can have two squad memberships.
  }
  getPlayersInGame(game_id:number):Observable<PlayerPayload[]>{
     return this.http.get<PlayerPayload[]>(this.apiPath + `game/${game_id}/player`);
  }


  // Admin stuff dont touch <3
  checkAdmin():Observable<boolean> {
    return this.http.get<boolean>(this.apiPath + 'admin');
    
  }
  // Gets an extended game view
  getAdminGames(): Observable<AdminGamePayload[]> {
     return this.http.get<AdminGamePayload[]>(this.apiPath + `game/`);
  }

  // Get a specific game
  getAdminGame(game_id):Observable<AdminGamePayload> {
     return this.http.get<AdminGamePayload>(this.apiPath + `game/${game_id}`);
  }
  // Makes soft delete on a game if admin and id exists
  deleteGame(game_id:number):Observable<Object>{
     return this.http.delete(this.apiPath+`game/${game_id}`);
  }
  //Get the squads in a game
  getSquads(game_id:number):Observable<AdminSquadPayload[]>{
     return this.http.get<AdminSquadPayload[]>(this.apiPath + `game/${game_id}/squad`);
  }
  //Get the missions in a game
  getMissions(game_id:number):Observable<AdminMissionPayload[]> {
     return this.http.get<AdminMissionPayload[]>(this.apiPath + `game/${game_id}/mission`);
  }
  // Admin updating a mission, will only work if user is admin
  updateMission(game_id:number, mission:AdminUpdateMissionPayload, mission_id:number):Observable<Object>{
     return this.http.put<AdminUpdateMissionPayload>(this.apiPath + `game/${game_id}/mission/${mission_id}`,mission);
  }

  //Get players for join squad component
  getPlayersInSquad(game_id: number, player_id: number): Observable<PlayerModel> {
     return this.http.get<PlayerModel>(this.apiPath + `game/${game_id}/player/${player_id}`);
  }
  // Admin get the players of a game
  getPlayers(game_id:number):Observable<AdminGetPlayersPayload[]>{
     return this.http.get<AdminGetPlayersPayload[]>(this.apiPath +`game/${game_id}/player`);
  }

  // Admin get the user from the userId in order to get the username
   getUserById(user_id:number):Observable<AdminGetUserPayload>{
      return this.http.get<AdminGetUserPayload>(this.apiPath + `user/${user_id}`);
  }
  // Admin makes a new game
  makeNewGame(newGame:AdminNewGamePayload):Observable<GameListPayload>{
     return this.http.post<GameListPayload>(this.apiPath + `game`, newGame);
  }

  //Admin updates a game
  updateGame(updatedGame:AdminNewGamePayload, game_id:number):Observable<Object>{
     return this.http.put<Object>(this.apiPath + `game/${game_id}`, updatedGame);
  }

  //Admin get squadmembers
  getSquadMembers(game_id:number, squad_id:number):Observable<AdminSquadMemberPayload[]>{
     return this.http.get<AdminSquadMemberPayload[]>(this.apiPath +`game/${game_id}/squad/${squad_id}/squadmembers`);
  }
  // Admin get player by id
  getPlayerById(game_id:number, player_id:number):Observable<AdminGetPlayersPayload>{
     return this.http.get<AdminGetPlayersPayload>(this.apiPath + `game/${game_id}/player/${player_id}`);
  }

  // Admin deletes squad
  deleteSquad(game_id:number, squad_id:number):Observable<Object>{
     return this.http.delete(this.apiPath + `game/${game_id}/squad/${squad_id}`);
  }

  // Admin delete squadmember
  deleteSquadMember(game_id:number, squad_id:number, squadmember_id:number):Observable<Object>{
     return this.http.delete(this.apiPath + `game/${game_id}/squad/${squad_id}/squadmembers/${squadmember_id}`);
  }

  // Admin update rank
  updateRank(game_id:number, player_id:number, player:AdminGetPlayersPayload):Observable<Object>{
     return this.http.put(this.apiPath + `game/${game_id}/player/${player_id}`, player);
  }
  //Get UserId for squadjoin - SquadView
  getUserFromAsp(): Observable<AspUserModel> {
     return this.http.get<AspUserModel>(this.apiPath + `user`);
  }
  // Get playerId from user - SquadView
  getPlayerFromUser(user_id: number, game_id: number): Observable<PlayerPayload> {
     return this.http.get<PlayerPayload>(this.apiPath + `game/${game_id}/player/user/${user_id}`);
  }
  
  getUser(): Observable<UserModel> {
     return this.http.get<UserModel>(this.apiPath + `user`);
  }

  getPlayersInGameExtended(game_id: number): Observable<PlayerModel[]> {
     return this.http.get<PlayerModel[]>(this.apiPath + `game/${game_id}/player`);
  }
  postPlayer(game_id: number, player: PlayerPayload): Observable<PlayerPayload> {
     return this.http.post<PlayerPayload>(this.apiPath + `game/${game_id}/player`, player);
  }
  getPlayerFromUserExtended(user_id: number, game_id: number): Observable<PlayerModel> {
    return this.http.get<PlayerModel>(this.apiPath + `game/${game_id}/player/user/${user_id}`);
  }

  // Admin get kills
  adminGetKills(game_id:number):Observable<AdminKillPayload[]>{
     return this.http.get<AdminKillPayload[]>(this.apiPath + `game/${game_id}/kill`);
  }

  // Admin update kill
  adminUpdateKill(game_id:number,kill_id:number, kill:AdminEditKillPayload):Observable<Object>{
     return this.http.put<Object>(this.apiPath + `game/${game_id}/kill/${kill_id}`, kill);
  }

  adminDeleteKill(game_id:number, kill_id:number):Observable<Object>{
     return this.http.delete<Object>(this.apiPath + `game/${game_id}/kill/${kill_id}`);
  }

  createMission(game_id:number, mission:missionPayload):Observable<AdminMissionPayload>{
     return this.http.post<AdminMissionPayload>(this.apiPath + `game/${game_id}/mission`, mission);
  }

  //admin delete mission
  deleteMission(game_id:number, mission_id:number):Observable<Object>{
    return this.http.delete<object>(this.apiPath + `game/${game_id}/mission/${mission_id}`);
  }

}
export class AspUserModel {
  id: number = 0;
  aspNetUserId: string ="";
  username: string = "";
  firstname: string ="";
  lastname: string = "";
  isDeleted: boolean = false;
}
export class PlayerModelSquad {
  id:number = 0;
  isHuman:Boolean = false;
  isPatientZero:Boolean = false;
  userId:number = 0;
  gameId:number = 0;
  //user:User;
  biteCode:string = "";
}
export class GameListPayload {
  id: number = 0;
  name: string = "";
  gameState: number = 0;
  startTime: Date = new Date();
  endTime: Date = new Date();
  players: number = 0;
}

export class Message {
  message: string = "";
  isHumanGlobal: boolean = false;
  isZombieGlobal:boolean = false;
  isSquad: boolean = false;
  squadId: number = 0;
  chatTime: Date = new Date();
  id: number = 0;
  playerRef: PlayerRef = new PlayerRef();
}

export class PlayerRef {
  username: string = "";
  playerId: number = 0;
  isHuman: boolean;
  squadMember: SquadMemberRef[] = [];
}

export class SquadMemberRef {
  id: number;
  squad: SquadRef;
}


export class SquadRef {
  id: number;
  name: string;
  isHuman: boolean;
}
//  Used when getting details for a game.
export class GameOverview {

}
export class ChatUpdate
  {
    globalChat: Message[] = [];
    factionChat: Message[] = [];
    squadChat: Message[] = [];
  }
export class PlayerPayload{
  id: number = 0;
  isHuman: Boolean = false;
  isPatientZero: Boolean = false;
  biteCode: String = "";
  userId:number = 0;
  gameId:number = 0;
}

export class AdminGamePayload{
  id: number = 0;
  name: string = "";
  gameState: number = 0;
  players: number = 0;
  isDeleted:Boolean = false;
  mission:Array<Object> = []
  player:Array<Object> = []
  nwLat: number;
  nwLng: number;
  seLat: number;
  seLng: number;
    
}
export class AdminSquadPayload{
  id:number = 0;
  name:string = "";
  is_human:Boolean = false;
  gameId:number = 0;
  isDeleted:Boolean = false;
}
export class AdminMissionPayload{
  id:number = 0;
  name:string = "";
  isHumanVisible:Boolean = false;
  isZombieVisible:Boolean = false;
  description:string = "";
  startTime:string = "";
  endTime:string = "";
  gameId:number = 0;
  isDeleted:Boolean = false;
  lat:number = 0;
  lng:number = 0;
}

export class AdminUpdateMissionPayload{
  id:number = 0;
  name:string = "";
  isHumanVissible:Boolean = false;
  description:string = "";
  startTime:string = "";
  endTime:string = "";
  gameId:number = 0;
  lat:number = 0;
  lng:number = 0;
}

export class AdminGetPlayersPayload{
  id:number = 0;
  isHuman:Boolean = false;
  isPatientZero:Boolean = false;
  userId:number = 0;
  gameId:number = 0;
  user:User;
  biteCode:string = "";
  rank:number = 0;
}
export class AdminGetUserPayload {
  id:number = 0;
  username:string = "";
  firstName:string = "";
  lastName:string = "";
  isDeleted:Boolean = false;
}

export class AdminNewGamePayload{
  id:number = 0;
  name:string ="";
  gameState:number = 0;
  isDeleted:Boolean = false;
  nwLat:number = 0;
  nwLng:number = 0;
  seLat:number = 0;
  seLng:number = 0;
}

export class AdminSquadMemberPayload{
  id:number = 0;
  rank:number = 0;
  gameId:number = 0;
  playerId:number = 0;
  player:Player
  isDeleted:boolean = false;
}

export class User{
  id:number = 0;
  username:string = "";
  firstName:string = "";
  lastName:string = "";
  isDeleted:boolean = false;
}

export class Player{
  id:number = 0;
  isHuman:boolean = false;
  rank:number = 0;
  gameId:number = 0;
  
}

export class AdminKillPayload{
  id:number = 0;
  timeOfDeath:string = "";
  story:string ="";
  lat:number = 0;
  lng:number = 0;
  gameId:number = 0;
  killerId:number = 0;
  victimId:number = 0;
  isDeleted:boolean = false;
}
export class AdminEditKillPayload{
  id:number;
  timeOfDeath:string = "";
  story:string ="";
  lat:number = 0;
  lng:number = 0;
  gameId:number = 0;
  killerId:number = 0;
  victimId:number = 0;
  isDeleted:boolean = false;
}
export class SquadCheckinMarker {
   username: string = "";
   checkIn: SquadCheckin = new SquadCheckin();
}
export class SquadCheckin {
   id: number = 0;
   startTime: string = "" ;
   endTime: string = "";
   lat: number = 0;
   lng: number = 0;
   gameId: number = 0;
   squadId: number = 0;
   squadMemberId: number = 0; // can be retrieved by server.
   // That's all I need for now?
}
