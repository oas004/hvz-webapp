import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenCheckService {
  //  use a placeholder variable here
  defaultKey: string = "oidc.user";
  constructor() { }

  isLoggedIn() {
    let sessionToken = this.getStorage();
    if(sessionToken) {
      sessionToken = JSON.parse(sessionToken);
      let expireTime = sessionToken.expires_at;
      let currentTime = Date.now() / 1000;
      if(currentTime > expireTime) {
        return false;
      }
      return true;
    } else {
      return false;
     
    }
  }

  getStorage() {
    let ret = null;
    //  iterateover session storage to get token
    Object.keys(sessionStorage).forEach((e) => {
      if(e.includes(this.defaultKey)) {
        
        ret = sessionStorage.getItem(e);
      }
      
    });
    return ret;
    
  }
  
}
