import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCreateAGameComponent } from './admin-create-a-game.component';

describe('AdminCreateAGameComponent', () => {
  let component: AdminCreateAGameComponent;
  let fixture: ComponentFixture<AdminCreateAGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCreateAGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCreateAGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
