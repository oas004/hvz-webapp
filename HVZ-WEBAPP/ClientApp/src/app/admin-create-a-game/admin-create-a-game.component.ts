import { Component, OnInit,Input } from '@angular/core';
import { ApiService, AdminNewGamePayload } from '../api.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ErrorService } from '../error.service';
import { InputField } from '../map-modal/map-modal.component';
import { take } from 'rxjs/operators';


@Component({
  selector: 'app-admin-create-a-game',
  templateUrl: './admin-create-a-game.component.html',
  styleUrls: ['./admin-create-a-game.component.css']
})
export class AdminCreateAGameComponent implements OnInit {
  // Create game object
  newGame: AdminNewGamePayload;
  newGameForm:FormGroup;
  gameState:number;
  isSubmited = false;
  //subscription:Subscription = new Subscription();
  // Game states
  states:any[] = [{id:0, StateName:"Registration"}, {id:1, StateName:"In Progress"}, {id:2, StateName:"Completed"}];
  curState:any = this.states[0];
  id:number;

  //  Map modal component
  mapCoordinates: InputField[] = [new InputField("North-West Corner"), new InputField("South-East Corner")];

  formatCoordinate(input: InputField, coordsOnly:boolean = true) {
    if(coordsOnly) return `${input.latLng.lat.toFixed(6)}, ${input.latLng.lng.toFixed(6)}`
    return `${input.name}[ lat: ${input.latLng.lat} lng: ${input.latLng.lng}]`
  }

  gotCoordinates(e) {

  }

  constructor(private apiService:ApiService, private formBuilder:FormBuilder, private errorService:ErrorService) { }

  ngOnInit(): void {
    this.newGame = new AdminNewGamePayload();
    this.newGameForm = this.formBuilder.group({
      gameName:['',Validators.required]
      
    });
  }
  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }
  get formControls() {
    return this.newGameForm.controls;
  }

  // post request to create a new game
  createGame(){
    this.gameState = this.curState.id;
    this.isSubmited = true;
    
    if(this.newGameForm.invalid){
      return;
    }
    this.newGame.name = this.newGameForm.value.gameName;
    this.newGame.isDeleted = false;
    this.newGame.gameState = this.id;
    //  add coordinates.
    this.newGame.nwLat = this.mapCoordinates[0].latLng.lat;
    this.newGame.nwLng = this.mapCoordinates[0].latLng.lng;
    this.newGame.seLat = this.mapCoordinates[1].latLng.lat;
    this.newGame.seLng = this.mapCoordinates[1].latLng.lng;
   
    //Do the post request
    var confirmation = confirm(`Are you sure you want to make a game with title: ${this.newGame.name} ?`);
    if(confirmation == true){
      this.apiService.makeNewGame(this.newGame).pipe(take(1)).subscribe(resp => {
       
        error=>this.errorService.updateError("Something went horrobly wrong.." + resp);

      });
    }else{
      return;
    }
  }
  
  //Set the selected gamestate
  setNewState(id:any){

    this.id = parseInt(id);
    this.curState = this.states.filter(value => value.id === parseInt(id));

  }
 
}
