import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AdminNewGamePayload, ApiService, AdminGamePayload, AdminSquadPayload, AdminMissionPayload, AdminGetPlayersPayload, AdminKillPayload } from '../api.service';
import {take} from 'rxjs/operators';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ErrorService } from '../error.service';
import { InputField , GameField} from '../map-modal/map-modal.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-admin-edit-individual-games',
  templateUrl: './admin-edit-individual-games.component.html',
  styleUrls: ['./admin-edit-individual-games.component.css']
})
export class AdminEditIndividualGamesComponent implements OnInit {
  // What navbar is selected
  selectedNav:Number; // 0 = game, 1 = squad, 2 = mission
  
  // Before user has edited
  gameEdited:Boolean;
  gameId:Number;
  game:AdminGamePayload;
  squads:Array<AdminSquadPayload> = [];
  missions:Array<AdminMissionPayload> = [];
  players:Array<AdminGetPlayersPayload> = [];
  gameState:number;
  // After user edit
   // Create game object
   newGame: AdminNewGamePayload;
   newGameForm:FormGroup;
   isSubmited = false;
  name:string;
  
  // subscription:Subscription= new Subscription();
  // subscription1:Subscription= new Subscription();
  // subscription2:Subscription= new Subscription();
  // subscription3:Subscription= new Subscription();
  // subscription4:Subscription= new Subscription();

  // Game states
  states:any[] = [{id:0, StateName:"Registration"}, {id:1, StateName:"In Progress"}, {id:2, StateName:"Completed"}];
  curState:any = this.states[0];
  id:number;

  //Killobject
  kills:Array<AdminKillPayload> = [];

  //Values after the admin has said what he wants
  constructor(private route:ActivatedRoute, private apiService: ApiService, private formBuilder:FormBuilder, private errorService:ErrorService) { }

  ngOnInit(): void {
  this.newGame = new AdminNewGamePayload(); 
  this.gameId = this.route.snapshot.params.id;
  this.getGame();
  this.gameEdited = false;
  this.selectedNav = 0;
  this.newGameForm = this.formBuilder.group({
    gameName:['',Validators.required],
    undoDelete:[false, Validators.required] 
  });
  }
  ngOnDestroy() {
    // this.subscription.unsubscribe();
    // this.subscription1.unsubscribe();
    // this.subscription2.unsubscribe();
    // this.subscription3.unsubscribe();
    // this.subscription4.unsubscribe();
    
  }
 
  
  // map coordinates
  mapCoordinates: InputField[] = [new InputField("North-West Corner"), new InputField("South-East Corner")];
  formatCoordinate(input: InputField, coordsOnly:boolean = true) {
    if(coordsOnly) return `${input.latLng.lat.toFixed(6)}, ${input.latLng.lng.toFixed(6)}`
    return `${input.name}[ lat: ${input.latLng.lat} lng: ${input.latLng.lng}]`
  }
  generateGameField() {
    return new GameField(this.game);
  }

  gotCoordinates(e) {
    
  }

  // Get the game values
  getGame(){
   this.apiService.getAdminGame(this.gameId).pipe(take(1)).subscribe(
        resp => {
          this.game = resp;
          this.mapCoordinates[0].latLng
          this.mapCoordinates[0].latLng.lat = this.game.nwLat;
          this.mapCoordinates[0].latLng.lng = this.game.nwLng;
          this.mapCoordinates[1].latLng.lat = this.game.seLat;
          this.mapCoordinates[1].latLng.lng = this.game.seLng;
          
        } ,
        error => this.errorService.updateError("Could not get game from database")
      
    );
  }
  get formControls() {
    return this.newGameForm.controls;
  }
  // The game navbar is selected
  public gameSelected() {
    this.selectedNav = 0;
  
    this.mapCoordinates = [new InputField("North-West Corner"), new InputField("South-East Corner")];
  }

  // The Squad navbar is selected
  public squadSelected() {
    this.getSquads();
    this.selectedNav = 1;
  
    
  }
  
  // The Mission navbar is selected
  public missionSelected() {
    this.getMissions();
    this.selectedNav = 2;


  }

  //When the edit player states are selected
  public playerSelected() {
    this.selectedNav = 3;
    this.getPlayers();
  }
  
  //Get the squads
  public getSquads() {
    this.apiService.getSquads(this.game.id).pipe(take(1)).subscribe(resp => {
      this.squads = resp;
    });
  }
  
  // Get the missions
  public getMissions(){
    this.apiService.getMissions(this.game.id).pipe(take(1)).subscribe(resp => {
        this.missions = resp;
    });
  }
  
  // Get the players
  public getPlayers(){
    this.apiService.getPlayers(this.game.id).pipe(take(1)).subscribe(resp => {
      this.players = resp;
    });
  }

    //Set the selected gamestate
    setNewState(id:any){
      this.id = parseInt(id);
      this.curState = this.states.filter(value => value.id === parseInt(id));
    }

    // When the admin has changes the values in a game and commits changes
    SubmitGameChanges(){
    this.gameState = this.curState.id;
    this.isSubmited = true;
    if(this.newGameForm.invalid){
      return;
    }
    // Make the put request
    this.newGame.name = this.newGameForm.value.gameName;
    this.newGame.gameState = this.id;
    this.newGame.id = this.game.id;
    if(this.newGameForm.value.undoDelete){ // If the user dont want the game to be deleted 
      this.newGame.isDeleted = false;
    }
    //  Coordinates of edited game!
    this.newGame.nwLat = this.mapCoordinates[0].latLng.lat;
    this.newGame.nwLng = this.mapCoordinates[0].latLng.lng;
    this.newGame.seLat = this.mapCoordinates[1].latLng.lat;
    this.newGame.seLng = this.mapCoordinates[1].latLng.lng;

    this.apiService.updateGame(this.newGame, this.game.id).pipe(take(1)).subscribe(resp => {
      if(resp == null){
        alert("Update was succesfull");
        window.location.reload();
      }else{
        alert("Something went wrong " + resp.toString());
      }
    });

    }

    // Auto fill input
    fill(){
 
      this.name = this.game.name;

    }

    //TODO
    // Edit mission marker
    missionMarker(){
      this.selectedNav = 4
    }

    //Edit kills
    killsSelected(){
      this.selectedNav = 5;
      this.getKills();
    }
    
    // Get all the kills in a game
    getKills(){
      this.apiService.adminGetKills(this.game.id).pipe(take(1)).pipe(take(1)).subscribe(resp => {
        this.kills = resp;
        error=>this.errorService.updateError("Could not get the kills");
      });
    }


}
