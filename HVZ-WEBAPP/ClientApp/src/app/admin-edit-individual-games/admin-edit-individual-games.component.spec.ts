import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditIndividualGamesComponent } from './admin-edit-individual-games.component';

describe('AdminEditIndividualGamesComponent', () => {
  let component: AdminEditIndividualGamesComponent;
  let fixture: ComponentFixture<AdminEditIndividualGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditIndividualGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditIndividualGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
