import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleTestComponent } from './role-test.component';

describe('RoleTestComponent', () => {
  let component: RoleTestComponent;
  let fixture: ComponentFixture<RoleTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
