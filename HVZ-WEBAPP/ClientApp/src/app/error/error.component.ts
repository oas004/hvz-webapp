import { Component, OnInit } from '@angular/core';
import { ErrorService } from '../error.service';


@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  
  constructor(private errorService: ErrorService) { }
  
  private timeout = null;
  private timeToClose = 4000;
  errorMessage: string = "";
  ngOnInit(): void {
    this.errorService.getError().subscribe({
      next: n => {
        if(this.timeout != null) {
          clearTimeout(this.timeout);
          this.timeout = null;
        } 

        this.timeout = setTimeout(() => {
          this.errorMessage = "";
        }, this.timeToClose);

        this.errorMessage = n;
      }
    });
    
    
  }
  closedAlert(): void {
    this.errorMessage = "";
  }
  
}
