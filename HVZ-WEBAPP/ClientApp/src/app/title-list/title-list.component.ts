import { Component, OnInit, Input } from '@angular/core';
import {take} from 'rxjs/operators'
import {Game} from '../../game'
import { ApiService, GameListPayload} from '../api.service';
import { ErrorService } from '../error.service';
import { TokenCheckService } from '../token-check.service';
import {CurrentgameService} from '../currentgame.service';
@Component({
  selector: 'app-title-list',
  templateUrl: './title-list.component.html',
  styleUrls: ['./title-list.component.css']
})
export class TitleListComponent implements OnInit {

  constructor(private apiService: ApiService, private errorService: ErrorService, private tokenService : TokenCheckService, private gameService: CurrentgameService) { }
  @Input() rules: string;
  games: Array<GameListPayload> = [];
  loggedIn = false;
  ngOnInit(): void {
    this.getGames();
    this.loggedIn = this.tokenService.isLoggedIn();
   
  }

  getGames() {

    this.apiService.getGames().pipe<GameListPayload[]>(take(1)).subscribe( 
      data => this.games = data, 
      error=> this.errorService.updateError("Could not get game list"),
    );
    //  error handling khatoi
  }

  setCurrentGame(id: number) {
    this.gameService.setCurrentgame(id)
  }

}
