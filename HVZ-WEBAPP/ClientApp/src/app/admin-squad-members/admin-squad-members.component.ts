import { Component, OnInit, Input } from '@angular/core';
import { AdminSquadMemberPayload, ApiService, AdminGetPlayersPayload, AdminGetUserPayload, User } from '../api.service';
import { switchMap, catchError } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { ErrorService } from '../error.service';
import {take} from 'rxjs/operators';
@Component({
  selector: 'app-admin-squad-members',
  templateUrl: './admin-squad-members.component.html',
  styleUrls: ['./admin-squad-members.component.css']
})
export class AdminSquadMembersComponent implements OnInit {
 @Input () squadmember:AdminSquadMemberPayload;
  player:AdminGetPlayersPayload;
  constructor(private apiService:ApiService, private errorService:ErrorService) { }
  subscription:Subscription = new Subscription();

  ngOnInit(): void {
   
    this.getPlayer();
 }
 ngOnDestroy(){
   this.subscription.unsubscribe();
 }

 getPlayer() {
   this.apiService.getPlayerById(this.squadmember.gameId,this.squadmember.playerId).pipe<AdminGetPlayersPayload>(take(1)).subscribe(resp=> {
    this.player = resp;
    error=> this.errorService.updateError("Something is wrong");

   });

   
 }

 

}
