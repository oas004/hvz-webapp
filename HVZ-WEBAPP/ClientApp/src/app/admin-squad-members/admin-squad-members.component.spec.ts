import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSquadMembersComponent } from './admin-squad-members.component';

describe('AdminSquadMembersComponent', () => {
  let component: AdminSquadMembersComponent;
  let fixture: ComponentFixture<AdminSquadMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSquadMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSquadMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
