import { EventEmitter, Component, OnInit, Input, Output ,ViewChild, ElementRef} from '@angular/core';
import {GameTitle} from '../../game'
import {AdminGamePayload} from '../api.service';
import { MapDefaults } from '../map/map.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { faThumbsDown } from '@fortawesome/free-solid-svg-icons';

export class InputField{
  constructor(name: string = "", latLng: LatLngTouple = new LatLngTouple()) {
    this.name = name;
    this.latLng = latLng;
  }
  name: string;
  latLng: LatLngTouple;
}

export class GameField{
  constructor(game: AdminGamePayload = null) {
    if(game != null) {
      this.convertGame(game);
    }
    
  }
  
  nwLat: number = 0;
  nwLng: number = 0;
  seLng: number = 0;
  seLat: number = 0;

  convertGame(game: AdminGamePayload) {
    Object.keys(this).forEach(key => {
      this[key] = game[key];
    })

  }
}

export class LatLngTouple {
  constructor(lng: number = 0, lat: number = 0) {
    this.lng = lng;
    this.lat = lat;
  }
  lng: number
  lat: number
}

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.css']
})
export class MapModalComponent implements OnInit {
  @Input() game: GameField = new GameField();
  //  Having the name makes the entire thing easier.
  @Input() buttonText: string = "Select from map";
  @Input() inputFields: InputField[] = [{name: "Marker Position", latLng: {lng: 0, lat: 0}}]  //  use ngModel
  @Input() autoCenter = true;
  @Output() submittedFields: EventEmitter<InputField[]> = new EventEmitter<InputField[]>();
  @ViewChild('content') modal; 
  @Input() zoom: number = 5;
  @Input() forceDrawRect = false;
  @Input() overlaySrc: string = "";

  
  intputFieldsTmp = this.inputFields;
  selectedObject = null;
  //@Output() outputFields = new EventEmitter();
  default: MapDefaults = null;
  selectedField: InputField = null;
  modalRef = null;
  
  constructor(private modalService: NgbModal) { }
  
  ngOnInit(): void {
    this.default = this.genereateMapDefaults();
  
    this.selectedField = this.inputFields[0];
    this.selectedObject = this.selectedField;
  }
  //  handle mapClick
  handleClick(latLng) {
    if(this.selectedObject == null)  return;

    this.selectedObject.latLng = latLng;
    //this.selectedObject.latLng.lat = latLng.lng;
  }

  generateInputFields(touple) {
    if(touple === undefined) return null;
    return Object.keys(touple);
  }

  genereateMapDefaults() {
    
    let def : MapDefaults = {
      nwLat: this.game.nwLat,
      nwLng: this.game.nwLng,
      seLat: this.game.seLat,
      seLng: this.game.seLng,
      zoom: 10,
      overlaySrc: this.overlaySrc
    }

    return def;
  }
  //  Handle selecting an object
  selectMe(obj) {
   
    this.selectedObject = obj
  }

  //  submit handler, submits event
  submitCoordinates(out) {
    //  should close the component
 
    this.submittedFields.emit(this.inputFields)
    this.modalRef.close();
  }

  openModal(content) {  
    this.modalRef = this.modalService.open(content, { centered: true });
  }

  getRefByValue(value) {
    let ret = null;
    this.inputFields.forEach( e=> {
      if(e == value) {
        ret = e;
        return;
      }
    })
    return ret;
  } 
  
  close(e) {
    this.modalRef.close();
  }

}
