import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  public emitted = "asd";

  handleQr(event) {
    this.emitted = event;
  }
  
  constructor(public router: Router) {}
}
