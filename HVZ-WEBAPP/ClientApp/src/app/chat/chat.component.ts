import { Component, OnInit } from '@angular/core';
import {take} from 'rxjs/operators';
import { ApiService, ChatUpdate,Message, PlayerRef, SquadRef } from '../api.service';
import {CurrentgameService} from '../currentgame.service'
import { CurrentPlayerService } from '../current-player.service';
import { SignalRService, ChatUniCast } from '../signal-rservice.service';
import { updateLocale } from 'moment';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  selectedChat: string = "globalChat";
  selected: Message[] = [];
  chats: ChatUpdate = new ChatUpdate();
  currentPlayer: PlayerRef = null;
  currentSquad: SquadRef = null;
  currentGame: number = -1;
  outMessage: string = "";

  constructor(private apiService: ApiService, private gameService: CurrentgameService
    , private currentPlayerService: CurrentPlayerService, private signalRService: SignalRService) {
      this.signalRService.currentMessage.subscribe(next => {
        if(next == null) {
          return;
        }
       
        //  Do stuff with the data here.
        this.handleUniCast(next ); 
        
      });
     }
  
  
  ngOnInit() {
    
    this.gameService.currentgame.subscribe(next =>   {
      this.changedGame(next);
      
    });

    //  subscribed to player check, reloads chats when a game and subsequently player is changed.
    this.currentPlayerService.currentPlayer.subscribe(next => {
      this.currentPlayer = next;
   
      if(next != null) {
        this.changedPlayer(next);
      } else {
        this.currentPlayer = null;

      }
    })
  }
  //  gets image of playerRank
  getPlayerRankImg(isHuman: boolean, rank: number) {
    
    if(rank >= 0 && rank < 14) {
      console.log("assets/icons/R" + rank + ".png");
      return "assets/icons/R" + rank + ".png";
    } else {
      return "";
    }
  }
  private handleUniCast(update: ChatUniCast) {
  
    if(update == null) return;
    update.message.playerRef['rankSrc'] = this.getPlayerRankImg(update.message.playerRef.isHuman, update.message.playerRef['rank']);
    this.chats[update.type].push(update.message);
  }

  private addRankImg(chatUpdate: ChatUpdate, channel: Message[] = null) {
    if(channel) {
      let chat: Message[] = channel;
        chat.forEach(msg => {
          msg.playerRef['rankSrc'] = this.getPlayerRankImg(msg.playerRef.isHuman,msg.playerRef['rank']);
        });
    } else {
      Object.keys(this.chats).forEach( key => {
        let chat: Message[] = this.chats[key];
        chat.forEach(msg => {
          msg.playerRef['rankSrc'] = this.getPlayerRankImg(msg.playerRef.isHuman,msg.playerRef['rank']);
        });
      });
    }
    
  }
  //  player changed, need to change squad.
  //  would be better to have a player with all this as a class.
  changedPlayer(currentPlayer): void {
    let tmp = this.currentPlayer.squadMember.find(m => m.squad.isHuman == this.currentPlayer.isHuman);
    if(tmp === undefined) {
      this.signalRService.startConnection(this.currentGame, this.currentPlayer, null);
      return;
    }
    this.currentSquad = tmp.squad;
   
    if(this.currentSquad != null) {
      this.apiService.getSquadChat(this.currentGame, this.currentSquad.id).pipe(take(1)).subscribe(
        data => {
          //  signalR service
          this.signalRService.startConnection(this.currentGame, this.currentPlayer, this.currentSquad.id);
          this.chats.squadChat = data.squadChat;
          this.addRankImg(this.chats, this.chats.squadChat);
          if(this.selectedChat == "squadChat") this.selected = this.chats.squadChat;
        },
        
        error => {alert("Cannot get squad messages: " ); alert(error); this.signalRService.startConnection(this.currentGame, this.currentPlayer, null);},
      )
    }
  }

  //  Game was changed from navbar
  changedGame(gameId: number): void {
   
    this.currentGame = gameId;
    this.chats = new ChatUpdate();
    
    this.apiService.getChats(gameId).pipe(take(1)).subscribe(
      
      data => {
        let chats = this.chats;
      
        this.chats = data;
        this.chats.squadChat = chats.squadChat;
        this.addRankImg(this.chats);
        console.log(this.chats);
        this.selected = this.chats[this.selectedChat]},
        
      error => console.log("horrible error etc")
    )
  }

  onChange(): void {
    //  selected chat is changed.
    
    this.selected = this.chats[this.selectedChat];
    
  }

  sendCallback(selected: Message[], msg: Message) {
    //selected.push(msg);
  }
  sendMessage(): void {
    
    if(this.outMessage == "") return;
    let tmpMsg = this.outMessage;
    this.outMessage = "";

    // let tmpSelected = this.selectedChat;
    let selectedRef = this.selectedChat;
    //  generate messaged
    let message: Message =  {isSquad: this.selectedChat == "squadChat" && this.currentSquad != null, squadId:  0,
    message: tmpMsg, isHumanGlobal: this.selectedChat == "globalChat",
      isZombieGlobal: this.selectedChat == "globalChat", chatTime: new Date(),
      id: 0, playerRef: this.currentPlayer }
  
    let s = this.selected;
  
    if(message.isSquad && this.currentSquad != null) {
      let asd = this.apiService.postSquadChat(this.currentGame, this.currentSquad.id, message ).pipe(take(1)).subscribe(data => {
      
        this.sendCallback(s, message);
        asd.unsubscribe();
      }, error => alert(error));
    } else {
      let ret = this.apiService.postChat(this.currentGame, message).pipe(take(1)).subscribe(data => {
    
        this.sendCallback(s, message);
      },
      error => alert(error)
      );
    }

  }
}
