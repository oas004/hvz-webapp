import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  updateObservable;
  observable: Observable<string>
  // constructor() {
    
  //   this.observable = Observable.create((observer) => {
  //     observer.next("asd");

  //     this.updateObservable = function (newValue) {
  //       observer.next(newValue);
  //       observer.complete();
  //     };
  //   });
  // }

  constructor() {
    this.observable = new Observable(observer => 
      { 
        this.updateObservable = (newValue) => { 
          observer.next(newValue);  
        }; 
      });
  } 
  updateError(error: string): void {
    this.updateObservable(error);
  }

  getError() {
    return this.observable;
  }

}
