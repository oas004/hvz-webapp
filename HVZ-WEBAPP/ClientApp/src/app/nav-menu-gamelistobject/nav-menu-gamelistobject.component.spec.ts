import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavMenuGamelistobjectComponent } from './nav-menu-gamelistobject.component';

describe('NavMenuGamelistobjectComponent', () => {
  let component: NavMenuGamelistobjectComponent;
  let fixture: ComponentFixture<NavMenuGamelistobjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavMenuGamelistobjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavMenuGamelistobjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
