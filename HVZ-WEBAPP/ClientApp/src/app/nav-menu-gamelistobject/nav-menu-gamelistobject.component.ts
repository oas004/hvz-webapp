import {Input, Component, OnInit } from '@angular/core';
import {GameListPayload} from '../api.service'
import { CurrentgameService } from '../currentgame.service';
import { CurrentPlayerService } from '../current-player.service';

@Component({
  selector: 'app-nav-menu-gamelistobject',
  templateUrl: './nav-menu-gamelistobject.component.html',
  styleUrls: ['./nav-menu-gamelistobject.component.css']
})
export class NavMenuGamelistobjectComponent implements OnInit {
  
  // Add service to constructor
  constructor(public currentgameService: CurrentgameService, private currentPlayerService: CurrentPlayerService) { }
  @Input() game: GameListPayload;

  ngOnInit(): void {
    
  }

  // Make new fuction to call the set in the service to change it.
  updateCurrentgame(){
    this.currentgameService.setCurrentgame(this.game.id);
    
  }

}
