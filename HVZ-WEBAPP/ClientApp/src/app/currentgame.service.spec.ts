import { TestBed } from '@angular/core/testing';

import { CurrentgameService } from './currentgame.service';

describe('CurrentgameService', () => {
  let service: CurrentgameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrentgameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
