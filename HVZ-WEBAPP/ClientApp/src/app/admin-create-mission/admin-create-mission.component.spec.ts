import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCreateMissionComponent } from './admin-create-mission.component';

describe('AdminCreateMissionComponent', () => {
  let component: AdminCreateMissionComponent;
  let fixture: ComponentFixture<AdminCreateMissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCreateMissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCreateMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
