import { Component, OnInit, Input } from '@angular/core';
import { AdminGamePayload, ApiService, AdminMissionPayload } from '../api.service';
import { InputField } from '../map-modal/map-modal.component';

@Component({
  selector: 'app-admin-create-mission',
  templateUrl: './admin-create-mission.component.html',
  styleUrls: ['./admin-create-mission.component.css']
})
export class AdminCreateMissionComponent implements OnInit {
  @Input() game:AdminGamePayload;
  
  // Input
  title:string;
  description:string;
  human_vissible:boolean;
  start_time: string ="2020-01-01T08:30" ;
  end_time:string = "2020-01-01T08:30";
  lat:number = 0; // from modal..
  lng:number= 0; // from modal..
  location: InputField[] = [new InputField("Mission Marker")];
  
  // Mission that is return from the post
  createdMission:AdminMissionPayload;
  sending: boolean = false;
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    // this.location.latLng.lat = (this.game.nwLat + this.game.seLat) / 2;
    // this.location.latLng.lng = (this.game.nwLng + this.game.seLng) / 2;
    this.location[0].latLng = {lat: (this.game.nwLat + this.game.seLat) / 2, lng:(this.game.nwLng + this.game.seLng) / 2 };

  }


  submit(){
    if(this.sending) return;
    if(this.title ==null || this.description == null || this.human_vissible == null || this.start_time == null || this.end_time == null || this.game.id == null || this.lat == null || this.lng == null){return;}
    let newMission = new missionPayload();
    newMission.name = this.title;
    newMission.description = this.description;
    newMission.isHumanVissible = this.human_vissible;
    
    if(this.human_vissible)
    {
      newMission.isZombieVisible = false;
    }
    else
    {
      newMission.isZombieVisible = true
    }
    newMission.startTime = this.start_time;
    newMission.endTime = this.end_time;
    newMission.gameId = this.game.id;
    newMission.lat = this.location[0].latLng.lat;
    newMission.lng = this.location[0].latLng.lng;
    newMission.isDeleted = false;


    this.apiService.createMission(this.game.id, newMission).subscribe(resp => {
      this.sending = false;
      this.createdMission = resp;
      if(resp == null) {
        alert("Something went wrong..");
        
      }else{
        alert("You successfully created a mission");
        window.location.reload();
      }
    });

  }


}

export class missionPayload{
  name:string ="";
  isHumanVissible:boolean = false;
  isZombieVisible:boolean = false;
  description:string = "";
  startTime:string = "";
  endTime:string = "";
  gameId:number = 0;
  lat:number = 0;
  lng:number = 0;
  isDeleted = false;
}