import { User } from "../../api.service";

export class PlayerModel {
  id: number;
  name: string;
  isHuman: boolean;
  userId: number;
  user: User;
  biteCode: string;
  rank: number;
}
// export class PlayerRef {
//   username: string = "";
//   playerId: number = 0;
//   isHuman: boolean;
  
//   squadMember: SquadMemberRef[] = [];
// }
