export class GameModel {
  id: number;
  name: string;
  gameState: number;
  nwLat: number;
  nwLng: number;
  seLat: number;
  seLng: number;
  startTime: Date = new Date();;
  endTime: Date = new Date();;


}
