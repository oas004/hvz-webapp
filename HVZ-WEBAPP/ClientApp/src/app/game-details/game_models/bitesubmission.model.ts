export class BiteSubmissionModel {
  biteCode: string;
  gameId: number;
  killerId: number;
  timeOfDeath: Date = new Date();
  story: string = "";
  lat: number = 0;
  lng: number = 0;
}
