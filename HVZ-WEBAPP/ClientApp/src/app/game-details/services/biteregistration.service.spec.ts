import { TestBed } from '@angular/core/testing';

import { BiteregistrationService } from './biteregistration.service';

describe('BiteregistrationService', () => {
  let service: BiteregistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BiteregistrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
