import { Component, OnInit, Input } from '@angular/core';
import { PlayerModel } from '../../game_models/player.model';
import { faSmile, faDizzy } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-player-list-entry',
  templateUrl: './player-list-entry.component.html',
  styleUrls: ['./player-list-entry.component.css']
})
export class PlayerListEntryComponent implements OnInit {

  @Input() player: PlayerModel;
  faSmile=faSmile;
  faDizzy=faDizzy;

  constructor() { }

  ngOnInit(): void {
  }

}
