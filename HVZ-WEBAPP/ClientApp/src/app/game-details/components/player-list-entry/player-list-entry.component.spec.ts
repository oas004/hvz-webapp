import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerListEntryComponent } from './player-list-entry.component';

describe('PlayerListEntryComponent', () => {
  let component: PlayerListEntryComponent;
  let fixture: ComponentFixture<PlayerListEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerListEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerListEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
