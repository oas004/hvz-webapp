import { Component, OnInit, Input } from '@angular/core';
import { PlayerModel } from '../../game_models/player.model';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {

  @Input() playerList: PlayerModel[];

  constructor() { }

  ngOnInit(): void {
  }

}
