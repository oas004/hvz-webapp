import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from "../../../api.service";
import { SquadObjectModel } from "../../game_models/squad.object.model"
import { CurrentgameService } from '../../../currentgame.service';

@Component({
  selector: 'app-squad-list',
  templateUrl: './squad-list.component.html',
  styleUrls: ['./squad-list.component.css']
})
export class SquadListComponent implements OnInit {

  @Input() squads: SquadObjectModel[];
  @Input() currentGame:number;

  constructor(private apiService: ApiService, private gameService: CurrentgameService) { }

  ngOnInit() {

  }
  

  

}
