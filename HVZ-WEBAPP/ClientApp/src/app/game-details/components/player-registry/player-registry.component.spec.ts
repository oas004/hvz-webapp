import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRegistryComponent } from './player-registry.component';

describe('PlayerRegistryComponent', () => {
  let component: PlayerRegistryComponent;
  let fixture: ComponentFixture<PlayerRegistryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerRegistryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRegistryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
