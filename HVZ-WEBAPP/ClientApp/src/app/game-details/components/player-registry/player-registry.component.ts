import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlayerPayload, ApiService } from '../../../api.service';
import { UserModel } from '../../game_models/user.model';
import { GameModel } from '../../game_models/game.model';

import { take } from 'rxjs/operators';
@Component({
  selector: 'app-player-registry',
  templateUrl: './player-registry.component.html',
  styleUrls: ['./player-registry.component.css']
})
export class PlayerRegistryComponent implements OnInit {

  @Input() currentGame: number;
  @Input() game: GameModel;
  player: PlayerPayload;
  @Input() user: UserModel;
  @Output() joined = new EventEmitter();


  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  submit() {
    let now = new Date().getTime();
    let start = new Date(this.game.startTime).getTime();
    let end = new Date(this.game.endTime).getTime();
    //  Checking for state and registration
    
      this.player = { id: 0, isHuman: true, isPatientZero: false, biteCode: "placeholder", userId: this.user.id, gameId: this.currentGame };

      this.apiService.postPlayer(this.currentGame, this.player).subscribe(data => { });
      this.joined.emit();
  }

}
