import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../../api.service';
import { BiteSubmissionModel } from "../../game_models/bitesubmission.model"
import { PlayerModel } from '../../game_models/player.model';
import { take } from 'rxjs/operators';
import { GameModel } from '../../game_models/game.model';
import * as moment from 'moment';

@Component({
  selector: 'app-biteregistry',
  templateUrl: './biteregistry.component.html',
  styleUrls: ['./biteregistry.component.css']
})
export class BiteregistryComponent implements OnInit {

  @Input() currentGame: number;
  bitecode: string;
  @Input() player: PlayerModel;
  submission: BiteSubmissionModel;

  game: GameModel;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getGame(this.currentGame).pipe(take(1)).subscribe(x => { this.game = x });

  }

  submit() {
    // //  Don't do this in the front-end?
    // let now = new Date().getTime();
    // let start = new Date(this.game.startTime).getTime();
    // let end = new Date(this.game.endTime).getTime();

    // if (start < now && now < end) {
    //   this.submission = { gameId: this.currentGame, biteCode: this.bitecode, killerId: this.player.id, timeOfDeath: new Date() };
    //   this.apiService.submitBite(this.currentGame, this.submission).pipe(take(1)).subscribe(data => { });

    // } else {
    //   //  handle error.
    // }

  }
}
