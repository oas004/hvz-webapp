import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiteregistryComponent } from './biteregistry.component';

describe('BiteregistryComponent', () => {
  let component: BiteregistryComponent;
  let fixture: ComponentFixture<BiteregistryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiteregistryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiteregistryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
