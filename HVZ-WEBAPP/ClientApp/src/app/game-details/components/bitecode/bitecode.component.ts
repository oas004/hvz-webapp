import { Component, OnInit, Input } from '@angular/core';
import { BiteCodeModel } from '../../game_models/bitecode.model';
import { ApiService } from '../../../api.service';
import { PlayerModel } from '../../game_models/player.model';
@Component({
  selector: 'app-bitecode',
  templateUrl: './bitecode.component.html',
  styleUrls: ['./bitecode.component.css']
})
export class BitecodeComponent implements OnInit {

  
  @Input() player: PlayerModel;

  constructor() { }

  ngOnInit(): void {
    
  }

}
