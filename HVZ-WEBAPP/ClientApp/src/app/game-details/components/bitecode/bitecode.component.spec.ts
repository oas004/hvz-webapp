import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BitecodeComponent } from './bitecode.component';

describe('BitecodeComponent', () => {
  let component: BitecodeComponent;
  let fixture: ComponentFixture<BitecodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BitecodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BitecodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
