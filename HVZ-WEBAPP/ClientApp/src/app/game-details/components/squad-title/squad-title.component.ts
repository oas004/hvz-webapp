import { Component, OnInit, Input } from '@angular/core';
import { SquadObjectModel } from '../../game_models/squad.object.model';
@Component({
  selector: 'app-squad-title',
  templateUrl: './squad-title.component.html',
  styleUrls: ['./squad-title.component.css']
})
export class SquadTitleComponent implements OnInit {

  @Input() squad: SquadObjectModel;
  

  constructor() { }

  ngOnInit() {
  }

}
