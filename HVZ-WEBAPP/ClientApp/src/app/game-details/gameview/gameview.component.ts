import { Component, OnInit, Input } from '@angular/core';
import { MapDefaults, Marker, MarkerType } from '../../map/map.component'
import { CurrentgameService } from '../../currentgame.service';
import { PlayerModel } from '../game_models/player.model';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators'
import { ApiService, SquadCheckinMarker, AdminMissionPayload } from '../../api.service';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { map } from 'rxjs/operators';
import { UserModel } from '../game_models/user.model';
import { GameModel } from '../game_models/game.model';
import { SquadObjectModel } from '../game_models/squad.object.model';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { CurrentPlayerService } from 'src/app/current-player.service';
import { AdminKillPayload } from '../../api.service';
import { BiteSubmissionModel } from '../game_models/bitesubmission.model';
import { ErrorService } from 'src/app/error.service';
import { missionPayload } from 'src/app/admin-create-mission/admin-create-mission.component';


@Component({
  selector: 'app-game',
  templateUrl: './gameview.component.html',
  styleUrls: ['./gameview.component.css']
})
export class GameviewComponent implements OnInit {
  defaults = null;
  currentGame: number; // this.detectGame();
  markers: Marker[] = [];
  game: GameModel;
  user: UserModel;
  player: PlayerModel;
  playersInGame: PlayerModel[] = [];
  loaded: boolean;
  playerLoaded: boolean;
  squads: SquadObjectModel[];
  gamestate: number = -1;
  NUMBER_OF_MARKER_TYPES = 3;
  markerCounter = 0;
  stateMessage: string[] = [
    "The game is open for registration.", "The game is in progress.", "The game has ended."
  ]
  kills: AdminKillPayload[] = [];
  checkIns: SquadCheckinMarker[] = [];
  missions: AdminMissionPayload[] = [];

  constructor(private authorizeService: AuthorizeService, private currentgameService: CurrentgameService, private apiService: ApiService, private route: ActivatedRoute, private currentPlayer: CurrentPlayerService, private errorService: ErrorService) {

  }

  ngOnInit() {

    // Detect gameid when the page is refreshed
    this.currentGame = this.detectGame();
    this.currentgameService.setCurrentgame(this.currentGame);

    this.fetchData();

  }

  private refresh() {
    window.location.reload();
  }

  //  callback done on response from API. Instead of nesting requests.
  markerCallback() {
    if (++this.markerCounter == this.NUMBER_OF_MARKER_TYPES) {
   
      this.generateMarkers(this.kills, this.checkIns, this.missions);
    }
  }

  generateButtonText() {
    return this.player.isHuman ? "Show bite code." : "Register bite code";
  }

  //  Generates Marker object for kills, checkIns and missions.
  private generateMarkers(kills: AdminKillPayload[], checkIns: SquadCheckinMarker[] = [], missions: AdminMissionPayload[] = []) {
  
    let markers: Marker[] = [];
    kills.forEach(kill => {

      //  so fucking stupid too.
      //  so ugly holy mama
      //  Kill doesn't have these defined, so this is the ugly way of accessing these variables
      let killer = kill["killer"]["username"];
      let victim = kill["victim"]["username"]
      let title = `<span>${killer} killed ${victim}</span>
      
      <span>At: ${this.convertTimeStamp(kill.timeOfDeath, "DD-MM - HH:MM:SS")}</span>`

      let body = `<div>
      
      <p>${kill.story}</p>
      </div>
      `
      let type = new MarkerType();
      type.icon = "assets/icons/grave_marker.png";
      type.id = 0;
      type.type = "Death";

      let m = new Marker();
      m.type = type;
      m.lat = kill.lat;
      m.lng = kill.lng;
      m.title = title;
      m.marker = null;
      m.body = body;
      m.dateTime = new Date(kill.timeOfDeath);
      markers.push(m);
    });
    //  checkIn
    checkIns.forEach(checkInContainer => {
      let username = checkInContainer.username;
      let ci = checkInContainer.checkIn;
      let title = `
      <Span>${username} Checked in.</Span>
     
      `;

      let body = ` <span>${this.convertTimeStamp("" + ci.startTime, "HH:MM")}-${this.convertTimeStamp("" + ci.endTime, "HH:MM")}</span>`
      let type = new MarkerType();
      type.icon = "https://gamepedia.cursecdn.com/wowpedia/e/e5/MinimapGuideFlag_16x16.png?version=65f1ace00b1c11255785528047e9d749";
      type.id = 1;
      type.type = "Squad Check-In";

      let m = new Marker();
      m.type = type;
      m.lat = ci.lat;
      m.lng = ci.lng;
      m.title = title;
      m.body = body;
      m.marker = null;
      m.dateTime = new Date(ci.startTime)
      markers.push(m);

    })
    //  mission
    missions.forEach(mission => {
   
      let title = `
      <Span>${mission.name}</Span>
     
      `;

      let body = `<p>${mission.description}</p> 
      <span>From: ${this.convertTimeStamp("" + mission.startTime, "DD.MM.YY  HH:mm")}- To: ${this.convertTimeStamp("" + mission.endTime, "DD.MM.YY  HH:mm")}</span>`
      let type = new MarkerType();
      type.icon = "https://gamepedia.cursecdn.com/wowpedia/b/b6/Quest_Avail_16x16.png?version=69ab6287909750da543ccfce078ccfa8";
      type.id = 2;
      type.type = "Mission";

      let m = new Marker();
      m.type = type;
      m.lat = mission.lat;
      m.lng = mission.lng;
      m.title = title;
      m.body = body;
      m.marker = null;
      m.dateTime = new Date(mission.startTime);
      markers.push(m);
    });


    this.markers = markers;
  }
  //  fetchData, done at initialization
  private fetchData() {
    this.currentgameService.currentgame.subscribe(cg => {
      // When number changes in the params, reset state
      this.resetState(cg);

      this.apiService.getGame(this.currentGame).pipe<GameModel>(take(1)).subscribe(g => {
        this.game = g;
        this.generateMapDefaults();

      });
      this.apiService.getUser().pipe<UserModel>(take(1)).subscribe(u => this.user = u);
      this.apiService.getSquadsGameDetails(this.currentGame).pipe<SquadObjectModel[]>(take(1)).subscribe(data => this.squads = data);
      //  kills
      this.apiService.adminGetKills(this.currentGame).pipe(take(1)).subscribe(kills => { 
        this.kills = kills;
        this.markerCallback();

      }, error => {
        alert(error);
        this.markerCallback();
      }
      );
      this.apiService.getMe(this.currentGame).subscribe(data => {
        let squad = data.squadMember.find(s => s.squad.isHuman == data.isHuman);
        //  If the player is currently a member of a squad, set this.checkIns
        if (squad) {
          this.apiService.getSquadCheckin(this.currentGame, squad.squad.id).subscribe(
            data => {
              this.checkIns = data;
              this.markerCallback();
            },
            error => {
              this.markerCallback();
            }
          );
        } else {
          this.markerCallback();
        }
      }, error => this.markerCallback());
      

      this.apiService.getMissions(this.currentGame).subscribe(
        data => {
          this.missions = data;
          this.markerCallback();
        },
        error => {
          this.markerCallback();
        }
      )
      //  ?
      this.apiService.getPlayersInGameExtended(this.currentGame).pipe(take(1)).subscribe(playerlist => {
        this.playersInGame = playerlist;
        
        this.apiService.getPlayerFromUserExtended(this.user.id, this.currentGame).pipe(take(1)).subscribe(currentPlayer => {
          this.playerCheck(currentPlayer);
          this.loaded = true;

        });

      });
    });

  }



  private resetState(cg: number) {
    this.loaded = false;
    this.playerLoaded = false;
    this.player = undefined;
    this.currentGame = cg;
    this.playersInGame = [];

  }

  private playerCheck(tempPlayer: PlayerModel) {
    if (tempPlayer) {
      this.player = {
        id: tempPlayer.id,
        name: tempPlayer.name,
        isHuman: tempPlayer.isHuman,
        biteCode: tempPlayer.biteCode,
        userId: tempPlayer.userId,
        user: tempPlayer.user,
        rank: tempPlayer.rank
      };
      this.playerLoaded = true;
    }
    else {
      this.playerLoaded = false;
    }
  }
  generateMapDefaults() {
    let defaults = new MapDefaults();
    defaults.nwLat = this.game.nwLat;
    defaults.nwLng = this.game.nwLng;
    defaults.seLat = this.game.seLat;
    defaults.seLng = this.game.seLng;
    defaults.overlaySrc = "assets/images/zone_overlay.png";
    this.defaults = defaults;
    //return defaults;
  }
  convertTimeStamp(dateTime: string, format: string = 'HH:mm') {

    let time = moment(dateTime);
    return time.format(format)
  }
  biteCodeHandler(event) {
    //
    let lat = null;
    let lng = null;
    if(!event.hasStory) {
      event.story = null;

    }
    let submission = new BiteSubmissionModel();
    lat = event.lat;
    lng = event.lng;
    submission = { gameId: this.currentGame, biteCode: event.bitecode, killerId: this.player.id, timeOfDeath: new Date(), story: event.story, lat: lat, lng: lng };
    console.log(submission);
    this.apiService.submitBite(this.currentGame, submission).pipe(take(1)).subscribe(data => {

      event.callback(true);
    },
      error => {
        event.callback(false);
      });

  }
  private detectGame(): number {
    let param = this.route.snapshot.paramMap.get("id");
    let gameid = Number(param);


    if (Number.isInteger(gameid)) {
      return gameid;
    }
    else {
      return null;
    }

  }

  private getGameState(): number {
    let now = new Date().getTime();
    let start = new Date(this.game.startTime).getTime();
    let end = new Date(this.game.endTime).getTime();

    if (now < start) {
      return 0;
    } else if (start < now && now < end) {
      return 1;
    } else {
      return 2;
    }

  }
}
