import { Component, OnInit, Input } from '@angular/core';
import { SquadList } from '../models/SquadList';

@Component({
  selector: 'app-squad-title',
  templateUrl: './squad-title.component.html',
  styleUrls: ['./squad-title.component.css']
})
export class SquadTitleComponent implements OnInit {

  @Input() squad: SquadList;
  
  constructor() { }

  ngOnInit(): void {
  }

}
