import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquadTitleComponent } from './squad-title.component';

describe('SquadTitleComponent', () => {
  let component: SquadTitleComponent;
  let fixture: ComponentFixture<SquadTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquadTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquadTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
