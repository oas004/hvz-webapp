import { EventEmitter, Component, ViewChild, ElementRef, AfterViewInit, Input, OnInit, Output, OnDestroy, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { Observable, Subscriber, of } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';

//  Defaults not set boyos!
export class MapDefaults {
  nwLat: number = 0;
  nwLng: number = 0;
  seLng: number = 0;
  seLat: number = 0;
  zoom: number = 8;
  overlaySrc: string =  "";
}

export class Marker {
  lat: number = 0;
  lng: number = 0;
  body: string = "";
  imgSrc: string = ".";
  title: string = "";
  dateTime: Date; //  Unix epoch
  type: MarkerType;
  marker: google.maps.Marker;
}

export class MarkerType {
  id: number = 0;
  type: string = "";
  icon: string = "";
  
}
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})



export class MapComponent implements AfterViewInit, OnInit, OnDestroy, AfterViewInit, OnChanges {
  @ViewChild('mapRef') mapElement: ElementRef;
  @Input() defaults: MapDefaults = new MapDefaults();
  // @Input() markers: Array<Marker> = [
  //   {lat: 0, lng: 0, body: "Hello bois", imgSrc : "https://apprecs.org/ios/images/app-icons/256/7d/1125608061.jpg",
  //    title: "Test title", dateTime: new Date(), type: {id : -1, type: "Gravestone", icon: "https://apprecs.org/ios/images/app-icons/256/7d/1125608061.jpg"}, marker: null }
  // ];
  @Input() markers: Marker[] = [];
  @Output() mapClicked = new EventEmitter();
  @Input() addMarkerOnClick = false;
  @Input() markerAddLimit: number = 0;
  @Input() zoom = -1;
  @Input() autoCenter = true;
  @Input() game = null;
  overlay: google.maps.GroundOverlay = null;
  change: Observable<MapDefaults> = null;
  types: Array<MarkerType> = [];
  filteredSecondHolder: number = -1;
  selectedTypes: Array<MarkerType> = [];
  filteredMarkers: Array<Marker> = [];
  map = null;
  currentMarker: google.maps.Marker;
  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    
    this.filteredMarkers = null;
 
    this.renderMap();
    //  easier than handling state.
  }

  evalOverlaySrc() {
    return this.defaults.overlaySrc != "";
  }

  toggleOverlay() {
    if(this.overlay != null) {
      if(this.overlay.getMap() != null) {
        this.overlay.setMap(null);
      } else {
        this.overlay.setMap(this.map);
      }
    }
  }
  ngOnDestroy() {
    //this.change.
  }
  //  on checkbox change.
  handleCheckboxChange(e) { //  rename
    //  recreate filteredMarkers.
    //  change == on
    //  on
    if(e.target.checked) {
      this.selectedTypes.push(this.types.find(type => type.type == e.target.id));
    } else {
      this.selectedTypes = this.selectedTypes.filter( type => type.type != e.target.id);
    }
    this.filterMarkers(this.filteredSecondHolder);
  }
  //  Initializes the map render.
  renderMap() {
    
    if(!window.document.getElementById('google-map-script')) {
      var s = window.document.createElement("script");
      s.id = "google-map-script";
      s.type = "text/javascript";
      s.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAfDYVDtTNf_AYJKkrSQjDgW8N5g5h006Y";
      document.getElementsByTagName("HEAD")[0].appendChild(s);
      s.onload = this.loadMap;
      this.filteredMarkers = this.markers;
    
      this.filteredMarkers.forEach( marker => {
        let type = marker.type;

        if(!this.types.find(t => t.type == type.type)) {
          
          this.types.push(type);
        }

      }
      
      );
    } else {
      this.types = [];
      this.loadMap();
    }
  }
  stripHtml(html)
  {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
  }
  //  Load map from google and add markers + overlay.
  loadMap = () => {
    if(this.zoom >= 0) {
      this.defaults.zoom = this.zoom;
    }
    
    let center =  {lat: (this.defaults.nwLat + this.defaults.seLat) / 2, lng: (this.defaults.nwLng + this.defaults.seLng) / 2};

    var map = new google.maps.Map(this.mapElement.nativeElement, {
      
      center: center,
      zoom: this.defaults.zoom
    });
    map.setCenter(map.getCenter());
    this.map = map;
    
    
    //  geo location
    
    if (this.autoCenter && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        map.setCenter(pos);
      }, function() {
        
      });
    } else {

      // Browser doesn't support Geolocation
      //  use error service?
    }

    // Used for clicking to edit 
    map.addListener('click', e=> {
      if(this.addMarkerOnClick) {
        let location = e.latLng;
        if(this.currentMarker != null) this.currentMarker.setMap(null);
        var marker = new google.maps.Marker({
          position: location, 
          map: map
        });
        this.currentMarker = marker;
        map.panTo(location)
        this.mapClicked.emit({lat: e.latLng.lat(), lng: e.latLng.lng()});
      }
      
    });
    //  adds overlay
    if(this.defaults.overlaySrc != "") {
      let overlay = new google.maps.GroundOverlay(this.defaults.overlaySrc, {north: this.defaults.nwLat, south: this.defaults.seLat, east: this.defaults.seLng, west: this.defaults.nwLng});
      this.overlay = overlay;
      overlay.setMap(map);
    }

    //  Add a google.maps.Marker to each marker object.
    this.markers.forEach( e => {
      let type = e.type;
        //  If type is not detected, add it.
        //  Could use a dicitionary based on the actual type, but I'm a stupid boi.
        if(!this.types.find(t => t.type == type.type)) {
          
          this.types.push(type);
        }
      var marker = new google.maps.Marker({
        position: {lat: e.lat, lng: e.lng},
        map: map,
        title: this.stripHtml(e.title),
        
        draggable: false,
        icon: e.type.icon,
        animation: google.maps.Animation.DROP,
      });
      
      e.marker = marker; // big brain.
      
      //  Marker onclick
      var infowindow = new google.maps.InfoWindow({
        content: e.title + e.body
      });
   
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
    });

    


  }
  //  Filters markers based on datetime and type
  filterMarkers(offsetSeconds: number = -1): void {
 
    this.filteredSecondHolder = offsetSeconds;
    if(offsetSeconds == -1) {
      //this.filteredMarkers = this.markers.filter(e => this.selectedTypes.find(e2=> e2.type == e.type.type));
      //  refactored to unset and set Map.
      this.markers.map(marker => {
        //  TODO: TEST IF THIS ACTUALLY WORKS
      
        let typeSelected = this.selectedTypes.find(type => marker.type.id == type.id)
        if(typeSelected) marker.marker.setMap(this.map);
        else {
          
          marker.marker.setMap(null);
        }
      })
    } else {
      let date = new Date();
      let timeLimit = date.getTime() - offsetSeconds * 1000;
      
      
      //this.filteredMarkers = this.markers.filter( e => e.dateTime.getTime() >= timeLimit && this.selectedTypes.find(e2=> e2.type == e.type.type));
      //  refactores to unset on this too
      this.markers.map(marker => {
        let typeSelected = this.selectedTypes.find(type => marker.type.id == type.id);  // iterate over types, which could be faster, but okay, 3n or something
        if(typeSelected && marker.dateTime.getTime() > timeLimit) marker.marker.setMap(this.map);
        else {
          marker.marker.setMap(null);
        }
      })
      this.markers
    }
    //this.loadMap();
  }

  ngAfterViewInit() {
    this.renderMap();
    
  }

}
