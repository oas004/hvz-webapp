import { Component, OnInit } from '@angular/core';

import {Game} from '../../game'
import { ApiService, GameListPayload} from '../api.service';
import { ErrorService } from '../error.service';
import {take} from 'rxjs/operators'
@Component({
  selector: 'app-nav-menu-gamelist',
  templateUrl: './nav-menu-gamelist.component.html',
  styleUrls: ['./nav-menu-gamelist.component.css']
})
export class NavMenuGamelistComponent implements OnInit {

  constructor(private apiService: ApiService, private errorService: ErrorService) { }
  
  games: Array<GameListPayload> = [];

  ngOnInit(): void {
    this.getGames()
  }

  getGames() {

    this.apiService.getGames().pipe<GameListPayload[]>(take(1)).subscribe( 
      data => this.games = data, 
      error=> this.errorService.updateError("Could not get game list"),
      );
    //  error handling khatoi
  }

}
