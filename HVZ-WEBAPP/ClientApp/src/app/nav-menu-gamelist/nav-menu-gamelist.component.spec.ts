import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavMenuGamelistComponent } from './nav-menu-gamelist.component';

describe('NavMenuGamelistComponent', () => {
  let component: NavMenuGamelistComponent;
  let fixture: ComponentFixture<NavMenuGamelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavMenuGamelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavMenuGamelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
