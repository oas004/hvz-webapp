import { Component, OnInit, Input } from '@angular/core';
import { ApiService, Message, PlayerRef } from '../api.service';
import * as moment from 'moment';
import { ModuleMapNgFactoryLoader } from '@nguniversal/module-map-ngfactory-loader';


@Component({
  selector: 'app-chat-channel',
  templateUrl: './chat-channel.component.html',
  styleUrls: ['./chat-channel.component.css']
})
export class ChatChannelComponent implements OnInit {
  @Input()messages: Message[] = [];
  @Input()currentPlayer: PlayerRef = null;
  constructor(private apiService: ApiService, ) { }

  ngOnInit() {
   
  }
  
  

  convertTimeStamp(dateTime: string) {
    let time = moment(dateTime);
    return time.format('HH:mm')
  }
  getClassOfMessage(message) {
    
  }
}
