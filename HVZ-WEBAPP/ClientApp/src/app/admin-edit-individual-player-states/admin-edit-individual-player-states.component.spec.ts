import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditIndividualPlayerStatesComponent } from './admin-edit-individual-player-states.component';

describe('AdminEditIndividualPlayerStatesComponent', () => {
  let component: AdminEditIndividualPlayerStatesComponent;
  let fixture: ComponentFixture<AdminEditIndividualPlayerStatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditIndividualPlayerStatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditIndividualPlayerStatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
