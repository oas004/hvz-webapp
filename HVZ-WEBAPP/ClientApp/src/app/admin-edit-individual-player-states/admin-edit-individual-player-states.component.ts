import { Component, OnInit, Input } from '@angular/core';
import { ApiService, AdminGetPlayersPayload, AdminGetUserPayload } from '../api.service';
import {take} from 'rxjs/operators';
import { Observable, of } from 'rxjs';
@Component({
  selector: 'app-admin-edit-individual-player-states',
  templateUrl: './admin-edit-individual-player-states.component.html',
  styleUrls: ['./admin-edit-individual-player-states.component.css']
})
export class AdminEditIndividualPlayerStatesComponent implements OnInit {
  @Input() player:  AdminGetPlayersPayload;
  @Input() gameid: number; 
  user:AdminGetUserPayload;
  bitecode:string;
  updatedPlayer:AdminGetPlayersPayload;
  // subscription1:Subscription= new Subscription();
  // subscription2:Subscription= new Subscription();

  // Rank system
  selectedRank:number;
  ranks:any[] = [
    {id:0,rankname:"Rank 0 - Unranked"},
    {id:1, rankname:"Rank 1 - Private"},
    {id:2, rankname:"Rank 2 - Corporal"}, 
    {id:3, rankname:"Rank 3 - Sergant"},
    {id:4, rankname:"Rank 4 - Master Sergant"},
    {id:5, rankname:"Rank 5 - Sergant Major"},
    {id:6, rankname:"Rank 6 - Knight"},
    {id:7, rankname:"Rank 7 - Knight-Lieutenant"},
    {id:8, rankname:"Rank 8 - Knight-Captain"},
    {id:9, rankname:"Rank 9 - Knight-Champion"},
    {id:10, rankname:"Rank 10 - Lieutenant-Commander"},
    {id:11, rankname:"Rank 11 - Commander"},
    {id:12, rankname:"Rank 12 - Marshal"},
    {id:13, rankname:"Rank 13 - Field Marshal"},
    {id:14, rankname:"Rank 14 - Grand Marshal"}
  ];
  

  constructor(private apiService:ApiService) { }

  
  ngOnInit(): void {
    this.getPlayerName();
    this.bitecode = this.player.biteCode;
 
    this.selectedRank = this.player.rank;
    this.updatedPlayer = this.player;
    this.updatedPlayer.biteCode.trim();
  }

  ngOnDestroy(){
    // this.subscription2.unsubscribe();
    // this.subscription1.unsubscribe();
  }


  getPlayerName(){
      this.apiService.getUserById(this.player.userId).pipe(take(1)).subscribe(resp => {
        this.user = resp;
      
      });
      
  }

  setNewRank(rank:any){
    this.updatedPlayer.rank = parseInt(rank);
    // Make put request 
    this.updatedPlayer.biteCode = this.player.biteCode.trim();
 
    this.apiService.updateRank(this.player.gameId, this.player.id, this.updatedPlayer).pipe(take(1)).subscribe(resp => {
 
    });
  }

  checkPatientZ(): any {

    return new Promise( resolve => {
 
      this.apiService.getPlayers(this.gameid).pipe(take(1)).subscribe(resp => {
        resp.forEach(element => {
          if(element.isPatientZero == true){
            resolve(true);
          }
        });
        resolve(false)
      })
    })
    
  }

  turnZombie(evt:any){
    this.checkPatientZ().then( value =>{
  
      if(value == false){
        this.updatedPlayer.isPatientZero = true;
        this.updatedPlayer.isHuman = false;
        this.apiService.updateRank(this.player.gameId, this.player.id, this.updatedPlayer).subscribe(resp => {
      
        })
      } else {
        alert("There is already a patient zero in the game.");
      }
    });
  }

}
