import { Injectable } from '@angular/core';
import { CurrentgameService } from './currentgame.service';
import {Observable, BehaviorSubject} from 'rxjs'
import {take} from 'rxjs/operators';
import {PlayerRef, ApiService} from './api.service'

@Injectable({
  providedIn: 'root'
})
export class CurrentPlayerService {
  private player: BehaviorSubject<PlayerRef> = new BehaviorSubject<PlayerRef>(null);
  public currentPlayer = this.player.asObservable();

  constructor(private gameService: CurrentgameService, private apiService: ApiService) { 
    
    gameService.currentgame.subscribe( change => {
      this.setPlayer(null);
      apiService.getMe(change).pipe(take(1)).subscribe(
        data => this.setPlayer(data),
        error => console.log(error)
      )
    })
  }
  private setPlayer(player: PlayerRef){
    this.player.next(player);
  }
}
