import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquadInfoComponent } from './squad-info.component';

describe('SquadInfoComponent', () => {
  let component: SquadInfoComponent;
  let fixture: ComponentFixture<SquadInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquadInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquadInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
