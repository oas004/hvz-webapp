import { Component, OnInit, Input } from '@angular/core';
import { ApiService, PlayerModelSquad, PlayerPayload, AdminSquadMemberPayload } from 'src/app/api.service';
import { Squad } from 'src/app/models/Squad';
import { SquadMembers } from 'src/app/models/SquadMembers';
import {take} from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-squad-info',
  templateUrl: './squad-info.component.html',
  styleUrls: ['./squad-info.component.css']
})
export class SquadInfoComponent implements OnInit {

  @Input() squad:Squad;
  @Input() player:PlayerPayload;
  @Input() curGame;
  
  subscription1:Subscription= new Subscription();
  subscription2:Subscription= new Subscription();

  // The squadmembers
  squadMembers:Array<AdminSquadMemberPayload> = [];
  squadMemberIdAndUsername:Array<any> = [];

  constructor(private apiService: ApiService) { }
  ngOnInit() {
    this.getSquadMembersName();
  }

  ngOnDestroy(){
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
    
  }

  // The meaning of this function is to make an object with the squadmamberid and username
  getSquadMembersName(){
    this.subscription1 = this.apiService.getSquadMembers(this.curGame, this.squad.id).subscribe(resp => {
      this.squadMembers = resp;
      this.squadMembers.forEach(memb =>{
        
        this.subscription2 = this.apiService.getPlayerById(this.curGame, memb.playerId).subscribe(resp => {
          this.squadMemberIdAndUsername.push({"squadmemberId":memb.id, "username":resp.user.username, "firstname":resp.user.firstName, "lastname":resp.user.lastName, "rank":memb.rank, "alive":resp.isHuman});
        });
      });
    });
   
  }

  getTheloggedInSquad(squad_id:number){
    
  }
}
