import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ApiService, PlayerPayload } from 'src/app/api.service';
import { CurrentgameService } from 'src/app/currentgame.service';
import { Squad } from 'src/app/models/Squad';
import { Router } from '@angular/router';
import {take} from 'rxjs/operators'

import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-squad',
  templateUrl: './squad.component.html',
  styleUrls: ['./squad.component.css']
})
export class SquadComponent implements OnInit, OnDestroy {

  @ViewChild('content') modal; 
  subscription1:Subscription= new Subscription();
  subscription2:Subscription= new Subscription();
  subscription3:Subscription= new Subscription();
  subscription4:Subscription= new Subscription();

  constructor(private apiService: ApiService, private currentGameService: CurrentgameService, private route:Router, private modalService: NgbModal) { }
  dialogHasShown : boolean = false;
  currentGame:number = null;
  squads:Array<Squad> = []
  loggedInUserId:number;
  loggedInPlayer:PlayerPayload;
  modalRef = null;
  loggedInSquadId:number = 0;
  loggedInSquadMemberId:number = 0;
  // If the player is in a squad
  inSquad:Array<Boolean> = [];

  ngOnInit(): void {
    this.getCurrentGame();
    
  }
  ngOnDestroy(){
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
    this.subscription4.unsubscribe();
    
  }

  // Load CreateSquad Component
  public showJoinSquad = true;
  public createSquad = false;

  loadCreateSquad()  {
    this.createSquad = true;
    this.showJoinSquad = false;

  }
  loadJoinSquad()  {
    this.showJoinSquad = true;
    this.createSquad = false;
  }


  // Getting the game that the user has selected
  getCurrentGame() {
    this.currentGameService.currentgame.subscribe(resp => {
      this.currentGame = resp;
      //Get the squads of the game
     this.subscription1 = this.apiService.getMultipleSquads(resp).subscribe(resp => {
        this.squads = resp;
        this.getCurrentUser(this.currentGame);
       
      });
    });

  }

  // Get the current logged in user
  getCurrentUser(game_id:number)  {
    //Getting the user from asp net user then getting the user table from user
      this.subscription2 = this.apiService.getUserFromAsp().subscribe(resp => {
        this.apiService.getPlayerFromUser(resp.id, game_id).subscribe(resp => {
          this.loggedInPlayer = resp;
      
          if(resp == null && !this.dialogHasShown){
            this.dialogHasShown = true;
            alert("You are not in this game, please join the game first, then choose a squad <3");  
            this.route.navigate([`/game/${game_id}`]);
            
          }
          else {
            this.isPlayerInASquad();
          }
          
        });
      });
  }
  // Check if the player is in a squad
  isPlayerInASquad(){

    this.squads.forEach(squad => {
      this.subscription3 = this.apiService.getSquadMembers(this.currentGame, squad.id).subscribe(resp => {
         resp.forEach(memb =>{
           if(memb.playerId == this.loggedInPlayer.id){
             this.inSquad.push(true);
             this.loggedInSquadId =squad.id;
        
            this.loggedInSquadMemberId = memb.id;
           }else{
            //  alert("Not logged in id");
           }
         }) 
      })
    });
    
  }

  //Pin the location to the squad // TODO
  //  It's a squad checking tbh, but let's try to get it done.
  //  I don't want to open a new site, so let's pop up a modal with a modal.
  pinLocation(content){
    this.modalRef = this.modalService.open(content, { centered: true });

  }

  // Log the user out of the squad
  logout(){
    var confirmation = confirm(`Are you sure you want to leave ?`);

    if(confirmation == true){
     this.subscription4 = this.apiService.deleteSquadMember(this.currentGame, this.loggedInSquadId,this.loggedInSquadMemberId).subscribe(resp =>{
        if(resp == null){
          alert("You successfully left the squad");
          this.route.navigate(['/']);
        }else{
          alert("Something went wrong..");
        }

      });
    }
  }
}
