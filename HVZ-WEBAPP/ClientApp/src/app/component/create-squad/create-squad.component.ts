import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ApiService, PlayerPayload, AdminSquadPayload, AdminSquadMemberPayload } from '../../api.service';
import { Squad, NewSquad } from 'src/app/models/Squad';
import { FormBuilder, FormGroup, FormControl, Validators, RequiredValidator } from '@angular/forms';
import { CurrentgameService } from 'src/app/currentgame.service';
import { SquadMembers } from 'src/app/models/SquadMembers';
import { Router } from '@angular/router';
import {take} from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-squad',
  templateUrl: './create-squad.component.html',
  styleUrls: ['./create-squad.component.css']
})
export class CreateSquadComponent implements OnInit, OnDestroy {
  @Input() player:PlayerPayload;
  @Input() curGame;
  
  subscription1:Subscription= new Subscription();
  subscription2:Subscription= new Subscription();
  //submission: Squad
  constructor(private apiService: ApiService, private formBuilder: FormBuilder, private currentGameService: CurrentgameService, private route:Router) {
  }
  // New squad object made by user
  newSquad:NewSquad;

  // Squad that is returned in the post
  createdSquad:Squad;

  //New squadmember object
  newSquadMember:SquadMembers;

  //New created squadmember object from the post  request
  createdSquadMember:SquadMembers;

  ngOnInit(): void {

  }
  ngOnDestroy(){
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }


  // Reactive forms validation
  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    isHuman: new FormControl('', Validators.required)
  });
  
  get f(){
    return this.form.controls;
  }
  
  // Returns boolean value for radio-button
  getIsHuman() {
    return this.form.get('isHuman').value == 'true' ? true : false;
  }

  //Sumbits and posts new squad
  submit(){
    var confirmation = confirm("Sure you want to create a squad?");
    if(confirmation){

      //This is the new squad...
      this.newSquad = new NewSquad();
      this.newSquad.isHuman = this.player.isHuman;
      this.newSquad.gameId = this.curGame;
      this.newSquad.name = this.form.get('name').value;
      this.newSquad.isDeleted = false;
      // -----------------------------

     
      this.subscription1 = this.apiService.createSquad(this.curGame, this.newSquad).subscribe(resp =>{
        this.createdSquad = resp;
        if(resp != null){
          alert(`You have successfully made squad with name: ${resp.name}`);
          this.joinTheSquad(resp.id);
        }else{
          alert("Something went wrong...");
        }
      });

    }else{
      return;
    }
    
  }

  // Called when the squad was made and the player must now join and recive rank 1
  joinTheSquad(squad_id:number){
    if(squad_id == 0 || this.player.id == null || this.curGame == null){return;}
    this.newSquadMember = new SquadMembers();
    this.newSquadMember.rank = 1; // creator of squad...
    this.newSquadMember.isDeleted = false;
    this.newSquadMember.playerId = this.player.id;
    this.newSquadMember.squadId = squad_id;
    this.newSquadMember.gameId = this.curGame;

    
    // Make the post request
    this.subscription2 = this.apiService.joinSquad(this.curGame, squad_id, this.newSquadMember).subscribe(resp=>{
      this.createdSquadMember = resp;

      if(resp != null){
        
        this.route.navigate(['/']);
      }else{
        alert("Something went terribly wrong, and you can not join our justmade squad :(");
      }
    });
  }
}
