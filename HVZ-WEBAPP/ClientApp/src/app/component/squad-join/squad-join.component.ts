import { Component, OnInit, Input, Output } from '@angular/core';
import { ApiService, PlayerModelSquad, PlayerPayload, AdminSquadMemberPayload } from '../../api.service';
import { Squad } from 'src/app/models/Squad';
import { SquadMembers } from 'src/app/models/SquadMembers';
import { CurrentgameService } from 'src/app/currentgame.service';
import { SquadList } from 'src/app/models/SquadList';
import { Router } from '@angular/router';
import {take} from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-squad-join',
  templateUrl: './squad-join.component.html',
  styleUrls: ['./squad-join.component.css']
})
export class SquadJoinComponent implements OnInit {

  subscription1:Subscription= new Subscription();
  subscription2:Subscription= new Subscription();
 
  constructor(private apiService: ApiService, private currentGameService: CurrentgameService, private route:Router ) { }

  @Input() squad:Squad;
  @Input() player:PlayerPayload;
  @Input() curGame;
  
  squadMembers :Array<AdminSquadMemberPayload> = [];
  peopleInSquad:Number;
  deadPeople:Array<AdminSquadMemberPayload> = [];
  alivePeople:Array<AdminSquadMemberPayload> = [];
 // The new squadmember that is joining this squad
  newSquadMember:SquadMembers

  ngOnInit() {

    this.getSquadMembers();
  }
  ngOnDestroy(){
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
    
  }

  submit(){
    // This is the post payload to make a new squadmember 
    this.newSquadMember = new SquadMembers();
    this.newSquadMember.gameId = this.curGame;
    this.newSquadMember.isDeleted = false;
    this.newSquadMember.squadId = this.squad.id;
    this.newSquadMember.playerId = this.player.id;
    this.newSquadMember.rank = 0;
    // --------------------------------

    this.subscription1 = this.apiService.joinSquad(this.curGame, this.squad.id, this.newSquadMember).subscribe(resp => {
      if(resp != null){
        alert(`You joined with great success!`);
        this.route.navigate([`/game/${resp.gameId}`]);
       
      }else{
        alert("Something went terribly wrong...");
        this.route.navigate([`/game/${resp.gameId}`]);
       
      }
    });

  }

  // Get the squadmembers of a squad
  getSquadMembers(){
   this.subscription2 = this.apiService.getSquadMembers(this.curGame, this.squad.id).subscribe(resp => {
      this.squadMembers = resp;
      this.peopleInSquad = this.squadMembers.length;
      this.squadMembers.forEach(memb => {
        if(memb.player.isHuman){
          this.alivePeople.push(memb);
        }else{
          this.deadPeople.push(memb);
        }
      });
     
    });
  }
}
