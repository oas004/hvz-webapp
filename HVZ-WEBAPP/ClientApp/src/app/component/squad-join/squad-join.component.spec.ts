import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquadJoinComponent } from './squad-join.component';

describe('SquadJoinComponent', () => {
  let component: SquadJoinComponent;
  let fixture: ComponentFixture<SquadJoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquadJoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquadJoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
