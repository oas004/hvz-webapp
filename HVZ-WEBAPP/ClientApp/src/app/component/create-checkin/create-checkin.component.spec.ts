import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCheckinComponent } from './create-checkin.component';

describe('CreateCheckinComponent', () => {
  let component: CreateCheckinComponent;
  let fixture: ComponentFixture<CreateCheckinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCheckinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCheckinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
