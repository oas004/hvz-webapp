import { Component, OnInit, Input } from '@angular/core';
import { ApiService, SquadCheckin } from 'src/app/api.service';
import { InputField, GameField } from 'src/app/map-modal/map-modal.component';
import { ErrorService } from 'src/app/error.service';
import { ThrowStmt } from '@angular/compiler';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-create-checkin',
  templateUrl: './create-checkin.component.html',
  styleUrls: ['./create-checkin.component.css']
})
export class CreateCheckinComponent implements OnInit {
  @Input() gameId: number = 2;  //  dirty defaults
  @Input() squadId: number = 2;
  gameObject = null;
  
  fromTimeSelected = false;
  toTimeSelected = false;
  checkIn: SquadCheckin = new SquadCheckin();
  fromDate = null;
  toDate = null;
  modal = null;
  lockCheckin = false;
  constructor(private apiService: ApiService, private errorService: ErrorService, private modalService: NgbModal) { }

  location: InputField[] = [new InputField("Check-in Location")];
  game = null;

  formatCoordinate(input: InputField, coordsOnly:boolean = true) {
    if(coordsOnly) return `${input.latLng.lat.toFixed(6)}, ${input.latLng.lng.toFixed(6)}`
    return `${input.name}[ lat: ${input.latLng.lat} lng: ${input.latLng.lng}]`
  }

  ngOnInit(): void {
    this.apiService.getGame(this.gameId).subscribe(
      data => {
        this.game = data; //  Only need to access the area of the game.
        
      }, 
      error => {
        this.errorService.updateError("Could not get the game for check-in, please reload.");
       
      }
    )

    
  }

  openVerticallyCentered(content) {
    this.modal = this.modalService.open(content, { centered: true });
  }

  //  convert to ISO format so we can send this to the api.
  convertTime(dateTime) {
    let time = moment(dateTime);
    return time.toISOString();
  }
  //  post checkin to web api
  submitCheckIn() {
    if(this.lockCheckin)  return;
    this.lockCheckin = true;
    let checkIn = new SquadCheckin;
    checkIn.startTime = this.fromDate ? this.convertTime(this.fromDate) : this.convertTime(Date.now());
    checkIn.endTime = this.toDate ? this.convertTime(this.toDate): this.convertTime(Date.now());
    checkIn.lat = this.location[0].latLng.lat;
    checkIn.lng = this.location[0].latLng.lng;
    this.checkIn.gameId = this.gameId;
    this.checkIn.squadId = this.squadId;
  
    this.apiService.postSquadCheckIn(this.gameId, this.squadId, checkIn).subscribe(
      data => {
       
        this.modal.close();
        this.lockCheckin = false;
      }, 
      err => {
        this.lockCheckin = false;
        alert(err);
      }
    );
  }
  flipFrom() {
    this.fromTimeSelected = !this.fromTimeSelected;
  }
  flipTo() {
    this.toTimeSelected = !this.toTimeSelected;
  }


}
