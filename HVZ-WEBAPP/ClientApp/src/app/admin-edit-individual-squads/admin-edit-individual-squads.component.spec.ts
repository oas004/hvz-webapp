import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditIndividualSquadsComponent } from './admin-edit-individual-squads.component';

describe('AdminEditIndividualSquadsComponent', () => {
  let component: AdminEditIndividualSquadsComponent;
  let fixture: ComponentFixture<AdminEditIndividualSquadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditIndividualSquadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditIndividualSquadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
