import { Component, OnInit, Input } from '@angular/core';
import { AdminSquadPayload, AdminSquadMemberPayload, ApiService } from '../api.service';
import { ResourceLoader } from '@angular/compiler';
import { ErrorService } from '../error.service';
import {take} from 'rxjs/operators';
@Component({
  selector: 'app-admin-edit-individual-squads',
  templateUrl: './admin-edit-individual-squads.component.html',
  styleUrls: ['./admin-edit-individual-squads.component.css']
})
export class AdminEditIndividualSquadsComponent implements OnInit {
  @Input() squad:  AdminSquadPayload;
  squadMembers: Array<AdminSquadMemberPayload> = [];
  
  // subscription1:Subscription= new Subscription();
  // subscription2:Subscription= new Subscription();
  // subscription3:Subscription= new Subscription();
  // subscription4:Subscription= new Subscription();
  

  showDeleteBtn:boolean;

  constructor(private apiService:ApiService, private errorService:ErrorService) { }

  ngOnInit(): void {
    this.getSquadMembers();
    
  }
  ngOnDestroy(){
    // this.subscription1.unsubscribe();
    // this.subscription2.unsubscribe();
    // this.subscription3.unsubscribe();
    // this.subscription4.unsubscribe();
    
  }

  // Get the squadmembers from the squad id
  getSquadMembers(){
    this.apiService.getSquadMembers(this.squad.gameId,this.squad.id).pipe(take(1)).subscribe(resp => {
      this.squadMembers = resp;
    });
  }

  //Remove the squadmember
  removeSquadMember(squadmember:AdminSquadMemberPayload){

    this.apiService.getPlayerById(squadmember.gameId,squadmember.playerId).pipe(take(1)).subscribe(resp =>{
      let ans = confirm(`Are you sure you want to delete ${resp.user.username} ?`);
      if(ans){
        this.deleteSquadMember(squadmember);
      }else{
        return;
      }
    });
    
  }

  // delete squad
  deleteSquad(){
    var confirmation = confirm(`Are you sure you want to delete ${this.squad.name} ?`);
    if(confirmation == true){
     
      this.apiService.deleteSquad(this.squad.gameId, this.squad.id).pipe(take(1)).subscribe(resp => {
        if(resp == null) {
          alert("Deleted successfully");
          location.reload();
        }else {
          error => this.errorService.updateError("Could not delete squad " + resp.toString());
        }
      });
  }
  }
  
  // Do the deleterequest of squadmember
  deleteSquadMember(squadmember:AdminSquadMemberPayload){
    this.apiService.deleteSquadMember(this.squad.gameId, this.squad.id, squadmember.id).pipe(take(1)).subscribe(resp => {
      alert(resp);
      location.reload();
      
    });
  }

  showDeleteButton(){
    if(this.showDeleteBtn)
    {
      this.showDeleteBtn = false
    }else{ this.showDeleteBtn = true; }

   
  }

 

}
