import { Injectable } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CurrentgameService {
  private game: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public currentgame = this.game.asObservable();
  constructor() { }

  setCurrentgame(num:number){
    this.game.next(num);
  }
}
