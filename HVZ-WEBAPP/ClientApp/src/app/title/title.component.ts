import {Input, Component, OnInit } from '@angular/core';
import { Game, GameTitle } from '../../game';
import * as moment from 'moment';
import {GameListPayload} from '../api.service'
@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  constructor() { }
  @Input() game: GameListPayload;

  ngOnInit(): void {
    
  }
  
  getStateStr(state: number): string {
    switch(state){
      case 0: return "Registration";
      case 1: return "In-Progress";
      case 2: return "Ended";
    }
  }

  
  convertTimeStamp(dateTime: Date) {
    let time = moment(dateTime);
    //  Formated time
    return time.format('DD:MM:YYYY');
  }
  //  Generate date view based on game status.
  generateDateDisplay() {
    switch(this.game.gameState) {
      case 0: //  Registration
        return "";
      break;
      case 1: //  in-progress;
        return `${this.convertTimeStamp(this.game.startTime)} - `;
      break;
      case 2: // ended
      return `${this.convertTimeStamp(this.game.startTime)} - ${this.convertTimeStamp(this.game.endTime)} `;
      break;
    }
  }
}
