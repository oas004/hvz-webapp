import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { FontAwesomeModule, FaIconLibrary  } from '@fortawesome/angular-fontawesome';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TitleComponent } from './title/title.component';
import { TitleListComponent } from './title-list/title-list.component';
import { MapComponent } from './map/map.component';
import { ChatChannelComponent } from './chat-channel/chat-channel.component';
import { SquadJoinComponent } from './component/squad-join/squad-join.component';
import { SquadInfoComponent } from './component/squad-info/squad-info.component';
import { ChatComponent } from './chat/chat.component';
import { ErrorComponent } from './error/error.component';
import { RoleTestComponent } from './role-test/role-test.component';
import { NavMenuGamelistComponent } from './nav-menu-gamelist/nav-menu-gamelist.component';
import { NavMenuGamelistobjectComponent } from './nav-menu-gamelistobject/nav-menu-gamelistobject.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { QRComponent } from './qr/qr.component';
import { QRCodeModule } from 'angularx-qrcode';
import { SquadComponent } from './component/squad/squad.component';
import { CreateSquadComponent } from './component/create-squad/create-squad.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MaterialModule } from './material.module';
import { GameviewComponent } from "./game-details/gameview/gameview.component";
import { SquadListComponent } from "./game-details/components/squad-list/squad-list.component";
import { SquadTitleComponent } from "./game-details/components/squad-title/squad-title.component";
import { BitecodeComponent } from './game-details/components/bitecode/bitecode.component';
import { BiteregistryComponent } from './game-details/components/biteregistry/biteregistry.component';
import { AdminComponent } from './admin/admin.component';
import { AdminEditGameComponent } from './admin-edit-game/admin-edit-game.component';
import { AdminEditIndividualGamesComponent } from './admin-edit-individual-games/admin-edit-individual-games.component';
import { AdminEditIndividualSquadsComponent } from './admin-edit-individual-squads/admin-edit-individual-squads.component';
import { AdminEditIndividualMissionsComponent } from './admin-edit-individual-missions/admin-edit-individual-missions.component';
import { AdminEditIndividualPlayerStatesComponent } from './admin-edit-individual-player-states/admin-edit-individual-player-states.component';
import { AdminCreateAGameComponent } from './admin-create-a-game/admin-create-a-game.component';
import { AdminSquadMembersComponent } from './admin-squad-members/admin-squad-members.component';
import { PlayerRegistryComponent } from './game-details/components/player-registry/player-registry.component';
import { MapModalComponent } from './map-modal/map-modal.component';
import {NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminEditKilsComponent } from './admin-edit-kils/admin-edit-kils.component';
import { AdminCreateMissionComponent } from './admin-create-mission/admin-create-mission.component';
import { FooterComponent } from './footer/footer.component';
import { HomepageHeaderComponent } from './homepage-header/homepage-header.component';
import { CreateCheckinComponent } from './component/create-checkin/create-checkin.component';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import { PlayerListComponent } from './game-details/components/player-list/player-list.component';
import { PlayerListEntryComponent } from './game-details/components/player-list-entry/player-list-entry.component';
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    TitleComponent,
    SquadTitleComponent,
    SquadJoinComponent,
    SquadInfoComponent,
    TitleListComponent,
    MapComponent,
    ChatChannelComponent,
    ChatComponent,
    ErrorComponent,
    RoleTestComponent,
    NavMenuGamelistComponent,
    NavMenuGamelistobjectComponent,
    QRComponent,
    SquadComponent,
    CreateSquadComponent,
    GameviewComponent,
    SquadListComponent,
    SquadTitleComponent,
    BitecodeComponent,
    BiteregistryComponent,
    AdminComponent,
    AdminEditGameComponent,
    AdminEditIndividualGamesComponent,
    AdminEditIndividualSquadsComponent,
    AdminEditIndividualMissionsComponent,
    AdminEditIndividualPlayerStatesComponent,
    AdminCreateAGameComponent,
    AdminSquadMembersComponent,
    PlayerRegistryComponent,
    MapModalComponent,
    AdminEditKilsComponent,
    AdminCreateMissionComponent,
    FooterComponent,
    HomepageHeaderComponent,
    CreateCheckinComponent,
    PlayerListComponent,
    PlayerListEntryComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    ApiAuthorizationModule,
    MaterialModule,
    RouterModule.forRoot([
      // VVV   Sett riktig componenter når de blir merget    VVVV
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'game/:id/squad', component: SquadComponent, canActivate: [AuthorizeGuard] },
      { path: 'game/admin', component: AdminComponent, canActivate: [AuthorizeGuard]},
      { path: 'game/admin/:id', component: AdminEditIndividualGamesComponent, canActivate: [AuthorizeGuard]},
      { path: 'game/:id/chat', component: ChatComponent, canActivate: [AuthorizeGuard]},
      { path: 'game/:id', component: GameviewComponent, canActivate: [AuthorizeGuard]}

    ], {
      onSameUrlNavigation: "reload"}),
    FontAwesomeModule,
    NgbModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ZXingScannerModule,
    QRCodeModule,
    DlDateTimeDateModule,  // <--- Determines the data type of the model
    DlDateTimePickerModule,

  ],
  exports: [
    FormsModule,
    MatInputModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true },
    NgbActiveModal
  ],
  bootstrap: [AppComponent],
 
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCog);
  }
 }
