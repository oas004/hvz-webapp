import { PlayerModelSquad, PlayerPayload } from "../api.service";

export class SquadMembers {
    // id: number;
    rank: number;
    gameId: number;
    squadId: number;
    playerId: number;
    isDeleted: boolean = false;
    //player: PlayerPayload;
  }
