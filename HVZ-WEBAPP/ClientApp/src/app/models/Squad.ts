import { SquadMembers } from "./SquadMembers";

export class Squad {
    id: number;
    name: string;
    isHuman: Boolean;
    gameId:number;
 
  }
  export class NewSquad {
    name: string;
    isHuman: Boolean;
    gameId:number;
    isDeleted:Boolean = false;
  }
