import { Component, OnInit } from '@angular/core';
import { AdminEditGameComponent } from '../admin-edit-game/admin-edit-game.component';
import { ApiService, AdminGamePayload } from '../../app/api.service';
import { ErrorService } from '../error.service';
import {take} from 'rxjs/operators';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

// This is the admin parent component

export class AdminComponent implements OnInit {
  componentSelected:Number; // what component should be viewed
  games: Array<AdminGamePayload> = [];
  //subscription:Subscription = new Subscription();
  //  replaced with take(1)
  constructor(private apiService:ApiService,private errorService: ErrorService) { }

  ngOnInit(): void {
    this.componentSelected = 0;
    this.ListGames();
  }

  ngOnDestroy() {
    //this.subscription.unsubscribe();
  }

  // The list of active games listed
  public ListGames(){
    this.componentSelected = 0;
   

   
    this.apiService.getAdminGames().pipe(take(1)).subscribe(resp => {
      this.games = resp;
    });
    
    
    
  }

  // Show the child component to create game
  public CreateAGame(){
    this.componentSelected = 1;
  }

  

 

}
