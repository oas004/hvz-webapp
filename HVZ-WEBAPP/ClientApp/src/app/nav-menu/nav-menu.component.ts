import { Component, OnInit } from '@angular/core';
import { faCoffee, faHome, faComment, faUsers, faBiohazard, faLock } from '@fortawesome/free-solid-svg-icons';
import { from, Observable, zip } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { CurrentgameService } from '../currentgame.service';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  //Fontawesome icons
  faCoffee = faCoffee;
  faHome = faHome;
  faComment = faComment;
  faUsers = faUsers;
  faBiohazard = faBiohazard;
  faLock = faLock;
  
  //Make local currentgame
  currentgame:number;

  // For check of authorization
  isExpanded = false;
  public isAuthenticated: Observable<boolean>;
  public isAdmin: Observable<boolean>;
  public User: Observable<string>

  // Add services to the constructor
  constructor(private authorizeService: AuthorizeService, private currentgameService: CurrentgameService, private apiService: ApiService) { }

  ngOnInit() {
    // Authorize-service
    // Finds out if one are logged in or not
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.isAdmin = this.apiService.checkAdmin();
    this.User = this.authorizeService.getUser().pipe(map(u => u && u.name));

    // Currentgame-service
    // Subscribe to service to set the local currentgame to the service
    this.currentgameService.currentgame.subscribe(cg => this.currentgame = cg)
  }
}
