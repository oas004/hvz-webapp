import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditGameComponent } from './admin-edit-game.component';

describe('AdminEditGameComponent', () => {
  let component: AdminEditGameComponent;
  let fixture: ComponentFixture<AdminEditGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
