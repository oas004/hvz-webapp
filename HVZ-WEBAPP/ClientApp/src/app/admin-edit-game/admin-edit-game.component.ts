import { Component, OnInit, Input } from '@angular/core';
import {AdminGamePayload, PlayerPayload, ApiService, AdminNewGamePayload, AdminSquadPayload} from '../api.service'
import {take} from 'rxjs/operators';
@Component({
  selector: 'app-admin-edit-game',
  templateUrl: './admin-edit-game.component.html',
  styleUrls: ['./admin-edit-game.component.css']
})
export class AdminEditGameComponent implements OnInit {
  players : Array<PlayerPayload> = [];
  @Input() game:  AdminGamePayload;
  squads:Array<AdminSquadPayload> = [];
  // subscription1:Subscription = new Subscription();
  // subscription2:Subscription = new Subscription();
  // subscription3:Subscription = new Subscription();

  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.getPlayers();
    this.getSquads();

  }
  ngOnDestroy(){
    // this.subscription1.unsubscribe();
    // this.subscription2.unsubscribe();
    // this.subscription3.unsubscribe();
    
  }

  // Called whent the admin presses the delete ptn
  public deleteGame(){
    let id = this.game.id; // the Id of the game that should be deleted
    var sure = confirm(`Are you sure you want to delete game ${this.game.name} ?`);
    if(sure == true){
      
      this.apiService.deleteGame(id).pipe(take(1)).subscribe(resp => {
        window.location.reload();
      });
    }else{
      return;
    }
  }

  // Get the players in a game
  getPlayers(){
      this.apiService.getPlayersInGame(this.game.id).pipe(take(1)).subscribe(resp => {
        this.players = resp;
  
      });
  }

  // Get the amount of squads in a game
  getSquads(){
    this.apiService.getSquads(this.game.id).pipe(take(1)).subscribe(resp => {
      this.squads = resp;
    });
  }


}
