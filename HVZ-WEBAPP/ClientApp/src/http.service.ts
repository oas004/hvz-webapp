import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule, HttpResponse} from '@angular/common/http';
import { of, Observable} from 'rxjs';
import { GameTitle } from './game';

@Injectable({
  providedIn: 'root'
})
export class HTTPService {


  private apiUrl = 'https://localhost:44321/api/'// Development, change this later.
  constructor(private http: HttpClient) {
    console.log("loading service");
    this.http.get("assets/config.json").subscribe(e => {
      console.log(e);
      this.apiUrl = e['api_path']
      console.log(this.apiUrl);
    });
  }
  //  Game Endpoint
  public getGames(): Observable<GameTitle> {    
    console.log(this.apiUrl);
    return this.http.get<GameTitle>(this.apiUrl + "game");
    //  catch errors in function?
  }

  public getGame(gameId: number): Observable<Object> {
    console.log(this.apiUrl);
    return this.http.get(this.apiUrl + "game/" + gameId);
  }

}
