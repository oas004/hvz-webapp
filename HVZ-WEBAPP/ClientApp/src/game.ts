export interface Game {

}

export class GameTileObject {
    rules: string;
    game: GameTitle;
}

export class GameTitle {
    id: number;
    name: string;
    gameState: number;
    nwLat: number;
    nwLng: number;
    seLat: number;
    seLng: number;
    players:number;
    startTime: Date = new Date();
    endTime: Date = new Date();
}